#include <Saga.h>
#include <chrono>
#include <iostream>

using namespace saga;
using namespace core;
using namespace video;

struct CameraMatrix {
  glm::mat4 View;
  glm::mat4 Projection;
  glm::vec3 Position;
};

struct LightSource {
  glm::vec3 Position;
  float Intensity;
};

struct Material {
  glm::vec3 albedo;
  float metallic;
  float roughness;
  float reflectance;
  float anisotropy;
  float clearCoat;
  float clearCoatRoughness;
} material;

struct MaterialMap {
  std::uint32_t albedoMap;
  std::uint32_t normalMap;
  std::uint32_t roughnessMap;
  std::uint32_t metallicMap;
  std::uint32_t reflectanceMap;
  std::uint32_t anisotropyMap;
  std::uint32_t clearCoatMap;
  std::uint32_t clearCoatRoughnessMap;
} materialMap;

int main(int argc, char* argv[]) {
  auto device = createDevice(E_DRIVER_TYPE::VULKAN, {1280, 720}, 16, false, false, false);
  auto& driver = device->getVideoDriver();
  auto& smgr = device->getSceneManager();

  auto& cam = smgr->addCameraSceneNode();
  cam->setPosition({3, 0, 1});
  cam->setTarget({0, 0, 0});

  auto mesh = smgr->getMesh("../media/ceberus/ceberus.obj");
  if (!mesh) return 1;
  auto albedoTexture = driver->createTexture("../media/ceberus/albedo.png");
  if (!albedoTexture) return 1;
  auto normalTexture = driver->createTexture("../media/ceberus/normal.png");
  if (!normalTexture) return 1;
  auto roughnessTexture = driver->createTexture("../media/ceberus/roughness.png");
  if (!roughnessTexture) return 1;
  auto metallicTexture = driver->createTexture("../media/ceberus/metallic.png");
  if (!metallicTexture) return 1;
  auto node = smgr->createSceneNode(mesh);
  node->setTexture(0, albedoTexture);
  node->setTexture(1, normalTexture);
  node->setTexture(2, roughnessTexture);
  node->setTexture(3, metallicTexture);
  node->setScale({5, 5, 5});

  auto shaderInfo = driver->createShader();

  shaderInfo.VSSource = R"(
    #version 450
    layout (location = 0) in vec4 inPosition;
    layout (location = 1) in vec3 inNormal;
    layout (location = 2) in vec2 texCoord;

    layout (binding = 0) uniform CameraMatrix
    {
      mat4 View;
      mat4 Projection;
      vec3 Position;
    } camera;

    layout (binding = 1) uniform ModelMatrix
    {
      mat4 Model;
    } model;

    layout (binding = 2) uniform LightSource
    {
      vec3 Position;
      float Intensity;
    } light;

    layout (location = 0) out vec3 globalPos;
    layout (location = 1) out vec3 outNormal;
    layout (location = 2) out vec2 uv;

    void main() {
      globalPos = vec3(model.Model * inPosition);
      outNormal = normalize(mat3(model.Model) * inNormal);
      uv = texCoord;
      gl_Position = camera.Projection * camera.View * model.Model * inPosition;
    }
  )";

  shaderInfo.FSSource = R"(
    #version 450
    layout (location = 0) in vec3 globalPos;
    layout (location = 1) in vec3 inNormal;
    layout (location = 2) in vec2 uv;

    layout (push_constant) uniform MaterialMapFlag {
      layout (offset = 0) bool albedoMap;
      layout (offset = 4) bool normalMap;
      layout (offset = 8) bool roughnessMap;
      layout (offset = 12) bool metallicMap;
      layout (offset = 16) bool reflectanceMap;
      layout (offset = 20) bool anisotropyMap;
      layout (offset = 24) bool clearCoatMap;
      layout (offset = 28) bool clearCoatRoughnessMap;
    } mapFlag;

    layout (binding = 0) uniform CameraMatrix
    {
      mat4 View;
      mat4 Projection;
      vec3 Position;
    } camera;

    layout (binding = 1) uniform ModelMatrix
    {
      mat4 Model;
    } model;

    layout (binding = 2) uniform LightSource
    {
      vec3 Position;
      float Intensity;
    } light;

    layout (binding = 3) uniform Material
    {
      vec3 albedo;
      float roughness;
      float metallic;
      float reflectance;
      float anisotropy;
      float clearCoat;
      float clearCoatRoughness;
    } material;

    layout (binding = 4) uniform sampler2D albedoMap;
    layout (binding = 5) uniform sampler2D normalMap;
    layout (binding = 6) uniform sampler2D roughnessMap;
    layout (binding = 7) uniform sampler2D metallicMap;
    layout (binding = 8) uniform sampler2D reflectanceMap;
    layout (binding = 9) uniform sampler2D anisotropyMap;
    layout (binding = 10) uniform sampler2D clearCoatMap;
    layout (binding = 11) uniform sampler2D clearCoatRoughnessMap;

    layout (location = 0) out vec4 fragColor;

    #define getMaterialFloat(MAT) (mapFlag.MAT##Map == true) ? texture(MAT##Map, uv).r : material.MAT
    #define getMaterialVec3(MAT) (mapFlag.MAT##Map == true) ? texture(MAT##Map, uv).rgb : material.MAT

    const float MEDIUMP_FLT_MAX = 65504.f;
    const float PI = 3.14159265358979323846;

    float saturateMediump(float x) {
      return min(x, MEDIUMP_FLT_MAX);
    }

    float D_GGX(float NoH, float roughness) {
      float alpha = roughness * roughness;
      float alpha2 = alpha * alpha;
      float denom = NoH * NoH * (alpha2 - 1.0) + 1.0;
      return (alpha2)/(PI * denom*denom);
    }

    float V_SmithGGXCorrelated(float NoL, float NoV, float a) {
      float a2 = a * a;
      float GGXL = NoV * sqrt((-NoL * a2 + NoL) * NoL + a2);
      float GGXV = NoL * sqrt((-NoV * a2 + NoV) * NoV + a2);
      return 0.5 / (GGXV + GGXL);
    }

    float D_GGX_Anisotropic(float NoH, const vec3 h,
        const vec3 t, const vec3 b, float at, float ab) {
      float ToH = dot(t, h);
      float BoH = dot(b, h);
      float a2 = at * ab;
      highp vec3 v = vec3(ab * ToH, at * BoH, a2 * NoH);
      highp float v2 = dot(v, v);
      float w2 = a2 / v2;

      return a2 * w2 * w2 * (1.0 / PI);
    }

    float V_SmithGGXCorrelated_Anisotropic(float at, float ab, float ToV, float BoV,
        float ToL, float BoL, float NoV, float NoL) {
      float lambdaV = NoL * length(vec3(at * ToV, ab * BoV, NoV));
      float lambdaL = NoV * length(vec3(at * ToL, ab * BoL, NoL));
      float v = 0.5 / (lambdaV + lambdaL);
      return saturateMediump(v);
    }

    vec3 F_Schlick(float LoH, vec3 f0) {
      return f0 + (vec3(1.0) - f0) * pow(1.0 - LoH, 5.0);
    }

    float F_Schlick(float VoH, float f0, float f90) {
      return f0 + (f90 - f0) * pow(1.0 - VoH, 5.0);
    }

    float V_Kelemen(float LoH) {
      return 0.25 / (LoH * LoH);
    }

    float Fd_Lambert() {
      return 1.0 / PI;
    }

    vec3 specularLobeBRDF(vec3 L, vec3 V, vec3 N, vec3 X, vec3 Y,
                    float roughness, float anisotropy, float metallic, float reflectance, vec3 baseColor) {
      vec3 H = normalize(L + V);
      float NoV = abs(dot(N, V)) + 1e-5;
      float NoH = clamp(dot(N, H), 0.0, 1.0);
      float NoL = clamp(dot(N, L), 0.0, 1.0);
      float LoH = clamp(dot(L, H), 0.0, 1.0);
      float ToV = dot(X, V);
      float BoV = dot(Y, V);
      float ToL = dot(X, L);
      float BoL = dot(Y, L);

      vec3 f0 = 0.16 * reflectance * reflectance * (1.0 - metallic) + baseColor * metallic;

      float at = max(roughness * (1.0 + anisotropy), 0.001);
      float ab = max(roughness * (1.0 - anisotropy), 0.001);

      float D = D_GGX_Anisotropic(NoH, H, X, Y, at, ab);
      float Vi = V_SmithGGXCorrelated_Anisotropic(at, ab, ToV, BoV, ToL, BoL, NoV, NoL);
      vec3 F = F_Schlick(LoH, f0);

      return D * Vi * F;
    }

    vec3 diffuseLobeBRDF(float metallic, vec3 baseColor) {
      vec3 diffuseColor = (1.0 - metallic) * baseColor;
      return diffuseColor * Fd_Lambert();
    }

    float clearCoatLobe(vec3 L, vec3 V, vec3 N, float clearCoat, 
            float clearCoatPerceptualRoughness) {
      float clearCoatRoughness = mix(0.089, 0.6, clearCoatPerceptualRoughness);
      clearCoatRoughness = pow(clearCoatRoughness, 2);

      vec3 H = normalize(L + V);
      float NoH = clamp(dot(N, H), 0.0, 1.0);
      float LoH = clamp(dot(L, H), 0.0, 1.0);

      float  Dc = D_GGX(NoH, clearCoatRoughness);
      float  Vc = V_Kelemen(LoH);
      float  Fc = F_Schlick(LoH, 0.04, 1.0) * clearCoat; // clear coat strength

      return (Dc * Vc) * Fc;
    }

    vec3 BRDF( vec3 L, vec3 V, vec3 N, vec3 X, vec3 Y )
    {
      vec3 albedo = getMaterialVec3(albedo);
      float roughness = getMaterialFloat(roughness);
      float metallic = getMaterialFloat(metallic);
      float reflectance = getMaterialFloat(reflectance);
      float anisotropy = getMaterialFloat(anisotropy);
      float clearCoat = getMaterialFloat(clearCoat);
      float clearCoatRoughness = getMaterialFloat(clearCoatRoughness);

      vec3 Fr = specularLobeBRDF(L, V, N, X, Y, roughness, anisotropy, metallic, reflectance, albedo);
      vec3 Fd = diffuseLobeBRDF(metallic, albedo);

      // clear coat
      float Frc = clearCoatLobe(L, V, N, clearCoat, clearCoatRoughness);

      return Fd + Fr + Frc;
    }

    vec3 vNormal, vLight, vView, vTangent, vBitangent;

    void evaluateNormal() {
      vec3 tangentNormal = texture(normalMap, uv).xyz * 2.0 - 1.0;
      vec3 q1 = dFdx(globalPos);
      vec3 q2 = dFdy(globalPos);
      vec2 st1 = dFdx(uv);
      vec2 st2 = dFdy(uv);

      vec3 N = normalize(inNormal);
      vTangent = normalize(q1 * st2.t - q2 * st1.t);
      vBitangent = -normalize(cross(N, vTangent));

      mat3 TBN = mat3(vTangent, vBitangent, N);

      vNormal = normalize(TBN * tangentNormal);
    }

    vec3 computeLuminance(vec3 color, float lightIntensity) {
      float NoL = clamp(dot(vNormal, vLight), 0.0, 1.0);
      float illuminance = light.Intensity * NoL;
      vec3 luminance = color * illuminance;
      return luminance;
    }

    void main() {
      vLight = normalize(light.Position - globalPos);
      vView = normalize(camera.Position - globalPos);
      if (mapFlag.normalMap) {
        evaluateNormal();
      } else {
        vNormal = inNormal;
      }

      vec3 color = BRDF(vLight, vView, vNormal, vTangent, vBitangent);
      vec3 luminance = computeLuminance(color, light.Intensity);

      fragColor = vec4(luminance, 1.0f);
    }
  )";

  auto pipelineInfo = driver->createPipeline();
  pipelineInfo.Shaders = driver->createResource(std::move(shaderInfo));
  pipelineInfo.Rasterizer.CullMode = E_CULL_MODE::NONE;

  pipelineInfo.Layout.Attributes[0][0] = {
    E_ATTRIBUTE_TYPE::POSITION,
    E_ATTRIBUTE_FORMAT::FLOAT3,
  };

  pipelineInfo.Layout.Attributes[0][1] = {
    E_ATTRIBUTE_TYPE::NORMAL,
    E_ATTRIBUTE_FORMAT::FLOAT3,
  };

  pipelineInfo.Layout.Attributes[0][2] = {
    E_ATTRIBUTE_TYPE::TEXTURE_COORDINATE,
    E_ATTRIBUTE_FORMAT::FLOAT2,
  };

  auto cameraUniformInfo = driver->createShaderUniform();
  cameraUniformInfo.Size = sizeof(CameraMatrix);
  auto cameraUniform = driver->createResource(std::move(cameraUniformInfo));

  auto modelUniformInfo = driver->createShaderUniform();
  modelUniformInfo.Size = sizeof(glm::mat4);
  auto modelUniform = driver->createResource(std::move(modelUniformInfo));

  auto lightUniformInfo = driver->createShaderUniform();
  lightUniformInfo.Size = sizeof(LightSource);
  auto lightUniform = driver->createResource(std::move(lightUniformInfo));

  auto MaterialUniformInfo = driver->createShaderUniform();
  MaterialUniformInfo.Size = sizeof(Material);
  auto materialUniform = driver->createResource(std::move(MaterialUniformInfo));

  auto materialFlagConstantInfo = driver->createPushConstant();
  materialFlagConstantInfo.Size = sizeof(MaterialMap);
  auto materialMapFlag = driver->createResource(std::move(materialFlagConstantInfo));

  auto pipeline = driver->createResource(std::move(pipelineInfo));
  node->setPipeline(pipeline);

  auto passInfo = driver->createRenderPass();
  passInfo.State.Colors[0] = {
    E_ATTACHMENT_STATE::CLEAR,
    { 0.f, 0.f, 0.f, 1.f }
  };

  auto pass = driver->createResource(std::move(passInfo));
  smgr->registerNode(node, pass);

  materialMap.albedoMap = 1;
  materialMap.normalMap = 1;
  materialMap.roughnessMap = 1;
  materialMap.metallicMap = 1;
  materialMap.reflectanceMap = 0;
  materialMap.anisotropyMap = 0;
  materialMap.clearCoatMap = 0;
  materialMap.clearCoatRoughnessMap = 0;

  material.albedo = glm::vec3(1.0f, 0.765557f, 0.336057f);
  material.metallic = 0.5;
  material.roughness = 0.5;
  material.reflectance = 0.8;
  material.anisotropy = 0.0;
  material.clearCoat = 0.0;
  material.clearCoatRoughness = 0.0;

  node->setOnRender([node, driver, modelUniform, lightUniform, materialUniform, materialMapFlag]() {
    auto matrix = node->getAbsoluteTransformation();
    driver->updateShaderUniform(modelUniform, &matrix);
    driver->bindShaderUniform(modelUniform, 1);

    LightSource light;
    light.Position = glm::vec3(20.f, 10.f, 20.f);
    light.Intensity = 10.f;
    driver->updateShaderUniform(lightUniform, &light);
    driver->bindShaderUniform(lightUniform, 2);
    driver->updateShaderUniform(materialUniform, &material);
    driver->bindShaderUniform(materialUniform, 3);
    driver->updatePushConstant(materialMapFlag, (const void*) &materialMap);

    driver->bindTexture(node->getTexture(0), 4);
    driver->bindTexture(node->getTexture(1), 5);
    driver->bindTexture(node->getTexture(2), 6);
    driver->bindTexture(node->getTexture(3), 7);
  });

  auto Then = std::chrono::high_resolution_clock::now();
  while (device->run()) {
    if (device->isWindowActive()) {
      smgr->animate();
      driver->begin();
      driver->beginPass(pass);

      CameraMatrix matrix;
      matrix.View = cam->getViewMatrix();
      matrix.Projection = cam->getProjectionMatrix();
      matrix.Position = cam->getPosition();
      driver->updateShaderUniform(cameraUniform, &matrix);
      driver->bindShaderUniform(cameraUniform, 0);

      driver->draw();
      driver->endPass();
      driver->end();
      driver->submit();
      driver->present();
    } else
      device->yield();

    auto Now = std::chrono::high_resolution_clock::now();
    auto millis = std::chrono::duration_cast<std::chrono::milliseconds>(Now - Then).count();
    Then = Now;
    if (millis > 0)
    {
      auto sec = millis / 1000.f;
      int fps = 60.f / sec;
      device->setWindowCaption("Saga 3D PBR - FPS: " + std::to_string(fps));
    }
  }

  return 0;
}
