#include <Saga.h>
#include <chrono>

using namespace saga;
using namespace core;
using namespace video;

int main(int argc, char* argv[]) {
  auto device = createDevice(E_DRIVER_TYPE::VULKAN, {800, 600}, 16, false, false, false);

  auto& driver = device->getVideoDriver();
  auto& smgr = device->getSceneManager();

  std::vector<scene::S3DVertex> Vertices;
  std::vector<glm::vec3> Colors;

  Vertices.push_back({
    0.0f, 0.5f, 0.5f
  });
  Colors.push_back({
    1.0f, 0.0f, 0.0f
  });

  Vertices.push_back({
    0.5f, -0.5f, 0.5f
  });
  Colors.push_back({
    0.0f, 1.0f, 0.0f
  });

  Vertices.push_back({
    -0.5f, -0.5f, 0.5f
  });
  Colors.push_back({
    0.0f, 0.0f, 1.0f
  });

  std::vector<std::uint32_t> Indices = { 0, 1, 2 };

  auto mesh = std::make_shared<scene::SMesh>();
  auto meshBuffer = std::make_unique<scene::CMeshBuffer>();
  meshBuffer->append(std::move(Vertices), std::move(Indices));
  const auto attributeSize = sizeof(float) * 3;
  meshBuffer->appendAttribute((const char*) Colors.data(), Colors.size() * attributeSize, attributeSize);
  constexpr auto meshBufferLocation = 0;
  meshBuffer->setBindingLocation(meshBufferLocation);
  mesh->addMeshBuffer(std::move(meshBuffer));

  auto node = smgr->createSceneNode(mesh);

  auto shaderInfo = driver->createShader();
  shaderInfo.VSSource = R"(
    #version 450
    layout (location = 0) in vec4 position;
    layout (location = 1) in vec3 color;
    layout (location = 0) out vec3 out_color;

    layout (push_constant) uniform ColorPower {
      layout (offset = 0) float value;
    } colorPower;

    void main() {
      gl_Position = position;
      out_color = color * colorPower.value;
    }
  )";

  shaderInfo.FSSource = R"(
    #version 450
    layout (location = 0) in vec3 out_color;
    layout (location = 0) out vec4 fragColor;

    void main() {
      fragColor = vec4(out_color.x, out_color.y, out_color.z, 1.0);
    }
  )";

  auto pipelineInfo = driver->createPipeline();
  pipelineInfo.Shaders = driver->createResource(std::move(shaderInfo));

  pipelineInfo.Layout.Attributes[meshBufferLocation][0] = {
    E_ATTRIBUTE_TYPE::POSITION,
    E_ATTRIBUTE_FORMAT::FLOAT3,
  };
  pipelineInfo.Layout.Attributes[meshBufferLocation][1] = {
    E_ATTRIBUTE_TYPE::CUSTOM,
    E_ATTRIBUTE_FORMAT::FLOAT3,
  };

  auto pipeline = driver->createResource(std::move(pipelineInfo));
  node->setPipeline(pipeline);

  auto passInfo = driver->createRenderPass();
  auto pass = driver->createResource(std::move(passInfo));
  smgr->registerNode(node, pass);

  // color power for push_constant, try setting to 1.0f to see the change
  const auto power = 0.5f;
  auto pushConstantInfo = driver->createPushConstant();
  pushConstantInfo.Size = sizeof(power);
  auto constant = driver->createResource(std::move(pushConstantInfo));

  // push constant is only updated when node's pipeline is bound, so we use OnRender hook
  node->setOnRender([node, driver, constant, &power]() {
    driver->updatePushConstant(constant, (const void*) &power);
  });

  auto Then = std::chrono::high_resolution_clock::now();
  while (device->run()) {
    if (device->isWindowActive()) {
      driver->begin();
      driver->beginPass(pass);
      driver->draw();
      driver->endPass();
      driver->end();
      driver->submit();
      driver->present();
    } else
      device->yield();

    auto Now = std::chrono::high_resolution_clock::now();
    auto millis = std::chrono::duration_cast<std::chrono::milliseconds>(Now - Then).count();
    Then = Now;
    if (millis > 0)
    {
      auto sec = millis / 1000.f;
      int fps = 60.f / sec;
      device->setWindowCaption("Saga 3D Triangle - FPS: " + std::to_string(fps));
    }
  }

  return 0;
}

