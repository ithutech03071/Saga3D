# Saga3D Samples

## [01 Window](https://gitlab.com/InnerPieceOSS/Saga3D/tree/master/samples/01_window)
Create a window and clear it to a color.

![](https://i.imgur.com/hfSHc5F.png)

## [02 Triangle](https://gitlab.com/InnerPieceOSS/Saga3D/tree/master/samples/02_triangle)
Draw a triangle and color it using fragment shader.

Also demonstrate custom vertex attribute and push_constant.

![](https://i.imgur.com/Cid3wfV.png)

## [03 Mesh](https://gitlab.com/InnerPieceOSS/Saga3D/tree/master/samples/03_mesh)
Load a mesh and do texture mapping via shader.

Use your mouse to look around and WASD keys to move camera.

![](https://i.imgur.com/vXyxfgu.png)

## [04 Multiple render targets](https://gitlab.com/InnerPieceOSS/Saga3D/tree/master/samples/04_multiple_render_targets)
Render scene to multiple textures.

Then copy 3 textures to one texture and display.

Because Vulkan's swapchain format is different, we use blitTexture to convert color format.

Texture contents:
- Left: position texture
- Middle: albedo texture
- Right: depth texture (why this looks abnormal? depth is a single value while pixel has 4 channel values)

![](https://i.imgur.com/jQz0gkH.jpg)

## [05 GPU skinning animation](https://gitlab.com/InnerPieceOSS/Saga3D/tree/master/samples/05_animation)

Animated model with GPU skinning. Model file is the wolf from [Free3D](https://free3d.com/3d-model/wolf-rigged-and-game-ready-42808.html).

Also demonstrate sky rendering using full-screen triangle clipped into full-screen quad, more efficiently than rendering 6 faces sky box.

Co-operated with Arek on sky rendering.

![](https://gitlab.com/InnerPieceOSS/Saga3D/uploads/50b93cf470e1b191af24a93f581b70eb/anim.gif)

## [07 Depth Peeling](https://gitlab.com/InnerPieceOSS/Saga3D/tree/master/samples/07_depth_peeling)

Implementation of Order Independent Transparency using Depth Peeling algorithm.

References:

- [Cass Everitt](https://pubweb.eng.utah.edu/~cs5610/handouts/order_independent_transparency.pdf)

![](https://i.imgur.com/gKlBUxn.png)

## [08 Physically Based Rendering](https://gitlab.com/InnerPieceOSS/Saga3D/tree/master/samples/08_pbr)

Physically Based Rendering (PBR) algorithm by `Anh Phan`.

References:
- [Google Filament](https://google.github.io/filament/Filament.html)
- [Disney PBR](https://disney-animation.s3.amazonaws.com/library/s2012_pbs_disney_brdf_notes_v2.pdf)

![](https://i.imgur.com/L61Fyqs.png)

## [09 Texture array](https://gitlab.com/InnerPieceOSS/Saga3D/tree/master/samples/09_texture_array)

Demonstrate texture array: Draw 3 triangles with instancing, load an array of textures, sample color from the texture array.

![](https://i.imgur.com/oGnD9QY.png)

## [10 ImGui](https://gist.github.com/manhnt9/611010ad418b13a3ca7bf4a10a2c35ce)

GUI rendering using ImGui and Saga3D.

![](https://i.imgur.com/CqGKWR7.png)
