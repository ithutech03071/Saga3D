#include <Saga.h>
#include <chrono>

using namespace saga;
using namespace core;
using namespace video;
using namespace scene;

struct CameraMatrix {
  glm::mat4 View;
  glm::mat4 Projection;
};

int main(int argc, char* argv[]) {
  auto device = createDevice(E_DRIVER_TYPE::VULKAN, {1280, 720}, 16, false, false, false);
  auto& driver = device->getVideoDriver();
  auto& smgr = device->getSceneManager();

  auto& cam = smgr->addCameraSceneNodeFPS();
  cam->setPosition({-3, 2, 0.5});
  cam->setTarget({-1, 2, 0});

  auto mesh = smgr->getMesh("../media/wolf.x");
  auto texture = driver->createTexture("../media/wolf.jpg");
  if (!mesh) return 1;
  if (!texture) return 1;
  auto node = smgr->createSceneNode(mesh);
  node->setScale({15, 15, 15});

  auto shaderInfo = driver->createShader();
  shaderInfo.VSSource = R"(
    #version 450
    layout (location = 0) in vec4 position;
    layout (location = 1) in vec2 uv;

    layout (binding = 0) uniform CameraMatrix
    {
      mat4 View;
      mat4 Projection;
    } camera;

    layout (binding = 1) uniform ModelMatrix
    {
      mat4 Model;
    } model;

    layout (location = 0) out vec2 out_uv;

    void main() {
      gl_Position = camera.Projection * camera.View * model.Model * position;
      float i = floor(gl_Position.x * 10.f);
      out_uv = uv;
    }
  )";

  shaderInfo.FSSource = R"(
    #version 450
    layout (binding = 2) uniform sampler2D depthTexture;
    layout (binding = 3) uniform sampler2D modelTexture;
    layout (location = 0) in vec2 uv;
    layout (location = 0) out vec4 fragColor;

    void main() {
      float depth = texture(depthTexture, uv).r;
      if (depth > 0 && gl_FragCoord.z <= depth)
        discard;
      fragColor = vec4(texture(modelTexture, uv).rgb, 1);
    }
  )";

  auto pipelineInfo = driver->createPipeline();
  pipelineInfo.Shaders = driver->createResource(std::move(shaderInfo));

  pipelineInfo.Layout.Attributes[0][0] = {
    E_ATTRIBUTE_TYPE::POSITION,
    E_ATTRIBUTE_FORMAT::FLOAT3,
  };
  pipelineInfo.Layout.Attributes[0][1] = {
    E_ATTRIBUTE_TYPE::TEXTURE_COORDINATE,
    E_ATTRIBUTE_FORMAT::FLOAT2,
  };
  pipelineInfo.Rasterizer.CullMode = E_CULL_MODE::NONE;
  pipelineInfo.Rasterizer.DepthClamp = true;
  auto renderPipeline = driver->createResource(SPipeline{pipelineInfo});

  auto cameraUniformInfo = driver->createShaderUniform();
  cameraUniformInfo.Size = sizeof(CameraMatrix);
  auto cameraUniform = driver->createResource(std::move(cameraUniformInfo));

  auto modelUniformInfo = driver->createShaderUniform();
  modelUniformInfo.Size = sizeof(glm::mat4);
  auto modelUniform = driver->createResource(std::move(modelUniformInfo));

  auto depthTextureInfo = driver->createTexture();
  depthTextureInfo.Format = E_PIXEL_FORMAT::DEPTH_32;
  depthTextureInfo.IsRenderTarget = true;
  depthTextureInfo.IsDepthAttachment = true;
  depthTextureInfo.Width = driver->getWidth();
  depthTextureInfo.Height = driver->getHeight();
  auto depthBuffer1 = driver->createTexture(STexture{depthTextureInfo});
  auto depthBuffer2 = driver->createTexture(STexture{depthTextureInfo});

  auto renderTargetInfo = driver->createTexture();
  renderTargetInfo.Format = E_PIXEL_FORMAT::RGBA8;
  renderTargetInfo.Width = driver->getWidth();
  renderTargetInfo.Height = driver->getHeight();
  renderTargetInfo.IsRenderTarget = true;
  auto colorBuffer1 = driver->createTexture(STexture{renderTargetInfo});
  auto colorBuffer2 = driver->createTexture(STexture{renderTargetInfo});
  auto colorBuffer3 = driver->createTexture(STexture{renderTargetInfo});

  renderTargetInfo.Format = E_PIXEL_FORMAT::BGRA8;
  auto screen = driver->createTexture(STexture{renderTargetInfo});

  auto passInfo = driver->createRenderPass();
  passInfo.UseDefaultAttachments = false;
  passInfo.DepthStencilAttachment = depthBuffer1;
  passInfo.ColorAttachments[0] = colorBuffer1;

  auto pass = driver->createResource(std::move(passInfo));
  passInfo.DepthStencilAttachment = NULL_GPU_RESOURCE_HANDLE;
  passInfo.ColorAttachments[0] = colorBuffer3;
  auto compositionPass = driver->createResource(std::move(passInfo));
  smgr->registerNode(node, pass);

  node->setOnRender([node, driver, modelUniform, texture]() {
    auto matrix = node->getAbsoluteTransformation();
    driver->updateShaderUniform(modelUniform, &matrix);
    driver->bindShaderUniform(modelUniform, 1);
    driver->bindTexture(texture, 3);
  });
  node->setPipeline(renderPipeline);

  std::shared_ptr<ISceneNode> screenNode;
  {
    auto mesh = std::make_shared<SMesh>();
    auto meshBuffer = std::make_unique<CGPUMeshBuffer>();
    meshBuffer->isNull(true);
    meshBuffer->setVertexCount(3);
    meshBuffer->setPrimitiveType(E_PRIMITIVE_TYPE::TRIANGLES);
    mesh->addMeshBuffer(std::move(meshBuffer));
    screenNode = smgr->createSceneNode(mesh);

    auto shaderInfo = driver->createShader();
    shaderInfo.VSSource = R"(
      #version 450
      layout (location = 0) out vec2 uv;

      void main() {
        uv = vec2((gl_VertexIndex << 1) & 2, gl_VertexIndex & 2);
        gl_Position = vec4(uv* 2.0f + -1.0f, 0.0f, 1.0f);
      }
    )";

    shaderInfo.FSSource = R"(
      #version 450
      layout (location = 0) in vec2 uv;
      layout (binding = 0) uniform sampler2D layer1Texture;
      layout (binding = 1) uniform sampler2D layer2Texture;
      layout (location = 0) out vec4 fragColor;

      void main() {
        fragColor = mix(texture(layer1Texture, uv), texture(layer2Texture, uv), 0.2);
      }
    )";

    auto pipelineInfo = driver->createPipeline();
    pipelineInfo.Shaders = driver->createResource(std::move(shaderInfo));
    pipelineInfo.Rasterizer.CullMode = E_CULL_MODE::FRONT_FACE;
    pipelineInfo.Rasterizer.FrontFaceMode = E_FRONT_FACE_MODE::COUNTER_CLOCKWISE;
    pipelineInfo.VertexBindingCount = 0;

    auto pipeline = driver->createResource(std::move(pipelineInfo));
    screenNode->setPipeline(pipeline);
    smgr->registerNode(screenNode, compositionPass);
  }

  constexpr int oitLayerCount = 2;
  auto Then = std::chrono::high_resolution_clock::now();
  while (device->run()) {
    if (device->isWindowActive()) {
      smgr->animate();

      auto& renderPass = driver->getRenderPass(pass);

      for (int i = 0; i < oitLayerCount; ++i)
      {
        if (i == 0)
        {
          renderPass.DepthStencilAttachment = depthBuffer1;
          renderPass.ColorAttachments[0] = colorBuffer1;
          renderPass.UpdateAttachments = true;
          renderPass.State.Depth = { E_ATTACHMENT_STATE::CLEAR, 1.f };
          driver->begin();
          driver->beginPass(pass);

          CameraMatrix matrix;
          matrix.View = cam->getViewMatrix();
          matrix.Projection = cam->getProjectionMatrix();
          driver->updateShaderUniform(cameraUniform, &matrix);
          driver->bindShaderUniform(cameraUniform, 0);
          // just to disable Vulkan Validation Layer's warning of used texture in shader but not being bound
          driver->bindTexture(colorBuffer3, 2);

          driver->draw();
          driver->endPass();
          driver->end();
          driver->submit();
        }
        else if (i == 1)
        {
          renderPass.DepthStencilAttachment = depthBuffer2;
          renderPass.ColorAttachments[0] = colorBuffer2;
          renderPass.UpdateAttachments = true;
          driver->begin();
          driver->beginPass(pass);

          CameraMatrix matrix;
          matrix.View = cam->getViewMatrix();
          matrix.Projection = cam->getProjectionMatrix();
          driver->updateShaderUniform(cameraUniform, &matrix);
          driver->bindShaderUniform(cameraUniform, 0);
          driver->bindTexture(depthBuffer1, 2);

          driver->draw();
          driver->endPass();
          driver->end();
          driver->submit();
        }
        else if (i == 2)
        {
          renderPass.DepthStencilAttachment = depthBuffer1;
          renderPass.ColorAttachments[0] = colorBuffer2;
          renderPass.UpdateAttachments = true;
          driver->begin();
          driver->beginPass(pass);

          CameraMatrix matrix;
          matrix.View = cam->getViewMatrix();
          matrix.Projection = cam->getProjectionMatrix();
          driver->updateShaderUniform(cameraUniform, &matrix);
          driver->bindShaderUniform(cameraUniform, 0);
          driver->bindTexture(depthBuffer2, 2);

          driver->draw();
          driver->endPass();
          driver->end();
          driver->submit();
        }

        driver->begin();
        driver->beginPass(compositionPass);

        driver->bindTexture(colorBuffer1, 0);
        driver->bindTexture(colorBuffer2, 1);

        driver->draw();
        driver->endPass();
        driver->end();
        driver->submit();
        /*
        driver->begin();
        driver->beginPass(pass);

        driver->bindTexture(colorBuffer1, 0);
        driver->bindTexture(colorBuffer2, 1);

        driver->draw();
        driver->endPass();
        driver->end();
        driver->submit();
        */
      }

      driver->begin();
      driver->blitTexture(colorBuffer3, screen);
      driver->end();
      driver->submit();
      driver->present(screen);
    } else
      device->yield();

    auto Now = std::chrono::high_resolution_clock::now();
    auto millis = std::chrono::duration_cast<std::chrono::milliseconds>(Now - Then).count();
    Then = Now;
    if (millis > 0)
    {
      auto sec = millis / 1000.f;
      int fps = 60.f / sec;
      device->setWindowCaption("Saga 3D Depth Peelilng OIT - FPS: " + std::to_string(fps));
    }
  }

  return 0;
}

