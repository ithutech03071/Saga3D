#include <Saga.h>
#include <chrono>
#include <iostream>

using namespace saga;
using namespace core;
using namespace video;

struct CameraMatrix {
  glm::mat4 View;
  glm::mat4 Projection;
  glm::mat4 ExtraRotation;
};

int main(int argc, char* argv[]) {
  auto device = createDevice(E_DRIVER_TYPE::VULKAN, {1280, 720}, 16, false, false, false);
  auto& driver = device->getVideoDriver();
  auto& smgr = device->getSceneManager();

  auto& cam = smgr->addCameraSceneNodeFPS(nullptr, 0.5, 2.0);
  cam->setPosition({ 0, 0, -10 });
  cam->setTarget({ 0, 10, 0 });
  cam->setFarValue(256.0f);

  auto mesh = smgr->getMesh("../media/wolf.x");
  auto texture = driver->createTexture("../media/wolf.jpg");

  auto skyMesh = std::make_shared<scene::SMesh>();
  auto meshBuffer = std::make_unique<scene::CGPUMeshBuffer>();
  meshBuffer->isNull(true);
  meshBuffer->setVertexCount(3);
  meshBuffer->setPrimitiveType(saga::scene::E_PRIMITIVE_TYPE::TRIANGLES);
  skyMesh->addMeshBuffer(std::move(meshBuffer));

  auto skyTextureInfo = driver->createTexture();
  driver->loadTexture(skyTextureInfo, 0, 0, "../media/desertsky_rt.tga");
  driver->loadTexture(skyTextureInfo, 1, 0, "../media/desertsky_lf.tga");
  driver->loadTexture(skyTextureInfo, 2, 0, "../media/desertsky_up.tga");
  driver->loadTexture(skyTextureInfo, 3, 0, "../media/desertsky_dn.tga");
  driver->loadTexture(skyTextureInfo, 4, 0, "../media/desertsky_bk.tga");
  driver->loadTexture(skyTextureInfo, 5, 0, "../media/desertsky_ft.tga");
  skyTextureInfo.Type = E_TEXTURE_TYPE::CUBE_MAP;
  auto skyTexture = driver->createTexture(std::move(skyTextureInfo));
  auto sky = smgr->createSceneNode(skyMesh);

  auto skyboxMatrixInfo = driver->createShaderUniform();
  skyboxMatrixInfo.Size = sizeof(CameraMatrix);
  auto skyboxMatrix = driver->createResource(std::move(skyboxMatrixInfo));

  {
    auto shaderInfo = driver->createShader();
    shaderInfo.VSSource = R"(
      #version 450

      layout (location = 0) out vec2 NDC;

      void main() {
        vec4 pos = (vec4[3](vec4(-3.0,1.0,1.0,1.0),vec4(0.0,-2.0,1.0,1.0),vec4(3.0,1.0,1.0,1.0)))[gl_VertexIndex]; // drawing on far plane trick
        gl_Position = pos;
        NDC = pos.xy;
      }
    )";

    shaderInfo.FSSource = R"(
      #version 450

      layout (binding = 0) uniform SkyBoxMatrix
      {
        mat4 ViewInverse;
        mat4 ViewProjInverse;
        mat4 ExtraRotation;
      } sky;

      layout (location = 0) in vec2 NDC;
      layout (location = 0) out vec4 fragColor;
      layout (binding = 1) uniform samplerCube cubeMap;

      void main() {
        vec4 tmp = sky.ViewProjInverse[0] * NDC.x + sky.ViewProjInverse[1] * NDC.y + sky.ViewProjInverse[2] + sky.ViewProjInverse[3];
        vec3 viewDir = tmp.xyz/tmp.www - sky.ViewInverse[3].xyz; // last column of inverse view matrix is camera position
        fragColor = texture(cubeMap, vec3(sky.ExtraRotation * vec4(viewDir, 1.0)));
      }
    )";

    auto pipelineInfo = driver->createPipeline();
    pipelineInfo.Shaders = driver->createResource(std::move(shaderInfo));
    pipelineInfo.Rasterizer.CullMode = E_CULL_MODE::NONE;

    auto pipeline = driver->createResource(std::move(pipelineInfo));
    sky->setPipeline(pipeline);
  }

  if (!mesh) return 1;
  if (!texture) return 1;
  auto node = smgr->createSceneNode(mesh);
  node->setTexture(0, texture);

  auto shaderInfo = driver->createShader();
  shaderInfo.VSSource = R"(
    #version 450
    layout (location = 0) in vec4 position;
    layout (location = 1) in vec2 texCoord;
    layout (location = 2) in vec4 boneWeights;
    layout (location = 3) in vec4 boneIDs;

    #define MAX_BONE 64

    layout (binding = 0) uniform CameraMatrix
    {
      mat4 View;
      mat4 Projection;
      mat4 ExtraRotation;
    } camera;

    layout (binding = 1) uniform ModelMatrix
    {
      mat4 Model;
    } model;

    layout (binding = 2) uniform Animation
    {
      mat4 Bones[MAX_BONE];
    } animation;

    layout (location = 0) out vec2 uv;

    void main() {
      uv = texCoord;
      mat4 boneTransform = animation.Bones[int(boneIDs.x)] * boneWeights[0];
      boneTransform     += animation.Bones[int(boneIDs.y)] * boneWeights[1];
      boneTransform     += animation.Bones[int(boneIDs.z)] * boneWeights[2];
      boneTransform     += animation.Bones[int(boneIDs.w)] * boneWeights[3];
      gl_Position = camera.Projection * camera.View * model.Model * boneTransform * position;
    }
  )";

  shaderInfo.FSSource = R"(
    #version 450

    layout (binding = 3) uniform sampler2D texture0;
    layout (location = 0) in vec2 uv;
    layout (location = 0) out vec4 fragColor;

    void main() {
      fragColor = texture(texture0, uv);
    }
  )";

  auto pipelineInfo = driver->createPipeline();
  pipelineInfo.Shaders = driver->createResource(std::move(shaderInfo));

  // this model needs front face culling
  pipelineInfo.Rasterizer.CullMode = E_CULL_MODE::FRONT_FACE;

  pipelineInfo.Layout.Attributes[0][0] = {
    E_ATTRIBUTE_TYPE::POSITION,
    E_ATTRIBUTE_FORMAT::FLOAT3,
  };
  pipelineInfo.Layout.Attributes[0][1] = {
    E_ATTRIBUTE_TYPE::TEXTURE_COORDINATE,
    E_ATTRIBUTE_FORMAT::FLOAT2,
  };

  pipelineInfo.Layout.Attributes[0][2] = {
    E_ATTRIBUTE_TYPE::BONE_WEIGHT,
    E_ATTRIBUTE_FORMAT::FLOAT4,
  };

  pipelineInfo.Layout.Attributes[0][3] = {
    E_ATTRIBUTE_TYPE::BONE_ID,
    E_ATTRIBUTE_FORMAT::FLOAT4,
  };

  auto cameraUniformInfo = driver->createShaderUniform();
  cameraUniformInfo.Size = sizeof(CameraMatrix);
  auto cameraUniform = driver->createResource(std::move(cameraUniformInfo));

  auto modelUniformInfo = driver->createShaderUniform();
  modelUniformInfo.Size = sizeof(glm::mat4);
  auto modelUniform = driver->createResource(std::move(modelUniformInfo));

  auto animUniformInfo = driver->createShaderUniform();
  animUniformInfo.Size = sizeof(glm::mat4) * scene::MAX_BONES;
  auto animUniform = driver->createResource(std::move(animUniformInfo));

  auto passInfo = driver->createRenderPass();
  auto pipeline = driver->createResource(std::move(pipelineInfo));
  node->setPipeline(pipeline);

  auto pass = driver->createResource(std::move(passInfo));
  smgr->registerNode(node, pass);

  std::array<glm::mat4, scene::MAX_BONES> animation;
  node->setOnRender([mesh, node, cam, driver, modelUniform, animUniform, cameraUniform, &animation]() {
    CameraMatrix camMatrix;
    camMatrix.View = cam->getViewMatrix();
    camMatrix.Projection = cam->getProjectionMatrix();
    driver->updateShaderUniform(cameraUniform, &camMatrix);
    driver->bindShaderUniform(cameraUniform, 0);

    auto matrix = node->getAbsoluteTransformation();
    driver->updateShaderUniform(modelUniform, &matrix);
    driver->bindShaderUniform(modelUniform, 1);
    int i = 0;
    auto skinnedMesh = std::dynamic_pointer_cast<scene::ISkinnedMesh>(mesh);
    for (auto& mat : animation)
    {
      if (i >= skinnedMesh->getBoneCount())
        break;
      mat = skinnedMesh->getBoneTransform(i++);
    }
    driver->updateShaderUniform(animUniform, &animation);
    driver->bindShaderUniform(animUniform, 2);
    driver->bindTexture(node->getTexture(0), 3);
  });

  smgr->registerNode(sky, pass);

  sky->setOnRender([sky, cam, driver, skyboxMatrix, skyTexture]() {
    CameraMatrix matrix;

    matrix.View = glm::mat3(glm::inverse(cam->getViewMatrix()));
    matrix.Projection = glm::inverse(cam->getProjectionMatrix() * glm::mat4(glm::mat3(cam->getViewMatrix())));
    matrix.ExtraRotation = glm::mat4(1.0f);
    matrix.ExtraRotation = glm::rotate(matrix.ExtraRotation, glm::radians(90.0f), glm::vec3(1.0, 0.0, 0.0));

    driver->updateShaderUniform(skyboxMatrix, &matrix);
    driver->bindShaderUniform(skyboxMatrix, 0);
    driver->bindTexture(skyTexture, 1);
  });

  auto Then = std::chrono::high_resolution_clock::now();
  while (device->run()) {
    if (device->isWindowActive()) {
      smgr->animate();

      driver->begin();
      driver->beginPass(pass);

      driver->draw();
      driver->endPass();
      driver->end();
      driver->submit();
      driver->present();
    } else
      device->yield();

    auto Now = std::chrono::high_resolution_clock::now();
    auto millis = std::chrono::duration_cast<std::chrono::milliseconds>(Now - Then).count();
    Then = Now;
    if (millis > 0)
    {
      auto sec = millis / 1000.f;
      int fps = 60.f / sec;
      device->setWindowCaption("Saga 3D GPU Skinning & Sky- FPS: " + std::to_string(fps));
    }
  }

  return 0;
}

