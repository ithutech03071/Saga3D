#include <Saga.h>
#include <chrono>

using namespace saga;
using namespace core;
using namespace video;

int main(int argc, char* argv[]) {
  auto device = createDevice(E_DRIVER_TYPE::VULKAN, {800, 600}, 16, false, false, false);
  auto& driver = device->getVideoDriver();
  auto& smgr = device->getSceneManager();

  std::vector<scene::S3DVertex> Vertices;

  Vertices.push_back({
    0.0f, 0.5f, 0.5f
  });

  Vertices.push_back({
    0.5f, -0.5f, 0.5f
  });

  Vertices.push_back({
    -0.5f, -0.5f, 0.5f
  });

  std::vector<std::uint32_t> Indices = { 0, 1, 2 };

  auto mesh = std::make_shared<scene::SMesh>();
  auto meshBuffer = std::make_unique<scene::CMeshBuffer>();
  meshBuffer->append(std::move(Vertices), std::move(Indices));
  constexpr auto meshBufferLocation = 0;
  meshBuffer->setBindingLocation(meshBufferLocation);
  meshBuffer->setInstanceCount(3);
  mesh->addMeshBuffer(std::move(meshBuffer));

  auto node = smgr->createSceneNode(mesh);

  auto textureArrayInfo = driver->createTexture();
  driver->loadTexture(textureArrayInfo, 0, 0, "../media/texture_array/red.png");
  driver->loadTexture(textureArrayInfo, 1, 0, "../media/texture_array/green.png");
  driver->loadTexture(textureArrayInfo, 2, 0, "../media/texture_array/yellow.png");
  textureArrayInfo.Type = E_TEXTURE_TYPE::TEXTURE_2D_ARRAY;
  auto textureArray = driver->createTexture(std::move(textureArrayInfo));

  auto shaderInfo = driver->createShader();
  shaderInfo.VSSource = R"(
    #version 450
    layout (location = 0) in vec4 position;
    layout (location = 0) out int out_index;

    void main() {
      vec4 pos = position;

      // translate triangle instances on X axis
      pos.x += gl_InstanceIndex - 1;

      // scale for smaller triangles
      pos.x *= 0.5;
      pos.y *= 0.5;
      pos.z *= 0.5;

      gl_Position = pos;
      out_index = gl_InstanceIndex;
    }
  )";

  shaderInfo.FSSource = R"(
    #version 450
    layout (binding = 0) uniform sampler2DArray samplerArray;
    layout (location = 0) flat in int index;
    layout (location = 0) out vec4 fragColor;

    void main() {
      fragColor = texture(samplerArray, vec3(0, 0, index));
    }
  )";

  auto pipelineInfo = driver->createPipeline();
  pipelineInfo.Shaders = driver->createResource(std::move(shaderInfo));

  pipelineInfo.Layout.Attributes[meshBufferLocation][0] = {
    E_ATTRIBUTE_TYPE::POSITION,
    E_ATTRIBUTE_FORMAT::FLOAT3,
  };

  auto pipeline = driver->createResource(std::move(pipelineInfo));
  node->setPipeline(pipeline);

  auto passInfo = driver->createRenderPass();
  auto pass = driver->createResource(std::move(passInfo));
  smgr->registerNode(node, pass);

  // push constant is only updated when node's pipeline is bound, so we use OnRender hook
  node->setOnRender([node, driver, textureArray]() {
    driver->bindTexture(textureArray, 0);
  });

  auto Then = std::chrono::high_resolution_clock::now();
  while (device->run()) {
    if (device->isWindowActive()) {
      driver->begin();
      driver->beginPass(pass);
      driver->draw();
      driver->endPass();
      driver->end();
      driver->submit();
      driver->present();
    } else
      device->yield();

    auto Now = std::chrono::high_resolution_clock::now();
    auto millis = std::chrono::duration_cast<std::chrono::milliseconds>(Now - Then).count();
    Then = Now;
    if (millis > 0)
    {
      auto sec = millis / 1000.f;
      int fps = 60.f / sec;
      device->setWindowCaption("Saga 3D Texture Array - FPS: " + std::to_string(fps));
    }
  }

  return 0;
}

