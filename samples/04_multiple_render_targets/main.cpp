#include <Saga.h>
#include <chrono>
#include <fstream>
#include <iostream>

using namespace saga;
using namespace core;
using namespace video;

struct CameraMatrix {
  glm::mat4 View;
  glm::mat4 Projection;
};

int main(int argc, char* argv[]) {
  auto device = createDevice(E_DRIVER_TYPE::VULKAN, {1280, 720}, 16, false, false, false);
  auto& driver = device->getVideoDriver();
  auto& smgr = device->getSceneManager();

  // auto& cam = smgr->addCameraSceneNodeFPS();
  auto& cam = smgr->addCameraSceneNode();
  cam->setPosition({-5, 0, -5});
  cam->setTarget({-5, 0, 0});

  auto mesh = smgr->getMesh("../media/sibenik.x");
  auto texture = driver->createTexture("../media/sibenik_albedo.jpg");
  if (!mesh) return 1;
  if (!texture) return 1;
  auto node = smgr->createSceneNode(mesh);
  node->setTexture(0, texture);

  auto shaderInfo = driver->createShader();
  shaderInfo.VSSource = R"(
    #version 450
    layout (location = 0) in vec4 position;
    layout (location = 1) in vec2 texCoord;

    layout (binding = 0) uniform CameraMatrix
    {
      mat4 View;
      mat4 Projection;
    } camera;

    layout (binding = 1) uniform ModelMatrix
    {
      mat4 Model;
    } model;

    layout (location = 0) out vec4 out_pos;
    layout (location = 1) out vec2 out_uv;

    void main() {
      out_uv = texCoord;
      out_pos = camera.Projection * camera.View * model.Model * position;
      gl_Position = camera.Projection * camera.View * model.Model * position;
    }
  )";

  shaderInfo.FSSource = R"(
    #version 450

    layout (binding = 2) uniform sampler2D texture0;
    layout (location = 0) in vec4 out_position;
    layout (location = 1) in vec2 out_uv;
    layout (location = 0) out vec4 position;
    layout (location = 1) out vec4 albedo;

    void main() {
      position = out_position;
      albedo = texture(texture0, out_uv);
    }
  )";

  auto pipelineInfo = driver->createPipeline();
  pipelineInfo.Shaders = driver->createResource(std::move(shaderInfo));

  pipelineInfo.Layout.Attributes[0][0] = {
    E_ATTRIBUTE_TYPE::POSITION,
    E_ATTRIBUTE_FORMAT::FLOAT3,
  };
  pipelineInfo.Layout.Attributes[0][1] = {
    E_ATTRIBUTE_TYPE::TEXTURE_COORDINATE,
    E_ATTRIBUTE_FORMAT::FLOAT2,
  };

  auto cameraUniformInfo = driver->createShaderUniform();
  cameraUniformInfo.Size = sizeof(CameraMatrix);
  auto cameraUniform = driver->createResource(std::move(cameraUniformInfo));

  auto modelUniformInfo = driver->createShaderUniform();
  modelUniformInfo.Size = sizeof(glm::mat4);
  auto modelUniform = driver->createResource(std::move(modelUniformInfo));

  auto pipeline = driver->createResource(std::move(pipelineInfo));
  node->setPipeline(pipeline);

  auto depthTextureInfo = driver->createTexture();
  depthTextureInfo.Format = E_PIXEL_FORMAT::DEPTH_32;
  depthTextureInfo.IsRenderTarget = true;
  depthTextureInfo.IsDepthAttachment = true;
  depthTextureInfo.Width = driver->getWidth();
  depthTextureInfo.Height = driver->getHeight();

  auto renderTargetInfo = driver->createTexture();
  renderTargetInfo.Format = E_PIXEL_FORMAT::RGBA8;
  renderTargetInfo.Width = driver->getWidth();
  renderTargetInfo.Height = driver->getHeight();
  renderTargetInfo.IsRenderTarget = true;

  auto passInfo = driver->createRenderPass();
  passInfo.UseDefaultAttachments = false;
  passInfo.DepthStencilAttachment = driver->createTexture(std::move(depthTextureInfo));
  passInfo.ColorAttachments[0] = driver->createTexture(STexture{renderTargetInfo});
  passInfo.ColorAttachments[1] = driver->createTexture(STexture{renderTargetInfo});

  auto resultTexture = driver->createTexture(STexture{renderTargetInfo});
  renderTargetInfo.Format = E_PIXEL_FORMAT::BGRA8;
  auto screen = driver->createTexture(STexture{renderTargetInfo});

  auto screenshotBufferInfo = driver->createShaderBuffer();
  const auto size = sizeof(float) * 4 * driver->getWidth() * driver->getHeight();
  screenshotBufferInfo.Size = size;
  screenshotBufferInfo.ReadBack = true;
  auto screenshotBuffer = driver->createResource(std::move(screenshotBufferInfo));

  passInfo.State.Colors[0] = {
    E_ATTACHMENT_STATE::CLEAR,
    { 0.5f, 0.5f, 0.5f, 1.f }
  };
  passInfo.State.Colors[1] = {
    E_ATTACHMENT_STATE::CLEAR,
    { 0.5f, 0.5f, 0.5f, 1.f }
  };
  passInfo.State.Depth = {
    E_ATTACHMENT_STATE::CLEAR,
    1.f
  };

  auto pass = driver->createResource(std::move(passInfo));
  smgr->registerNode(node, pass);

  node->setOnRender([node, driver, modelUniform]() {
    auto matrix = node->getAbsoluteTransformation();
    driver->updateShaderUniform(modelUniform, &matrix);
    driver->bindShaderUniform(modelUniform, 1);
    driver->bindTexture(node->getTexture(0), 2);
  });

  auto Then = std::chrono::high_resolution_clock::now();
  while (device->run()) {
    if (device->isWindowActive()) {
      smgr->animate();
      driver->begin();
      driver->beginPass(pass);

      CameraMatrix matrix;
      matrix.View = cam->getViewMatrix();
      matrix.Projection = cam->getProjectionMatrix();
      driver->updateShaderUniform(cameraUniform, &matrix);
      driver->bindShaderUniform(cameraUniform, 0);
      driver->draw();

      // driver->copyTexture(
      //   passInfo.ColorAttachments[0], resultTexture, {}, {},
      //   { driver->getWidth() / 3, driver->getHeight() }
      // );
      // driver->copyTexture(
      //   passInfo.ColorAttachments[1], resultTexture, {},
      //   { driver->getWidth() / 3, 0},
      //   { driver->getWidth() / 3, driver->getHeight() }
      // );
      // driver->copyTexture(
      //   passInfo.DepthStencilAttachment, resultTexture, {},
      //   { (driver->getWidth() / 3) * 2, 0},
      //   { driver->getWidth() / 3, driver->getHeight() }
      // );
      // driver->blitTexture(resultTexture, screen);
      
      driver->copyTextureToBuffer(
        passInfo.ColorAttachments[0], screenshotBuffer, {}, {},
        { driver->getWidth(), driver->getHeight() }
      );
      
      auto mem = driver->mapBuffer(screenshotBuffer, 0, size);
      std::ofstream file("out.ppm", std::ios::out | std::ios::binary);

      file << "P6\n" << driver->getWidth() << "\n" << driver->getHeight() << "\n" << 255 << "\n";

      auto data = mem;
      for (uint32_t y = 0; y < driver->getHeight(); y++)
      {
        unsigned int* row = (unsigned int*) data;
        for (uint32_t x = 0; x < driver->getWidth(); x++)
        {
          {
            file.write((char*)row, 3);
            // file.write((char*)row+2, 1);
            // file.write((char*)row+1, 1);
            // file.write((char*)row, 1);
            }
          row++;
        }
        data += 5120;
      }

      driver->unmapBuffer(screenshotBuffer);

      driver->endPass();
      driver->end();
      driver->submit();
      driver->present(screen);
    } else
      device->yield();

    auto Now = std::chrono::high_resolution_clock::now();
    auto millis = std::chrono::duration_cast<std::chrono::milliseconds>(Now - Then).count();
    Then = Now;
    if (millis > 0)
    {
      auto sec = millis / 1000.f;
      int fps = 60.f / sec;
      device->setWindowCaption("Saga 3D MRT - FPS: " + std::to_string(fps));
    }
  }

  return 0;
}

