#include <Saga.h>
#include <chrono>

using namespace saga;
using namespace core;
using namespace video;

struct CameraMatrix {
  glm::mat4 View;
  glm::mat4 Projection;
};

#if TARGET_OS_IPHONE == 1
std::string getResourcePath(NSString* name, NSString* ext) {
  NSString* path = [[NSBundle mainBundle] pathForResource:name ofType:ext];
  return std::string([path UTF8String]);
}
#endif

std::string sibenikMeshPath() {
#if TARGET_OS_IPHONE == 1
  return getResourcePath(@"sibenik", @"x");
#else
  return "../media/sibenik.x";
#endif
}

std::string sibenikTexturePath() {
#if TARGET_OS_IPHONE == 1
  return getResourcePath(@"sibenik_albedo", @"jpg");
#else
  return "../media/sibenik_albedo.jpg";
#endif
}

int main(int argc, char* argv[]) {
  auto device = createDevice(E_DRIVER_TYPE::VULKAN, {1280, 720}, 16, false, false, false);
  auto& driver = device->getVideoDriver();
  auto& smgr = device->getSceneManager();

  // auto& cam = smgr->addCameraSceneNodeFPS();
  auto& cam = smgr->addCameraSceneNodeFPS(nullptr, 0.5, 2.0);
  cam->setPosition({0, 0, -10});
  cam->setTarget({0, 10, 0});

  auto mesh = smgr->getMesh(sibenikMeshPath());
  auto texture = driver->createTexture(sibenikTexturePath());
  if (!mesh) return 1;
  if (!texture) return 1;
  auto node = smgr->createSceneNode(mesh);
  node->setTexture(0, texture);

  auto shaderInfo = driver->createShader();
  shaderInfo.VSSource = R"(
    #version 450
    layout (location = 0) in vec4 position;
    layout (location = 1) in vec2 texCoord;

    layout (binding = 0) uniform CameraMatrix
    {
      mat4 View;
      mat4 Projection;
    } camera;

    layout (binding = 1) uniform ModelMatrix
    {
      mat4 Model;
    } model;

    layout (location = 0) out vec2 uv;

    void main() {
      uv = texCoord;
      gl_Position = camera.Projection * camera.View * model.Model * position;
    }
  )";

  shaderInfo.FSSource = R"(
    #version 450

    layout (binding = 2) uniform sampler2D texture0;
    layout (location = 0) in vec2 uv;
    layout (location = 0) out vec4 fragColor;

    void main() {
      fragColor = texture(texture0, uv);
    }
  )";

  auto pipelineInfo = driver->createPipeline();
  pipelineInfo.Shaders = driver->createResource(std::move(shaderInfo));

  pipelineInfo.Layout.Attributes[0][0] = {
    E_ATTRIBUTE_TYPE::POSITION,
    E_ATTRIBUTE_FORMAT::FLOAT3,
  };
  pipelineInfo.Layout.Attributes[0][1] = {
    E_ATTRIBUTE_TYPE::TEXTURE_COORDINATE,
    E_ATTRIBUTE_FORMAT::FLOAT2,
  };

  auto cameraUniformInfo = driver->createShaderUniform();
  cameraUniformInfo.Size = sizeof(CameraMatrix);
  auto cameraUniform = driver->createResource(std::move(cameraUniformInfo));

  auto modelUniformInfo = driver->createShaderUniform();
  modelUniformInfo.Size = sizeof(glm::mat4);
  auto modelUniform = driver->createResource(std::move(modelUniformInfo));

  auto pipeline = driver->createResource(std::move(pipelineInfo));
  node->setPipeline(pipeline);

  auto passInfo = driver->createRenderPass();
  auto pass = driver->createResource(std::move(passInfo));
  smgr->registerNode(node, pass);

  node->setOnRender([node, driver, modelUniform]() {
    auto matrix = node->getAbsoluteTransformation();
    driver->updateShaderUniform(modelUniform, &matrix);
    driver->bindShaderUniform(modelUniform, 1);
    driver->bindTexture(node->getTexture(0), 2);
  });

  auto Then = std::chrono::high_resolution_clock::now();
  while (device->run()) {
    if (device->isWindowActive()) {
      smgr->animate();
      driver->begin();
      driver->beginPass(pass);

      CameraMatrix matrix;
      matrix.View = cam->getViewMatrix();
      matrix.Projection = cam->getProjectionMatrix();
      driver->updateShaderUniform(cameraUniform, &matrix);
      driver->bindShaderUniform(cameraUniform, 0);

      driver->draw();
      driver->endPass();
      driver->end();
      driver->submit();
      driver->present();
    } else
      device->yield();

    auto Now = std::chrono::high_resolution_clock::now();
    auto millis = std::chrono::duration_cast<std::chrono::milliseconds>(Now - Then).count();
    Then = Now;
    if (millis > 0)
    {
      auto sec = millis / 1000.f;
      int fps = 60.f / sec;
      device->setWindowCaption("Saga 3D Mesh - FPS: " + std::to_string(fps));
    }
  }

  return 0;
}

