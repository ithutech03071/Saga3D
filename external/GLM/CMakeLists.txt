add_library(Dependencies::GLM INTERFACE IMPORTED GLOBAL)

target_include_directories(Dependencies::GLM
  INTERFACE ${CMAKE_CURRENT_SOURCE_DIR}/glm-0.9.9.7/
)
