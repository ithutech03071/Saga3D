find_package(Assimp REQUIRED)

add_library(Dependencies::Assimp INTERFACE IMPORTED GLOBAL)

if (UNIX AND NOT APPLE)
  target_include_directories(Dependencies::Assimp
    INTERFACE /usr/include/assimp
  )
else()
  target_include_directories(Dependencies::Assimp
    INTERFACE ${ASSIMP_INCLUDE_DIRS}
  )
endif()

set_property(TARGET Dependencies::Assimp PROPERTY INTERFACE_LINK_DIRECTORIES ${ASSIMP_LIBRARY_DIRS})
set_property(TARGET Dependencies::Assimp PROPERTY INTERFACE_LINK_LIBRARIES assimp${ASSIMP_LIBRARY_SUFFIX})
