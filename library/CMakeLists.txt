add_library(saga_obj OBJECT)

if (NOT MSVC)
  target_compile_options(saga_obj PRIVATE -pipe -Wall -Wno-switch -Wno-format -fmax-errors=5 -Wno-return-type)
endif()

set(SAGA_VERSION_MAJOR 1)
set(SAGA_VERSION_MINOR 0)
set(SAGA_VERSION_PATCH 0)

configure_file(SagaConfig.h.in include/SagaConfig.h @ONLY)

target_sources(saga_obj
  PUBLIC "${CMAKE_CURRENT_SOURCE_DIR}/include/Saga.h"
  INTERFACE "${CMAKE_CURRENT_SOURCE_DIR}/include/SagaDevice.h"
  INTERFACE "${CMAKE_CURRENT_SOURCE_DIR}/include/IEventReceiver.h"

  PRIVATE source/Saga.cpp
  PRIVATE source/CSagaDeviceSDL.cpp
  PRIVATE source/CSagaDeviceStub.cpp
  PRIVATE source/CVideoDriver.cpp
  PRIVATE source/backend/vulkan/VulkanTypes.cpp
  PRIVATE source/EAttributeFormats.cpp
  PRIVATE source/CVulkanDriver.cpp
  PRIVATE source/IMeshBuffer.cpp
  PRIVATE source/CAssimpMeshLoader.cpp
  PRIVATE source/CBoneSceneNode.cpp
  PRIVATE source/CSceneManager.cpp
  PRIVATE source/CCameraSceneNode.cpp
  PRIVATE source/CSceneNodeAnimatorCameraFPS.cpp
  PRIVATE source/CAnimatedMeshSceneNode.cpp
  PRIVATE source/CSkinnedMesh.cpp
)

target_include_directories(saga_obj
  PUBLIC ${CMAKE_CURRENT_BINARY_DIR}/include
  PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/include
  PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/source
)

target_link_libraries(saga_obj PUBLIC
  Dependencies::GLM
  Dependencies::VEZ
  Dependencies::SDL2
  Dependencies::Assimp
  Vulkan::Vulkan
)

if (SAGA_BUILD_DYNAMIC_LIB)
  add_library(saga SHARED)
else()
  add_library(saga STATIC)
endif()

target_link_libraries(saga PUBLIC
  saga_obj
)
add_library(Saga3D::Library ALIAS saga)

#add_custom_target(files
#  PRIVATE README.md
#)

install(
  DIRECTORY include/
  DESTINATION include
)

install(
  DIRECTORY source/
  DESTINATION include/source
  FILES_MATCHING PATTERN "*.h"
)

install(
  DIRECTORY ${CMAKE_BINARY_DIR}/library/include/
  DESTINATION include
)

install(TARGETS saga
  DESTINATION lib
)

FILE(GLOB_RECURSE VEZ_LIB ${CMAKE_BINARY_DIR}/external/VEZ/v-ez/Bin/x86_64/libVEZ*)
install(FILES ${VEZ_LIB}
  DESTINATION bin
)
