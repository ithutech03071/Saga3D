#include "CSceneManager.h"
#include "CAssimpMeshLoader.h"
#include "CAnimatedMeshSceneNode.h"
#include "CCameraSceneNode.h"
#include "CSceneNodeAnimatorCameraFPS.h"
#include <SDL2/SDL.h>
#include <fstream>

namespace saga
{
namespace scene
{

CSceneManager::CSceneManager(const std::shared_ptr<video::IVideoDriver>& driver)
  :  Driver(driver)
{
  MeshLoaderList.push_back(std::make_unique<CAssimpMeshLoader>());
}

CSceneManager::~CSceneManager()
{

}

void CSceneManager::onEvent(const SDL_Event& event)
{
  for (auto& cam : CameraList)
  {
    cam->onEvent(event);
  }
}

void CSceneManager::registerNode(const std::shared_ptr<ISceneNode>& node, video::RenderPassHandle pass)
{
  node->onRegisterSceneNode(pass);
}

void CSceneManager::unregisterNode(const std::shared_ptr<ISceneNode>& node, video::RenderPassHandle pass)
{
  node->onUnregisterSceneNode(pass);
}

const std::shared_ptr<IMesh>& CSceneManager::getMesh(const std::string& fileName)
{
  std::ifstream file(fileName, std::ios::binary | std::ios::ate);
  auto size = file.tellg();
  file.seekg(0, std::ios::beg);

  std::vector<char> buffer(size);
  if (file.read(buffer.data(), size))
  {
    auto extension = fileName.substr(fileName.find_last_of("."));
    return getMesh(buffer.data(), buffer.size(), extension);
  }
  else
    exit(1);
}

const std::shared_ptr<IMesh>& CSceneManager::getMesh(void* data, const std::size_t size,
  const std::string& extension)
{
  std::shared_ptr<IMesh> mesh = nullptr;

  // iterate the list in reverse order so user-added loaders can override the built-in ones
  for (auto& loader : MeshLoaderList)
  {
    if (loader->isSupportedExtension(extension))
    {
      mesh = loader->createMesh(data, size, extension);
      if (mesh)
      {
        const auto id = getMeshID();
        mesh->setID(id);
        MeshCache[id] = mesh;
        return MeshCache[id];
      }
      else
        exit(-1);
    }
  }
}

std::shared_ptr<ISceneNode> CSceneManager::createSceneNode(const std::shared_ptr<IMesh>& mesh, const std::shared_ptr<ISceneNode>& parent,
  const glm::vec3& position, const glm::vec3& rotation, const glm::vec3& scale)
{
  std::shared_ptr<ISceneNode> node = nullptr;
  switch (mesh->getMeshType())
  {
    case E_MESH_TYPE::STATIC:
    {
      node = std::make_shared<IMeshSceneNode>(
        mesh, parent, shared_from_this(), position, rotation, scale
      );
    } break;

    case E_MESH_TYPE::SKINNED:
    {
      node = std::make_shared<CAnimatedMeshSceneNode>(
       std::dynamic_pointer_cast<IAnimatedMesh>(mesh), parent, shared_from_this(), position, rotation, scale
      );
    } break;
  }
  const auto id = getNodeID();
  node->setID(id);
  NodeCache[id] = node;
  return node;
}

const std::shared_ptr<ICameraSceneNode>& CSceneManager::addCameraSceneNode(
  const std::shared_ptr<ISceneNode> &parent, const glm::vec3 &position,
  const glm::vec3 &lookAt, bool makeActive)
{
  auto cam = std::make_shared<CCameraSceneNode>(parent, shared_from_this(), position, lookAt);
  CameraList.push_back(cam);
  if (makeActive)
    setActiveCamera(cam);
  return CameraList.back();
}

const std::shared_ptr<ICameraSceneNode>& CSceneManager::addCameraSceneNodeFPS(
  const std::shared_ptr<ISceneNode>& parent,
  float moveSpeed, float rotateSpeed,
  bool makeActive)
{
  auto cam = std::make_shared<CCameraSceneNode>(parent, shared_from_this());
  auto anim = std::make_shared<CSceneNodeAnimatorCameraFPS>(
    moveSpeed, rotateSpeed
  );
  cam->addAnimator(anim);
  CameraList.push_back(cam);
  if (makeActive)
    setActiveCamera(cam);
  return CameraList.back();
}

void CSceneManager::setActiveCamera(const std::shared_ptr<ICameraSceneNode>& cam)
{
  ActiveCamera = cam;
}

void CSceneManager::animate(const video::RenderPassHandle pass)
{
  const float time = SDL_GetTicks() / 1000.f;
  if (ActiveCamera)
  {
    ActiveCamera->onAnimate(time);
    ActiveCamera->updateViewMatrix();
  }
  for (auto& passIt : NodeList)
  {
    if (passIt.first == pass || pass == video::NULL_GPU_RESOURCE_HANDLE)
    {
      for (auto& node : passIt.second)
      {
        node->onAnimate(time);
        node->updateAbsolutePosition();
      }
    }
  }
}

void CSceneManager::clear() {
  auto List = NodeList;
  for (const auto& it : List) {
    for (const auto& node : List[it.first]) {
      unregisterNode(node, it.first);
    }
  }
  NodeList.clear();
}

} // namespace scene
} // namespace saga
