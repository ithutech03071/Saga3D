// Copyright (C) 2002-2012 Nikolaus Gebhardt
// This file is part of the "Irrlicht Engine".
// For conditions of distribution and use, see copyright notice in irrlicht.h

#ifndef __C_ANIMATED_MESH_SCENE_NODE_H_INCLUDED__
#define __C_ANIMATED_MESH_SCENE_NODE_H_INCLUDED__

#include "IAnimatedMeshSceneNode.h"
#include "IAnimatedMesh.h"

namespace saga
{
namespace scene
{

  class CAnimatedMeshSceneNode : public IAnimatedMeshSceneNode
  {
  public:

    //! constructor
    CAnimatedMeshSceneNode(
      const std::shared_ptr<IAnimatedMesh>& mesh,
      const std::shared_ptr<ISceneNode>& parent,
      const std::shared_ptr<ISceneManager>& mgr,
      const glm::vec3& position = glm::vec3(0,0,0),
      const glm::vec3& rotation = glm::vec3(0,0,0),
      const glm::vec3& scale = glm::vec3(1.0f, 1.0f, 1.0f));

    //! destructor
    virtual ~CAnimatedMeshSceneNode();

    //! sets the current frame. from now on the animation is played from this frame.
    virtual void setCurrentFrame(float frame) override;

    //! onAnimate() is called just before rendering the whole scene.
    virtual void onAnimate(const float time) override;

    //! returns the axis aligned bounding box of this node
    virtual const core::aabbox3d<float>& getBoundingBox() const override;

    //! sets the frames between the animation is looped.
    //! the default is 0 - MaximalFrameCount of the mesh.
    //! NOTE: setMesh will also change this value and set it to the full range of animations of the mesh
    virtual bool setFrameLoop(std::int32_t begin, std::int32_t end) override;

    //! Sets looping mode which is on by default. If set to false,
    //! animations will not be looped.
    virtual void setLoopMode(bool playAnimationLooped) override;

    //! returns the current loop mode
    virtual bool getLoopMode() const override;

    //! Sets a callback interface which will be called if an animation
    //! playback has ended. Set this to 0 to disable the callback again.
    virtual void setAnimationEndCallback(IAnimationEndCallBack* callback= 0) override;

    //! sets the speed with which the animation is played
    //! NOTE: setMesh will also change this value and set it to the default speed of the mesh
    virtual void setAnimationSpeed(float framesPerSecond) override;

    //! gets the speed with which the animation is played
    virtual float getAnimationSpeed() const override;

    //! Returns a pointer to a child node, which has the same transformation as
    //! the corrsesponding joint, if the mesh in this scene node is a skinned mesh.
    // virtual IBoneSceneNode* getJointNode(const char* jointName) override;

    //! same as getJointNode(const char* jointName), but based on id
    // virtual IBoneSceneNode* getJointNode(std::uint32_t jointID) override;

    //! Gets joint count.
    virtual std::uint32_t getBoneCount() const override;

    //! Returns the current displayed frame number.
    virtual float getFrameNumber() const override;

    //! Returns the current start frame number.
    virtual std::int32_t getStartFrame() const override;

    //! Returns the current end frame number.
    virtual std::int32_t getEndFrame() const override;

    //! Returns type of the scene node
    virtual E_SCENE_NODE_TYPE getType() const override { return E_SCENE_NODE_TYPE::ANIMATED_MESH; }

    //! updates the absolute position based on the relative and the parents position
    virtual void updateAbsolutePosition() override;

    //! Set the joint update mode (0-unused, 1-get joints only, 2-set joints only, 3-move and set)
    // virtual void setJointMode(E_JOINT_UPDATE_ON_RENDER mode) override;

    //! Sets the transition time in seconds (note: This needs to enable joints, and setJointmode maybe set to 2)
    //! you must call animateJoints(), or the mesh will not animate
    // virtual void setTransitionTime(float Time) override;

    //! updates the joint positions of this mesh
    // virtual void animateJoints(bool CalculateAbsolutePositions = true) override;

  private:
    std::int32_t StartFrame;
    std::int32_t EndFrame;
    float FramesPerSecond;
    float CurrentFrameNumber;

    std::uint32_t LastTimeMs;
    // std::uint32_t TransitionTimeMs;
    // float Transiting;
    // float TransitingBlend;

    // E_JOINT_UPDATE_ON_RENDER JointMode;
    // bool JointsUsed;

    // bool Looping;

    // IAnimationEndCallBack* LoopCallBack;
  };

} // namespace scene
} // namespace saga

#endif

