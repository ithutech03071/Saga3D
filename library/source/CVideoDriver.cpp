#include "CVideoDriver.h"
#include "SRenderPassState.h"
#include "SRenderPass.h"
#include "EShaderTypes.h"
#include "SShader.h"
#include "SShaderUniform.h"
#include "ICameraSceneNode.h"
#include <stb_image.h>
#include <fstream>
#include <streambuf>

namespace saga
{
namespace video
{

CVideoDriver::CVideoDriver(const SDeviceCreationParameters& params)
  :  CreationParams(params)
{

}

CVideoDriver::~CVideoDriver()
{

}

SRenderPass CVideoDriver::createRenderPass() const
{
  SRenderPass pass;
  pass.Viewport = { 0.f, 0.f, static_cast<float>(getWidth()), static_cast<float>(getHeight()) };
  return pass;
}

SPipeline CVideoDriver::createPipeline() const
{
  return {};
}

STexture CVideoDriver::createTexture()
{
  return {};
}

TextureHandle CVideoDriver::createTexture(unsigned char* fileData, const std::size_t size)
{
  STexture texture;
  int channels;
  auto pixelData = stbi_load_from_memory(fileData, size, &texture.Width, &texture.Height, &channels, 4);
  auto& textureData = texture.Contents[0][0].Data;
  auto memSize = texture.Width * texture.Height * sizeof(char) * 4;
  textureData.resize(memSize);
  memcpy(textureData.data(), pixelData, memSize);
  stbi_image_free(pixelData);
  texture.Format = E_PIXEL_FORMAT::RGBA8;
  const auto id = SceneManager->getTextureID();
  auto handle = Handle++;
  texture.TextureID = id;
  texture.Handle = handle;
  TextureIDMap[id] = handle;
  Textures[handle] = std::move(texture);
  return handle;
}

TextureHandle CVideoDriver::createTexture(unsigned char* pixelData, const int width, const int height, const char channels, const E_PIXEL_FORMAT format)
{
  int bitWidth = 0;
  switch (format) {
    case E_PIXEL_FORMAT::RGBA8: bitWidth = 8; break;
    case E_PIXEL_FORMAT::RGBA16F: bitWidth = 16; break;
    case E_PIXEL_FORMAT::RGBA32F: bitWidth = 32; break;
  }

  STexture texture;
  texture.Width = width;
  texture.Height = height;
  auto memSize = texture.Width * texture.Height * channels * (bitWidth / 8);
  auto& textureData = texture.Contents[0][0].Data;
  textureData.resize(memSize);
  memcpy(textureData.data(), pixelData, memSize);
  texture.Format = format;
  const auto id = SceneManager->getTextureID();
  auto handle = Handle++;
  texture.TextureID = id;
  texture.Handle = handle;
  TextureIDMap[id] = handle;
  Textures[handle] = std::move(texture);
  return handle;
}

TextureHandle CVideoDriver::createTexture(const std::string& path)
{
  std::ifstream file(path, std::ios::binary | std::ios::ate);
  auto size = file.tellg();
  file.seekg(0, std::ios::beg);

  std::vector<unsigned char> buffer(size);
  if (file.read((char*) buffer.data(), size))
  {
    return createTexture(buffer.data(), size);
  }
  else
  {
    return NULL_GPU_RESOURCE_HANDLE;
  }
}

void CVideoDriver::loadTexture(STexture& texture, const int face, const int level, const std::string& path) const
{
  std::ifstream file(path, std::ios::binary | std::ios::ate);
  auto size = file.tellg();
  file.seekg(0, std::ios::beg);

  std::vector<unsigned char> buffer(size);
  if (file.read((char*) buffer.data(), size))
  {
    return loadTexture(texture, face, level, buffer.data(), size);
  }
}

void CVideoDriver::loadTexture(STexture& texture, const int face, const int level,
  const unsigned char* data, const size_t size) const
{
  int channels;
  auto pixelData = stbi_load_from_memory(data, size, &texture.Width, &texture.Height, &channels, 4);
  auto& textureData = texture.Contents[face][level].Data;
  auto memSize = texture.Width * texture.Height * sizeof(char) * 4;
  textureData.resize(memSize);
  memcpy(textureData.data(), pixelData, memSize);
  stbi_image_free(pixelData);
  texture.Format = E_PIXEL_FORMAT::RGBA8;
}

SShader CVideoDriver::createShader() const
{
  return {};
}

SShaderUniform CVideoDriver::createShaderUniform() const
{
  return {};
}

SPushConstant CVideoDriver::createPushConstant() const
{
  return {};
}

SShaderBuffer CVideoDriver::createShaderBuffer() const
{
  return {};
}

SIndirectBuffer CVideoDriver::createIndirectBuffer()
{
  return {};
}

SIndexedIndirectBuffer CVideoDriver::createIndexedIndirectBuffer()
{
  return {};
}

TextureHandle CVideoDriver::createTexture(STexture&& texture)
{
  texture.TextureID = SceneManager->getTextureID();
  auto handle = Handle++;
  texture.Handle = handle;
  TextureIDMap[texture.TextureID] = handle;
  Textures[handle] = std::move(texture);
  return handle;
}

void CVideoDriver::destroyTexture(const TextureHandle texture)
{
  TextureIDMap.erase(Textures.at(texture).TextureID);
  Textures.erase(texture);
}

void CVideoDriver::addShader(SShader& shader, E_SHADER_TYPE type, std::string&& source) const
{
  switch (type)
  {
    case E_SHADER_TYPE::VERTEX: shader.VSSource = std::move(source); break;
    case E_SHADER_TYPE::GEOMETRY: shader.GSSource = std::move(source); break;
    case E_SHADER_TYPE::FRAGMENT: shader.FSSource = std::move(source); break;
    case E_SHADER_TYPE::COMPUTE: shader.CSource = std::move(source); break;
  }
}

void CVideoDriver::addShaderFromFile(SShader& shader, E_SHADER_TYPE type, const std::string& path) const
{
  std::ifstream file(path);
  std::string source(
    (std::istreambuf_iterator<char>(file)),
    std::istreambuf_iterator<char>()
  );
  switch (type)
  {
    case E_SHADER_TYPE::VERTEX: shader.VSSource = std::move(source); break;
    case E_SHADER_TYPE::GEOMETRY: shader.GSSource = std::move(source); break;
    case E_SHADER_TYPE::FRAGMENT: shader.FSSource = std::move(source); break;
    case E_SHADER_TYPE::COMPUTE: shader.CSource = std::move(source); break;
  }
}

RenderPassHandle CVideoDriver::createResource(SRenderPass&& pass)
{
  pass.Handle = Handle;
  RenderPasses[Handle] = std::move(pass);
  return Handle++;
}

PipelineHandle CVideoDriver::createResource(SPipeline&& pipeline)
{
  pipeline.Handle = Handle;
  Pipelines[Handle] = std::move(pipeline);
  return Handle++;
}

ShaderHandle CVideoDriver::createResource(SShader&& shader)
{
  shader.Handle = Handle;
  Shaders[Handle] = std::move(shader);
  return Handle++;
}

ShaderUniformHandle CVideoDriver::createResource(SShaderUniform&& uniform)
{
  uniform.Handle = Handle;
  ShaderUniforms[Handle] = std::move(uniform);
  return Handle++;
}

PushConstantHandle CVideoDriver::createResource(SPushConstant&& con)
{
  con.Handle = Handle;
  PushConstants[Handle] = std::move(con);
  return Handle++;
}

ShaderBufferHandle CVideoDriver::createResource(SShaderBuffer&& buffer)
{
  buffer.Handle = Handle;
  ShaderBuffers[Handle] = std::move(buffer);
  return Handle++;
}

IndirectBufferHandle CVideoDriver::createResource(SIndirectBuffer&& buffer)
{
  if (buffer.Commands.empty() == false)
    buffer.Count = buffer.Commands.size();
  buffer.Handle = Handle;
  IndirectBuffers[Handle] = std::move(buffer);
  return Handle++;
}

IndexedIndirectBufferHandle CVideoDriver::createResource(SIndexedIndirectBuffer&& buffer)
{
  if (buffer.Commands.empty() == false)
    buffer.Count = buffer.Commands.size();
  buffer.Handle = Handle;
  IndexedIndirectBuffers[Handle] = std::move(buffer);
  return Handle++;
}

void CVideoDriver::beginPass(RenderPassHandle pass)
{
  CurrentPass = pass;
}

void CVideoDriver::endPass()
{
  CurrentPass = NULL_GPU_RESOURCE_HANDLE;
}

void CVideoDriver::render()
{
  for (const auto pass : RenderPassQueue)
  {
    beginPass(pass);
    draw();
    endPass();
  }
  submit();
  RenderPassQueue.clear();
}

void CVideoDriver::clear() {
  for (const auto& id : RenderPasses) {
    if (Persistence.count(id.first) == 0)
      destroyRenderPass(id.first);
  }
  for (const auto& id : Pipelines) {
    if (Persistence.count(id.first) == 0)
      destroyPipeline(id.first);
  }
  for (const auto& id : Shaders) {
    if (Persistence.count(id.first) == 0)
      destroyShader(id.first);
  }
  for (const auto& id : ShaderUniforms) {
    if (Persistence.count(id.first) == 0)
      destroyShaderUniform(id.first);
  }
  for (const auto& id : PushConstants) {
    if (Persistence.count(id.first) == 0)
      destroyPushConstant(id.first);
  }
  for (const auto& id : ShaderBuffers) {
    if (Persistence.count(id.first) == 0)
      destroyShaderBuffer(id.first);
  }
  for (const auto& id : Textures) {
    if (Persistence.count(id.first) == 0)
      destroyTexture(id.first);
  }
  for (const auto& id : IndirectBuffers) {
    if (Persistence.count(id.first) == 0)
      destroyIndirectBuffer(id.first);
  }
  for (const auto& id : IndexedIndirectBuffers) {
    if (Persistence.count(id.first) == 0)
      destroyIndexedIndirectBuffer(id.first);
  }
}

void CVideoDriver::setPersistence(const SGPUResource::HandleType resource, const bool persistence) {
  if (persistence) {
    Persistence[resource] = persistence;
  } else {
    if (Persistence.count(resource)) {
      Persistence.erase(resource);
    }
  }
}

void CVideoDriver::resetPersistence() {
  Persistence.clear();
}

} // namespace video
} // namespace saga

