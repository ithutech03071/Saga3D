#ifndef __C_SAGA_DEVICE_SDL_H_INCLUDED__
#define __C_SAGA__DEVICE_SDL_H_INCLUDED__

#include "SagaDevice.h"
#include "CSagaDeviceStub.h"
#include <SDL2/SDL.h>
#include <SDL2/SDL_syswm.h>

namespace saga
{
  class CSagaDeviceSDL : public CSagaDeviceStub
  {
  public:

    //! constructor
    CSagaDeviceSDL(const SDeviceCreationParameters& param);

    //! destructor
    virtual ~CSagaDeviceSDL();

    //! runs the device. Returns false if device wants to be deleted
    virtual bool run() override;

    //! pause execution temporarily
    virtual void yield() override;

    //! pause execution for a specified time
    virtual void sleep(std::uint32_t timeMs) override;

    //! Returns time elapsed in milliseconds since initialization
    /** \return Time in milliseconds  */
    virtual std::uint32_t getTime() const override;

    //! sets the caption of the window
    virtual void setWindowCaption(const std::string& text) override;

    //! returns if window is active. if not, nothing need to be drawn
    virtual bool isWindowActive() const override;

    //! returns if window has focus.
    bool isWindowFocused() const override;

    //! returns if window is minimized.
    bool isWindowMinimized() const override;

    //! notifies the device that it should close itself
    virtual void closeDevice() override;

    //! Sets if the window should be resizable in windowed mode.
    virtual void setResizable(bool resize=false) override;

    //! Minimizes the window.
    virtual void minimizeWindow() override;

    //! Maximizes the window.
    virtual void maximizeWindow() override;

    //! Restores the window size.
    virtual void restoreWindow() override;

    //! Get the position of this window on screen
    virtual glm::ivec2 getWindowPosition() override;

    SDL_Window* getSDLWindow() const { return Window; }

    //! Get window's width
    virtual std::uint32_t getWidth() const override { return Width; }
    
    //! Get window's height
    virtual std::uint32_t getHeight() const override { return Height; }

    //! Get aspect ratio
    virtual float getAspectRatio() const override { return AspectRatio; }

    //! Resize the render window.
    /**  This will only work in windowed mode and is not yet supported on all systems.
    It does set the drawing/clientDC size of the window, the window decorations are added to that.
    You get the current window size with IVideoDriver::getScreenSize() (might be unified in future)
    */
    virtual void setWindowSize(const glm::uvec2& size) override {}

  private:

    //! create the driver
    void createDriver();
    bool createWindow();

    SDL_Surface* Screen;
    int SDL_Flags;

    std::uint32_t Width, Height;
    float AspectRatio;

    bool Resizable;
    bool WindowHasFocus;
    bool WindowMinimized;

    SDL_SysWMinfo Info;
    SDL_Window* Window;
};

} // namespace saga

#endif // __C_IRR_DEVICE_SDL_H_INCLUDED__

