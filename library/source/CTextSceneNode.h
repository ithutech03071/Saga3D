// Copyright (C) 2002-2012 Nikolaus Gebhardt
// This file is part of the "Irrlicht Engine".
// For conditions of distribution and use, see copyright notice in irrlicht.h

#ifndef __C_TEXT_SCENE_NODE_H_INCLUDED__
#define __C_TEXT_SCENE_NODE_H_INCLUDED__

#include "ITextSceneNode.h"
#include "IBillboardTextSceneNode.h"
#include "ISceneCollisionManager.h"
#include "SMesh.h"

namespace saga
{
namespace scene
{

  class ICameraSceneNode;

  class CTextSceneNode : public ITextSceneNode
  {
  public:

    //! constructor
    CTextSceneNode(ISceneNode* parent, ISceneManager* mgr, std::int32_t id,
      gui::IGUIFont* font, scene::ISceneCollisionManager* coll,
      const glm::vec3& position = glm::vec3(0,0,0), const char* text= 0,
      video::SColor color=video::SColor(100,0,0,0));

    //! destructor
    virtual ~CTextSceneNode();

    virtual void onRegisterSceneNode(video::RenderPassHandle pass) override;

    //! renders the node.
    virtual void render() override;

    //! returns the axis aligned bounding box of this node
    virtual const core::aabbox3d<float>& getBoundingBox() const override;

    //! sets the text string
    virtual void setText(const char* text) override;

    //! get the text string
    virtual const char* getText() const override;

    //! sets the color of the text
    virtual void setTextColor(video::SColor color) override;

    //! get the color of the text
    virtual video::SColor getTextColor() const override;

    //! set the font used to draw the text
    virtual void setFont(gui::IGUIFont* font) override;

    //! Get the font used to draw the text
    virtual gui::IGUIFont* getFont() const override;

    //! Returns type of the scene node
    virtual E_SCENE_NODE_TYPE getType() const override { return E_SCENE_NODE_TYPE::TEXT; }

  private:

    std::wstring Text;
    video::SColor Color;
    gui::IGUIFont* Font;
    scene::ISceneCollisionManager* Coll;
    core::aabbox3d<float> Box;
  };

  class CBillboardTextSceneNode : public IBillboardTextSceneNode
  {
  public:

    CBillboardTextSceneNode(ISceneNode* parent, ISceneManager* mgr, std::int32_t id,
      gui::IGUIFont* font,const char* text,
      const glm::vec3& position, const glm::vec2& size,
      video::SColor colorTop, video::SColor shade_bottom);

    //! destructor
    virtual ~CBillboardTextSceneNode();

    //! sets the vertex positions etc
    virtual void onAnimate(const float time) override;

    //! registers the node into the transparent pass
    virtual void onRegisterSceneNode(video::RenderPassHandle pass) override;

    //! renders the node.
    virtual void render() override;

    //! returns the axis aligned bounding box of this node
    virtual const core::aabbox3d<float>& getBoundingBox() const override;

    //! sets the text string
    virtual void setText(const char* text) override;

    //! get the text string
    virtual const char* getText() const override;

    //! Get the font used to draw the text
    virtual gui::IGUIFont* getFont() const override;

    //! sets the size of the billboard
    virtual void setSize(const glm::vec2& size) override;

    //! gets the size of the billboard
    virtual const glm::vec2& getSize() const override;

    // virtual video::SMaterial& getMaterial(std::uint32_t i) override;

    //! returns amount of materials used by this scene node.
    virtual std::uint32_t getMaterialCount() const override;

    //! Returns type of the scene node
    virtual E_SCENE_NODE_TYPE getType() const override { return E_SCENE_NODE_TYPE::BILLBOARD_TEXT; }

    //! Set the color of all vertices of the billboard
    //! \param overallColor: the color to set
    virtual void setColor(const video::SColor & overallColor) override;

    //! Set the color of the top and bottom vertices of the billboard
    //! \param topColor: the color to set the top vertices
    //! \param bottomColor: the color to set the bottom vertices
    virtual void setColor(const video::SColor & topColor, const video::SColor & bottomColor) override;

    //! Gets the color of the top and bottom vertices of the billboard
    //! \param topColor: stores the color of the top vertices
    //! \param bottomColor: stores the color of the bottom vertices
    virtual void getColor(video::SColor & topColor, video::SColor & bottomColor) const override;

    virtual void setSize(float height, float bottomEdgeWidth, float topEdgeWidth) override
    {
      setSize(glm::vec2(bottomEdgeWidth, height));
    }

    virtual void getSize(float& height, float& bottomEdgeWidth, float& topEdgeWidth) const override
    {
      height = Size.Height;
      bottomEdgeWidth = Size.Width;
      topEdgeWidth = Size.Width;
    }

    virtual const core::aabbox3d<float>& getTransformedBillboardBoundingBox(const saga::scene::ICameraSceneNode* camera) override;

  protected:
    void updateMesh(const saga::scene::ICameraSceneNode* camera);

  private:

    std::wstring Text;
    // gui::IGUIFontBitmap* Font;

    glm::vec2 Size;
    core::aabbox3d<float> BBox;
    //video::SMaterial Material;

    video::SColor ColorTop;
    video::SColor ColorBottom;
    struct SSymbolInfo
    {
      std::uint32_t bufNo;
      float Width;
      float Kerning;
      std::uint32_t firstInd;
      std::uint32_t firstVert;
    };

    std::vector < SSymbolInfo > Symbol;

    SMesh *Mesh;
  };

} // namespace scene
} // namespace saga

#endif

