#include "CAssimpMeshLoader.h"
#include "CMeshBuffer.h"
#include "SMesh.h"
#include "SAnimatedMesh.h"
#include "CSkinnedMesh.h"
#include <assimp/Importer.hpp>
#include <assimp/postprocess.h>
#include <assimp/scene.h>

namespace saga
{
namespace scene
{

CAssimpMeshLoader::CAssimpMeshLoader()
{

}

CAssimpMeshLoader::~CAssimpMeshLoader()
{

}

std::shared_ptr<IMesh> CAssimpMeshLoader::createMesh(void* data, const std::size_t size, const std::string& extension)
{
  Assimp::Importer Importer;
  const aiScene* pScene;
  constexpr int flags =
    aiProcess_Triangulate |
    aiProcess_CalcTangentSpace |
    aiProcess_FlipUVs |
    aiProcess_GenSmoothNormals |
    aiProcess_OptimizeMeshes;

  pScene = Importer.ReadFileFromMemory(data, size, flags, extension.c_str());
  if (pScene == nullptr) return nullptr;
  bool isAnimatedMesh = false;
  bool isSkinnedMesh = false;

  if (pScene->HasAnimations())
  {
    isAnimatedMesh = true;
  }

  for (unsigned int i = 0; i < pScene->mNumMeshes; i++)
  {
    const aiMesh* paiMesh = pScene->mMeshes[i];
    if (paiMesh->HasBones())
    {
      isSkinnedMesh = true;
      break;
    }
  }

  uint32_t vertexCount = 0;
  for (uint32_t m = 0; m < pScene->mNumMeshes; m++) {
    vertexCount += pScene->mMeshes[m]->mNumVertices;
  }

  std::shared_ptr<IMesh> mesh;
  if (isSkinnedMesh)
  {
    auto skinnedMesh = std::make_shared<CSkinnedMesh>();
    skinnedMesh->Scene = Importer.GetOrphanedScene();
    skinnedMesh->setAnimation(0);

    skinnedMesh->Bones.resize(vertexCount);
    skinnedMesh->GlobalInverseTransform = pScene->mRootNode->mTransformation;
    skinnedMesh->GlobalInverseTransform.Inverse();
    uint32_t vertexBase(0);
    for (uint32_t m = 0; m < pScene->mNumMeshes; m++) {
      aiMesh* paiMesh = pScene->mMeshes[m];
      if (paiMesh->mNumBones > 0) {
        skinnedMesh->loadBones(paiMesh, vertexBase);
      }
      vertexBase += pScene->mMeshes[m]->mNumVertices;
    }
    mesh = skinnedMesh;
  }
  else if (isAnimatedMesh)
  {
    mesh = std::make_shared<SAnimatedMesh>();
  }
  else
  {
    mesh = std::make_shared<SMesh>();
  }

  std::uint32_t vertexBase = 0;
  std::vector<S3DVertex> vertices;
  std::vector<uint32_t> indices;
  vertices.reserve(vertexCount);
  auto meshBuffer = std::make_unique<CMeshBuffer>();
  for (unsigned int i = 0; i < pScene->mNumMeshes; i++)
  {
    const aiMesh* paiMesh = pScene->mMeshes[i];

    aiColor3D pColor(0.f, 0.f, 0.f);
    pScene->mMaterials[paiMesh->mMaterialIndex]->Get(AI_MATKEY_COLOR_DIFFUSE, pColor);
    static const aiVector3D Zero3D(0.0f, 0.0f, 0.0f);

    for (unsigned int j = 0; j < paiMesh->mNumVertices; j++)
    {
      const aiVector3D* pPos = &(paiMesh->mVertices[j]);
      const aiVector3D* pNormal = &(paiMesh->mNormals[j]);
      const aiVector3D* pTexCoord = (paiMesh->HasTextureCoords(0)) ? &(paiMesh->mTextureCoords[0][j]) : &Zero3D;
      const aiVector3D* pTangent = (paiMesh->HasTangentsAndBitangents()) ? &(paiMesh->mTangents[j]) : &Zero3D;
      const aiVector3D* pBiTangent = (paiMesh->HasTangentsAndBitangents()) ? &(paiMesh->mBitangents[j]) : &Zero3D;

      vertices.push_back({
        { pPos->x, pPos->y, pPos->z },
        { pNormal->x, pNormal->y, pNormal->z },
        { pColor.r, pColor.g, pColor.b, 1.0 },
        { pTexCoord->x, pTexCoord->y },
        { pTangent->x, pTangent->y, pTangent->z },
        { pBiTangent->x, pBiTangent->y, pBiTangent->z },
      });
      if (isSkinnedMesh)
      {
        for (uint32_t k = 0; k < MAX_BONES_PER_VERTEX; k++)
        {
          auto skinnedMesh = std::dynamic_pointer_cast<CSkinnedMesh>(mesh);
          vertices.back().BoneWeights[k] = skinnedMesh->Bones[vertexBase + j].Weights[k];
          vertices.back().BoneIDs[k] = skinnedMesh->Bones[vertexBase + j].IDs[k];
        }
      }
    }
    vertexBase += pScene->mMeshes[i]->mNumVertices;

    uint32_t indexBase = static_cast<uint32_t>(indices.size());
    for (unsigned int j = 0; j < paiMesh->mNumFaces; j++)
    {
      const aiFace& Face = paiMesh->mFaces[j];
      if (Face.mNumIndices != 3)
        continue;

      indices.push_back(Face.mIndices[0] + indexBase);
      indices.push_back(Face.mIndices[1] + indexBase);
      indices.push_back(Face.mIndices[2] + indexBase);
    }
  }
  meshBuffer->append(std::move(vertices), std::move(indices));
  mesh->addMeshBuffer(std::move(meshBuffer));
  return mesh;
}

} // namespace scene
} // namespace saga
