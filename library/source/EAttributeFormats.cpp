#include "EAttributeFormats.h"

namespace saga
{
namespace video
{

uint32_t GetAttributeSize(const E_ATTRIBUTE_FORMAT format)
{
  switch (format)
  {
    case E_ATTRIBUTE_FORMAT::FLOAT:     return 4;
    case E_ATTRIBUTE_FORMAT::FLOAT2:    return 8;
    case E_ATTRIBUTE_FORMAT::FLOAT3:    return 12;
    case E_ATTRIBUTE_FORMAT::FLOAT4:    return 16;
    case E_ATTRIBUTE_FORMAT::BYTE4:     return 4;
    case E_ATTRIBUTE_FORMAT::BYTE4N:    return 4;
    case E_ATTRIBUTE_FORMAT::UBYTE4:    return 4;
    case E_ATTRIBUTE_FORMAT::UBYTE4N:   return 4;
    case E_ATTRIBUTE_FORMAT::SHORT2:    return 4;
    case E_ATTRIBUTE_FORMAT::SHORT2N:   return 4;
    case E_ATTRIBUTE_FORMAT::SHORT4:    return 8;
    case E_ATTRIBUTE_FORMAT::SHORT4N:   return 8;
    case E_ATTRIBUTE_FORMAT::UINT10_N2: return 4;
  }
}

uint32_t GetAttributeComponentSize(const E_ATTRIBUTE_FORMAT format)
{
  switch (format)
  {
    case E_ATTRIBUTE_FORMAT::FLOAT:
    case E_ATTRIBUTE_FORMAT::FLOAT2:
    case E_ATTRIBUTE_FORMAT::FLOAT3:
    case E_ATTRIBUTE_FORMAT::FLOAT4:    return 4;
    case E_ATTRIBUTE_FORMAT::BYTE4:
    case E_ATTRIBUTE_FORMAT::BYTE4N:
    case E_ATTRIBUTE_FORMAT::UBYTE4:
    case E_ATTRIBUTE_FORMAT::UBYTE4N:   return 1;
    case E_ATTRIBUTE_FORMAT::SHORT2:
    case E_ATTRIBUTE_FORMAT::SHORT2N:
    case E_ATTRIBUTE_FORMAT::SHORT4:
    case E_ATTRIBUTE_FORMAT::SHORT4N:   return 2;
    case E_ATTRIBUTE_FORMAT::UINT10_N2: return 4;
  }
}

} // namespace video
} // namespace saga
