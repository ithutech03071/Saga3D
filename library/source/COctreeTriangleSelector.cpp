// Copyright (C) 2002-2012 Nikolaus Gebhardt
// This file is part of the "Irrlicht Engine".
// For conditions of distribution and use, see copyright notice in irrlicht.h

#include "COctreeTriangleSelector.h"
#include "ISceneNode.h"

#include "os.h"

namespace saga
{
namespace scene
{

//! constructor
COctreeTriangleSelector::COctreeTriangleSelector(const IMesh* mesh,
    ISceneNode* node, std::int32_t minimalPolysPerNode)
  : CTriangleSelector(mesh, node, false)
  , Root(0), NodeCount(0)
  , MinimalPolysPerNode(minimalPolysPerNode)
{
  #ifdef _DEBUG
  setDebugName("COctreeTriangleSelector");
  #endif

  if (!Triangles.empty())
  {
    const std::uint32_t start = os::Timer::getRealTime();

    // create the triangle octree
    Root = new SOctreeNode();
    Root->Triangles = Triangles;
    constructOctree(Root);

    char tmp[256];
    sprintf(tmp, "Needed %ums to create OctreeTriangleSelector.(%d nodes, %u polys)",
      os::Timer::getRealTime() - start, NodeCount, Triangles.size());
    os::Printer::log(tmp, ELL_INFORMATION);
  }
}

COctreeTriangleSelector::COctreeTriangleSelector(const IMeshBuffer* meshBuffer, std::uint32_t materialIndex, ISceneNode* node, std::int32_t minimalPolysPerNode)
  : CTriangleSelector(meshBuffer, materialIndex, node)
  , Root(0), NodeCount(0)
  , MinimalPolysPerNode(minimalPolysPerNode)
{
  #ifdef _DEBUG
  setDebugName("COctreeTriangleSelector");
  #endif

  if (!Triangles.empty())
  {
    const std::uint32_t start = os::Timer::getRealTime();

    // create the triangle octree
    Root = new SOctreeNode();
    Root->Triangles = Triangles;
    constructOctree(Root);

    char tmp[256];
    sprintf(tmp, "Needed %ums to create OctreeTriangleSelector.(%d nodes, %u polys)",
      os::Timer::getRealTime() - start, NodeCount, Triangles.size());
    os::Printer::log(tmp, ELL_INFORMATION);
  }
}

//! destructor
COctreeTriangleSelector::~COctreeTriangleSelector()
{
  delete Root;
}


void COctreeTriangleSelector::constructOctree(SOctreeNode* node)
{
  ++NodeCount;

  node->Box.reset(node->Triangles[0].pointA);

  // get bounding box
  const std::uint32_t cnt = node->Triangles.size();
  for (std::uint32_t i = 0; i < cnt; ++i)
  {
    node->Box.addInternalPoint(node->Triangles[i].pointA);
    node->Box.addInternalPoint(node->Triangles[i].pointB);
    node->Box.addInternalPoint(node->Triangles[i].pointC);
  }

  const glm::vec3& middle = node->Box.getCenter();
  glm::vec3 edges[8];
  node->Box.getEdges(edges);

  core::aabbox3d<float> box;
  std::vector<core::triangle3df> keepTriangles;

  // calculate children

  if (!node->Box.isEmpty() && (std::int32_t)node->Triangles.size() > MinimalPolysPerNode)
  for (std::int32_t ch= 0; ch<8; ++ch)
  {
    box.reset(middle);
    box.addInternalPoint(edges[ch]);
    node->Child[ch] = new SOctreeNode();

    for (std::int32_t i = 0; i < (std::int32_t)node->Triangles.size(); ++i)
    {
      if (node->Triangles[i].isTotalInsideBox(box))
      {
        node->Child[ch]->Triangles.push_back(node->Triangles[i]);
        //node->Triangles.erase(i);
        //--i;
      }
      else
      {
        keepTriangles.push_back(node->Triangles[i]);
      }
    }
    memcpy(node->Triangles.pointer(), keepTriangles.pointer(),
      sizeof(core::triangle3df)*keepTriangles.size());

    node->Triangles.set_used(keepTriangles.size());
    keepTriangles.set_used(0);

    if (node->Child[ch]->Triangles.empty())
    {
      delete node->Child[ch];
      node->Child[ch] = 0;
    }
    else
      constructOctree(node->Child[ch]);
  }
}


//! Gets all triangles which lie within a specific bounding box.
void COctreeTriangleSelector::getTriangles(core::triangle3df* triangles,
          std::int32_t arraySize, std::int32_t& outTriangleCount,
          const core::aabbox3d<float>& box,
          const glm::mat4* transform, bool useNodeTransform,
          std::vector<SCollisionTriangleRange>* outTriangleInfo) const
{
  glm::mat4 mat(glm::mat4::EM4CONST_NOTHING);
  core::aabbox3d<float> invbox = box;

  if (SceneNode && useNodeTransform)
  {
    if (SceneNode->getAbsoluteTransformation().getInverse(mat))
      mat.transformBoxEx(invbox);
    else
      // TODO: case not handled well, we can only return all triangles
      return CTriangleSelector::getTriangles(triangles, arraySize, outTriangleCount, transform, useNodeTransform, outTriangleInfo);
  }

  if (transform)
    mat = *transform;
  else
    mat.makeIdentity();

  if (SceneNode && useNodeTransform)
    mat *= SceneNode->getAbsoluteTransformation();

  std::int32_t trianglesWritten = 0;

  if (Root)
    getTrianglesFromOctree(Root, trianglesWritten,
      arraySize, invbox, &mat, triangles);

  if (outTriangleInfo)
  {
    SCollisionTriangleRange triRange;
    triRange.RangeSize = trianglesWritten;
    triRange.Selector = const_cast<COctreeTriangleSelector*>(this);
    triRange.SceneNode = SceneNode;
    triRange.MeshBuffer = MeshBuffer;
    triRange.MaterialIndex = MaterialIndex;
    outTriangleInfo->push_back(triRange);
  }

  outTriangleCount = trianglesWritten;
}


void COctreeTriangleSelector::getTrianglesFromOctree(
    SOctreeNode* node, std::int32_t& trianglesWritten,
    std::int32_t maximumSize, const core::aabbox3d<float>& box,
    const glm::mat4* mat, core::triangle3df* triangles) const
{
  if (!box.intersectsWithBox(node->Box))
    return;

  const std::uint32_t cnt = node->Triangles.size();

  for (std::uint32_t i = 0; i < cnt; ++i)
  {
    const core::triangle3df& srcTri = node->Triangles[i];
    // This isn't an accurate test, but it's fast, and the
    // API contract doesn't guarantee complete accuracy.
    if (srcTri.isTotalOutsideBox(box))
      continue;

    core::triangle3df& dstTri = triangles[trianglesWritten];
    mat->transformVect(dstTri.pointA, srcTri.pointA);
    mat->transformVect(dstTri.pointB, srcTri.pointB);
    mat->transformVect(dstTri.pointC, srcTri.pointC);
    ++trianglesWritten;

    // Halt when the out array is full.
    if (trianglesWritten == maximumSize)
      return;
  }

  for (std::uint32_t i = 0; i < 8; ++i)
    if (node->Child[i])
      getTrianglesFromOctree(node->Child[i], trianglesWritten,
      maximumSize, box, mat, triangles);
}


//! Gets all triangles which have or may have contact with a 3d line.
// new version: from user Piraaate
void COctreeTriangleSelector::getTriangles(core::triangle3df* triangles, std::int32_t arraySize,
    std::int32_t& outTriangleCount, const core::line3d<float>& line,
    const glm::mat4* transform, bool useNodeTransform,
    std::vector<SCollisionTriangleRange>* outTriangleInfo) const
{
#if 0
  core::aabbox3d<float> box(line.start);
  box.addInternalPoint(line.end);

  // TODO: Could be optimized for line a little bit more.
  COctreeTriangleSelector::getTriangles(triangles, arraySize, outTriangleCount,
    box, transform);
#else

  glm::mat4 mat (glm::mat4::EM4CONST_NOTHING);

  glm::vec3 vectStartInv (line.start), vectEndInv (line.end);
  if (SceneNode && useNodeTransform)
  {
    mat = SceneNode->getAbsoluteTransformation();
    mat.makeInverse();
    mat.transformVect(vectStartInv, line.start);
    mat.transformVect(vectEndInv, line.end);
  }
  core::line3d<float> invline(vectStartInv, vectEndInv);

  mat.makeIdentity();

  if (transform)
    mat = (*transform);

  if (SceneNode && useNodeTransform)
    mat *= SceneNode->getAbsoluteTransformation();

  std::int32_t trianglesWritten = 0;

  if (Root)
    getTrianglesFromOctree(Root, trianglesWritten, arraySize, invline, &mat, triangles);

  if (outTriangleInfo)
  {
    SCollisionTriangleRange triRange;
    triRange.RangeSize = trianglesWritten;
    triRange.Selector = const_cast<COctreeTriangleSelector*>(this);
    triRange.SceneNode = SceneNode;
    triRange.MeshBuffer = MeshBuffer;
    triRange.MaterialIndex = MaterialIndex;
    outTriangleInfo->push_back(triRange);
  }

  outTriangleCount = trianglesWritten;
#endif
}

void COctreeTriangleSelector::getTrianglesFromOctree(SOctreeNode* node,
    std::int32_t& trianglesWritten, std::int32_t maximumSize, const core::line3d<float>& line,
    const glm::mat4* transform, core::triangle3df* triangles) const
{
  if (!node->Box.intersectsWithLine(line))
    return;

  std::int32_t cnt = node->Triangles.size();
  if (cnt + trianglesWritten > maximumSize)
    cnt -= cnt + trianglesWritten - maximumSize;

  std::int32_t i;

  if (transform->isIdentity())
  {
    for (i = 0; i < cnt; ++i)
    {
      triangles[trianglesWritten] = node->Triangles[i];
      ++trianglesWritten;
    }
  }
  else
  {
    for (i = 0; i < cnt; ++i)
    {
      triangles[trianglesWritten] = node->Triangles[i];
      transform->transformVect(triangles[trianglesWritten].pointA);
      transform->transformVect(triangles[trianglesWritten].pointB);
      transform->transformVect(triangles[trianglesWritten].pointC);
      ++trianglesWritten;
    }
  }

  for (i = 0; i < 8; ++i)
    if (node->Child[i])
      getTrianglesFromOctree(node->Child[i], trianglesWritten,
      maximumSize, line, transform, triangles);
}


} // namespace scene
} // namespace saga
