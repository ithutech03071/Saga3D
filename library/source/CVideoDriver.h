// Copyright (C) 2002-2012 Nikolaus Gebhardt
// This file is part of the "Irrlicht Engine".
// For conditions of distribution and use, see copyright notice in irrlicht.h

#ifndef __IRR_CVIDEO_DRIVER_H_INCLUDED__
#define __IRR_CVIDEO_DRIVER_H_INCLUDED__

#include "IVideoDriver.h"
#include "SDeviceCreationParameters.h"
#include "ISceneManager.h"
#include <unordered_map>

namespace saga
{
namespace video
{
  class CVideoDriver : public IVideoDriver
  {
  public:
    CVideoDriver(const SDeviceCreationParameters& params);
    virtual ~CVideoDriver();

    virtual void beginPass(RenderPassHandle pass) override;
    virtual void endPass() override;

    virtual void setPersistence(const SGPUResource::HandleType resource, const bool persistence = false) override;
    virtual void resetPersistence() override;
    virtual void clear() override;

    #ifdef SAGA_EXTRA_API_MEMORY_INFO
    std::uint64_t getTotalMemory() const override { return TotalMemory; }
    #endif

    virtual void enqueuePass(const RenderPassHandle pass) override { RenderPassQueue.push_back(pass); }
    virtual void render() override;

    virtual SRenderPass createRenderPass() const override;
    virtual SGPUResource::HandleType createResource(SRenderPass&& pass) override;
    virtual SRenderPass& getRenderPass(const RenderPassHandle pass) override { return RenderPasses[pass]; }
    virtual void destroyRenderPass(const RenderPassHandle pass) override { RenderPasses.erase(pass); }

    virtual SPipeline createPipeline() const override;
    virtual SGPUResource::HandleType createResource(SPipeline&& pipeline) override;
    virtual SPipeline& getPipeline(const PipelineHandle p) override { return Pipelines[p]; }
    virtual void destroyPipeline(const PipelineHandle p) override { Pipelines.erase(p); }

    virtual STexture createTexture() override;
    virtual void loadTexture(STexture& texture, const int face, const int level, const std::string& path) const override;
    virtual void loadTexture(STexture& texture, const int face, const int level,
      const unsigned char* data, const size_t size) const override;

    virtual TextureHandle createTexture(STexture&& texture) override;
    virtual TextureHandle createTexture(const std::string& path) override;
    virtual TextureHandle createTexture(unsigned char* fileData, const std::size_t size) override;
    virtual TextureHandle createTexture(unsigned char* pixelData, const int width, const int height, const char channels, const E_PIXEL_FORMAT format) override;

    virtual STexture& getTexture(const TextureHandle texture) override { return Textures[texture]; }
    virtual STexture& getTextureByID(const ID id) override { return Textures[TextureIDMap[id]]; }
    virtual void destroyTexture(const TextureHandle texture) override;

    virtual SShader createShader() const override;
    virtual ShaderHandle createResource(SShader&& shader) override;
    virtual SShader& getShader(const ShaderHandle shader) override { return Shaders[shader]; }
    virtual void destroyShader(const ShaderHandle shader) override { Shaders.erase(shader); }

    virtual SShaderUniform createShaderUniform() const override;
    virtual ShaderUniformHandle createResource(SShaderUniform&& shader) override;

    virtual SShaderUniform& getShaderUniform(const ShaderUniformHandle uniform) override
    { return ShaderUniforms[uniform]; }
    virtual void destroyShaderUniform(const ShaderUniformHandle uniform) override
    { ShaderUniforms.erase(uniform); }

    virtual SPushConstant createPushConstant() const override;
    virtual PushConstantHandle createResource(SPushConstant&& con) override;
    virtual SPushConstant& getPushConstant(const PushConstantHandle con) override
    { return PushConstants[con]; }
    virtual void destroyPushConstant(const PushConstantHandle con) override
    { PushConstants.erase(con); }

    virtual SShaderBuffer createShaderBuffer() const override;
    virtual ShaderBufferHandle createResource(SShaderBuffer&& buffer) override;
    virtual SShaderBuffer& getShaderBuffer(const ShaderBufferHandle buffer) override
    { return ShaderBuffers[buffer]; }
    virtual void destroyShaderBuffer(const ShaderBufferHandle buffer) override
    { ShaderBuffers.erase(buffer); }

    virtual void addShader(SShader& shader, E_SHADER_TYPE type, std::string&& source) const override;
    virtual void addShaderFromFile(SShader& shader, E_SHADER_TYPE type, const std::string& path) const override;

    virtual SIndirectBuffer createIndirectBuffer() override;
    virtual IndirectBufferHandle createResource(SIndirectBuffer&& buffer) override;
    virtual SIndirectBuffer& getIndirectBuffer(const IndirectBufferHandle buffer) override
    { return IndirectBuffers[buffer]; }
    virtual void destroyIndirectBuffer(const IndirectBufferHandle buffer) override
    { IndirectBuffers.erase(buffer); }

    virtual SIndexedIndirectBuffer createIndexedIndirectBuffer() override;
    virtual IndexedIndirectBufferHandle createResource(SIndexedIndirectBuffer&& buffer) override;
    virtual SIndexedIndirectBuffer& getIndexedIndirectBuffer(const IndexedIndirectBufferHandle buffer) override
    { return IndexedIndirectBuffers[buffer]; }
    virtual void destroyIndexedIndirectBuffer(const IndexedIndirectBufferHandle buffer) override
    { IndexedIndirectBuffers.erase(buffer); }

    virtual void setSceneManager(const std::shared_ptr<scene::ISceneManager>& smgr) override
    { SceneManager = smgr; }

  protected:
    SDeviceCreationParameters CreationParams;
    std::unordered_map<SGPUResource::HandleType, SRenderPass> RenderPasses;
    std::unordered_map<SGPUResource::HandleType, SPipeline> Pipelines;
    std::unordered_map<SGPUResource::HandleType, SShader> Shaders;
    std::unordered_map<SGPUResource::HandleType, SShaderUniform> ShaderUniforms;
    std::unordered_map<SGPUResource::HandleType, SPushConstant> PushConstants;
    std::unordered_map<SGPUResource::HandleType, SShaderBuffer> ShaderBuffers;
    std::unordered_map<SGPUResource::HandleType, STexture> Textures;
    std::unordered_map<ID, SGPUResource::HandleType> TextureIDMap;
    std::unordered_map<SGPUResource::HandleType, SIndirectBuffer> IndirectBuffers;
    std::unordered_map<SGPUResource::HandleType, SIndexedIndirectBuffer> IndexedIndirectBuffers;
    std::unordered_map<SGPUResource::HandleType, bool> Persistence;
    std::shared_ptr<scene::ISceneManager> SceneManager = nullptr;
    std::vector<RenderPassHandle> RenderPassQueue;

    RenderPassHandle CurrentPass = NULL_GPU_RESOURCE_HANDLE;

    std::uint64_t TotalMemory = 0;

  private:
    SGPUResource::HandleType Handle = 1;
  };

} // namespace video
} // namespace saga


#endif
