// Copyright (C) 2002-2012 Nikolaus Gebhardt
// This file is part of the "Irrlicht Engine".
// For conditions of distribution and use, see copyright notice in irrlicht.h

#ifndef __C_TRIANGLE_BB_SELECTOR_H_INCLUDED__
#define __C_TRIANGLE_BB_SELECTOR_H_INCLUDED__

#include "CTriangleSelector.h"

namespace saga
{
namespace scene
{

//! Stupid triangle selector without optimization
class CTriangleBBSelector : public CTriangleSelector
{
public:

  //! Constructs a selector based on a mesh
  CTriangleBBSelector(ISceneNode* node);

  //! Gets all triangles.
  virtual void getTriangles(core::triangle3df* triangles, std::int32_t arraySize, std::int32_t& outTriangleCount,
    const glm::mat4* transform, bool useNodeTransform,
    std::vector<SCollisionTriangleRange>* outTriangleInfo) const override;

  //! Gets all triangles which lie within a specific bounding box.
  virtual void getTriangles(core::triangle3df* triangles, std::int32_t arraySize, std::int32_t& outTriangleCount,
    const core::aabbox3d<float>& box, const glm::mat4* transform, bool useNodeTransform,
    std::vector<SCollisionTriangleRange>* outTriangleInfo) const override;

  //! Gets all triangles which have or may have contact with a 3d line.
  virtual void getTriangles(core::triangle3df* triangles, std::int32_t arraySize,
    std::int32_t& outTriangleCount, const core::line3d<float>& line,
    const glm::mat4* transform, bool useNodeTransform,
    std::vector<SCollisionTriangleRange>* outTriangleInfo) const override;

protected:
  void fillTriangles() const;

};

} // namespace scene
} // namespace saga


#endif

