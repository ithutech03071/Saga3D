// Copyright (C) 2002-2012 Nikolaus Gebhardt
// This file is part of the "Irrlicht Engine".
// For conditions of distribution and use, see copyright notice in irrlicht.h

#include "CTerrainTriangleSelector.h"
#include "CTerrainSceneNode.h"
#include "os.h"

namespace saga
{
namespace scene
{


//! constructor
CTerrainTriangleSelector::CTerrainTriangleSelector (ITerrainSceneNode* node, std::int32_t LOD)
  : SceneNode(node)
{
  #ifdef _DEBUG
  setDebugName ("CTerrainTriangleSelector");
  #endif

  setTriangleData(node, LOD);
}


//! destructor
CTerrainTriangleSelector::~CTerrainTriangleSelector()
{
  TrianglePatches.TrianglePatchArray.clear();
}


//! Clears and sets triangle data
void CTerrainTriangleSelector::setTriangleData(ITerrainSceneNode* node, std::int32_t LOD)
{
  // Get pointer to the GeoMipMaps vertices
  const video::S3DVertex2TCoords* vertices = static_cast<const video::S3DVertex2TCoords*>(node->getRenderBuffer()->getVertices());

  // Clear current data
  const std::int32_t count = (static_cast<CTerrainSceneNode*>(node))->TerrainData.PatchCount;
  TrianglePatches.TotalTriangles = 0;
  TrianglePatches.NumPatches = count*count;

  TrianglePatches.TrianglePatchArray.reallocate(TrianglePatches.NumPatches);
  for (std::int32_t o= 0; o<TrianglePatches.NumPatches; ++o)
    TrianglePatches.TrianglePatchArray.push_back(SGeoMipMapTrianglePatch());

  core::triangle3df tri;
  std::vector<std::uint32_t> indices;
  std::int32_t tIndex = 0;
  for(std::int32_t x = 0; x < count; ++x)
  {
    for(std::int32_t z = 0; z < count; ++z)
    {
      TrianglePatches.TrianglePatchArray[tIndex].NumTriangles = 0;
      TrianglePatches.TrianglePatchArray[tIndex].Box = node->getBoundingBox(x, z);
      std::uint32_t indexCount = node->getIndicesForPatch(indices, x, z, LOD);

      TrianglePatches.TrianglePatchArray[tIndex].Triangles.reallocate(indexCount/3);
      for(std::uint32_t i = 0; i < indexCount; i += 3)
      {
        tri.pointA = vertices[indices[i+0]].Pos;
        tri.pointB = vertices[indices[i+1]].Pos;
        tri.pointC = vertices[indices[i+2]].Pos;
        TrianglePatches.TrianglePatchArray[tIndex].Triangles.push_back(tri);
        ++TrianglePatches.TrianglePatchArray[tIndex].NumTriangles;
      }

      TrianglePatches.TotalTriangles += TrianglePatches.TrianglePatchArray[tIndex].NumTriangles;
      ++tIndex;
    }
  }
}


//! Gets all triangles.
void CTerrainTriangleSelector::getTriangles(core::triangle3df* triangles,
      std::int32_t arraySize, std::int32_t& outTriangleCount,
      const glm::mat4* transform, bool useNodeTransform,
      std::vector<SCollisionTriangleRange>* outTriangleInfo) const
{
  std::int32_t count = TrianglePatches.TotalTriangles;

  if (count > arraySize)
    count = arraySize;

  glm::mat4 mat;

  if (transform)
    mat = (*transform);

  std::int32_t tIndex = 0;

  for (std::int32_t i = 0; i < TrianglePatches.NumPatches; ++i)
  {
    if (tIndex + TrianglePatches.TrianglePatchArray[i].NumTriangles <= count)
      for (std::int32_t j= 0; j<TrianglePatches.TrianglePatchArray[i].NumTriangles; ++j)
      {
        triangles[tIndex] = TrianglePatches.TrianglePatchArray[i].Triangles[j];

        mat.transformVect(triangles[tIndex].pointA);
        mat.transformVect(triangles[tIndex].pointB);
        mat.transformVect(triangles[tIndex].pointC);

        ++tIndex;
      }
  }

  if (outTriangleInfo)
  {
    SCollisionTriangleRange triRange;
    triRange.RangeSize = tIndex;
    triRange.Selector = const_cast<CTerrainTriangleSelector*>(this);
    triRange.SceneNode = SceneNode;
    outTriangleInfo->push_back(triRange);
  }

  outTriangleCount = tIndex;
}


//! Gets all triangles which lie within a specific bounding box.
void CTerrainTriangleSelector::getTriangles(core::triangle3df* triangles,
    std::int32_t arraySize, std::int32_t& outTriangleCount,
    const core::aabbox3d<float>& box,
    const glm::mat4* transform, bool useNodeTransform,
    std::vector<SCollisionTriangleRange>* outTriangleInfo) const
{
  std::int32_t count = TrianglePatches.TotalTriangles;

  if (count > arraySize)
    count = arraySize;

  glm::mat4 mat;

  if (transform)
    mat = (*transform);

  std::int32_t tIndex = 0;

  for (std::int32_t i = 0; i < TrianglePatches.NumPatches; ++i)
  {
    if (tIndex + TrianglePatches.TrianglePatchArray[i].NumTriangles <= count &&
      TrianglePatches.TrianglePatchArray[i].Box.intersectsWithBox(box))
      for (std::int32_t j= 0; j<TrianglePatches.TrianglePatchArray[i].NumTriangles; ++j)
      {
        triangles[tIndex] = TrianglePatches.TrianglePatchArray[i].Triangles[j];

        mat.transformVect(triangles[tIndex].pointA);
        mat.transformVect(triangles[tIndex].pointB);
        mat.transformVect(triangles[tIndex].pointC);

        ++tIndex;
      }
  }

  if (outTriangleInfo)
  {
    SCollisionTriangleRange triRange;
    triRange.RangeSize = tIndex;
    triRange.Selector = const_cast<CTerrainTriangleSelector*>(this);
    triRange.SceneNode = SceneNode;
    outTriangleInfo->push_back(triRange);
  }

  outTriangleCount = tIndex;
}


//! Gets all triangles which have or may have contact with a 3d line.
void CTerrainTriangleSelector::getTriangles(core::triangle3df* triangles,
    std::int32_t arraySize, std::int32_t& outTriangleCount, const core::line3d<float>& line,
    const glm::mat4* transform, bool useNodeTransform,
    std::vector<SCollisionTriangleRange>* outTriangleInfo) const
{
  const std::int32_t count = core::min_((std::int32_t)TrianglePatches.TotalTriangles, arraySize);

  glm::mat4 mat;

  if (transform)
    mat = (*transform);

  std::int32_t tIndex = 0;

  for (std::int32_t i = 0; i < TrianglePatches.NumPatches; ++i)
  {
    if (tIndex + TrianglePatches.TrianglePatchArray[i].NumTriangles <= count
            && TrianglePatches.TrianglePatchArray[i].Box.intersectsWithLine(line))
    {
      for (std::int32_t j= 0; j<TrianglePatches.TrianglePatchArray[i].NumTriangles; ++j)
      {
        triangles[tIndex] = TrianglePatches.TrianglePatchArray[i].Triangles[j];

        mat.transformVect(triangles[tIndex].pointA);
        mat.transformVect(triangles[tIndex].pointB);
        mat.transformVect(triangles[tIndex].pointC);

        ++tIndex;
      }
    }
  }

  if (outTriangleInfo)
  {
    SCollisionTriangleRange triRange;
    triRange.RangeSize = tIndex;
    triRange.Selector = const_cast<CTerrainTriangleSelector*>(this);
    triRange.SceneNode = SceneNode;
    outTriangleInfo->push_back(triRange);
  }

  outTriangleCount = tIndex;
}


//! Returns amount of all available triangles in this selector
std::int32_t CTerrainTriangleSelector::getTriangleCount() const
{
  return TrianglePatches.TotalTriangles;
}


ISceneNode* CTerrainTriangleSelector::getSceneNodeForTriangle(
    std::uint32_t triangleIndex) const
{
  return SceneNode;
}


/* Get the number of TriangleSelectors that are part of this one.
Only useful for MetaTriangleSelector others return 1
*/
std::uint32_t CTerrainTriangleSelector::getSelectorCount() const
{
  return 1;
}


/* Get the TriangleSelector based on index based on getSelectorCount.
Only useful for MetaTriangleSelector others return 'this' or 0
*/
ITriangleSelector* CTerrainTriangleSelector::getSelector(std::uint32_t index)
{
  if (index)
    return 0;
  else
    return this;
}


/* Get the TriangleSelector based on index based on getSelectorCount.
Only useful for MetaTriangleSelector others return 'this' or 0
*/
const ITriangleSelector* CTerrainTriangleSelector::getSelector(std::uint32_t index) const
{
  if (index)
    return 0;
  else
    return this;
}


} // namespace scene
} // namespace saga
