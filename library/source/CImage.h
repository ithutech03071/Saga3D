// Copyright (C) 2002-2012 Nikolaus Gebhardt
// This file is part of the "Irrlicht Engine".
// For conditions of distribution and use, see copyright notice in irrlicht.h

#ifndef __C_IMAGE_H_INCLUDED__
#define __C_IMAGE_H_INCLUDED__

#include "IImage.h"
#include "rect.h"

namespace saga
{
namespace video
{

//! IImage implementation with a lot of special image operations for
//! 16 bit A1R5G5B5/32 Bit A8RGB8 images, which are used by the SoftwareDevice.
class CImage : public IImage
{
public:

  //! constructor from raw image data
  /** \param useForeignMemory: If true, the image will use the data pointer
  directly and own it from now on, which means it will also try to delete [] the
  data when the image will be destructed. If false, the memory will by copied. */
  CImage(E_PIXEL_FORMAT format, const glm::uvec2& size, void* data,
    bool ownForeignMemory = true, bool deleteMemory = true);

  //! constructor for empty image
  CImage(E_PIXEL_FORMAT format, const glm::uvec2& size);

  //! returns a pixel
  virtual SColor getPixel(std::uint32_t x, std::uint32_t y) const override;

  //! sets a pixel
  virtual void setPixel(std::uint32_t x, std::uint32_t y, const SColor &color, bool blend = false) override;

  //! copies this surface into another, scaling it to fit.
  virtual void copyToScaling(void* target, std::uint32_t width, std::uint32_t height, E_PIXEL_FORMAT format, std::uint32_t pitch= 0) override;

  //! copies this surface into another, scaling it to fit.
  virtual void copyToScaling(IImage* target) override;

  //! copies this surface into another
  virtual void copyTo(IImage* target, const glm::ivec2& pos=glm::ivec2(0,0)) override;

  //! copies this surface into another
  virtual void copyTo(IImage* target, const glm::ivec2& pos, const core::rect<std::int32_t>& sourceRect, const core::rect<std::int32_t>* clipRect= 0) override;

  //! copies this surface into another, using the alpha mask, an cliprect and a color to add with
  virtual void copyToWithAlpha(IImage* target, const glm::ivec2& pos,
      const core::rect<std::int32_t>& sourceRect, const SColor &color,
      const core::rect<std::int32_t>* clipRect = 0, bool combineAlpha=false) override;

  //! copies this surface into another, scaling it to fit, applying a box filter
  virtual void copyToScalingBoxFilter(IImage* target, std::int32_t bias = 0, bool blend = false) override;

  //! fills the surface with given color
  virtual void fill(const SColor &color) override;

private:
  inline SColor getPixelBox (std::int32_t x, std::int32_t y, std::int32_t fx, std::int32_t fy, std::int32_t bias) const;
};

} // namespace video
} // namespace saga


#endif

