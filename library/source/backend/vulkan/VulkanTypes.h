#ifndef __VULKAN_TYPES_H_INCLUDED__
#define __VULKAN_TYPES_H_INCLUDED__

#include <vulkan/vulkan.h>
#include <cstdint>

namespace saga
{

namespace scene
{
enum class E_PRIMITIVE_TYPE;
enum class E_INDEX_TYPE;
}

namespace video
{

enum class E_ATTRIBUTE_FORMAT;
enum class E_PIXEL_FORMAT;
enum class E_BLEND_FACTOR;
enum class E_BLEND_OP;

namespace vk
{

VkFormat getFormat(const E_ATTRIBUTE_FORMAT format);
VkFormat getFormat(const E_PIXEL_FORMAT format);
VkPrimitiveTopology getTopology(const scene::E_PRIMITIVE_TYPE type);
VkIndexType getIndexType(const scene::E_INDEX_TYPE type);
VkBlendFactor getBlendFactor(const E_BLEND_FACTOR factor);
VkBlendOp getBlendOp(const E_BLEND_OP op);
VkColorComponentFlags getColorComponentFlags(const std::uint32_t flags);

} // namespace vk
} // namespace video
} // namespace saga

#endif // __VULKAN_TYPES_H_INCLUDED__
