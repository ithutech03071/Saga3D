#ifndef __CASSIMP_MESH_LOADER_H_INCLUDED__
#define __CASSIMP_MESH_LOADER_H_INCLUDED__

#include "IMeshLoader.h"

namespace saga
{
namespace scene
{

class CAssimpMeshLoader : public IMeshLoader
{
public:
  //! Constructor
  CAssimpMeshLoader();

  //! Destructor
  virtual ~CAssimpMeshLoader();

  //! Returns true if the file might be loaded by this class.
  /** This decision should be based on the file extension (e.g. ".cob")
  only.
  \param filename Name of the file to test.
  \return True if the file might be loaded by this class. */
  virtual bool isSupportedExtension(const std::string& extension) const override { return true; }

  //! Creates/loads an animated mesh from the file.
  /** \param file File handler to load the file from.
  \return Pointer to the created mesh. Returns 0 if loading failed.
  If you no longer need the mesh, you should call IAnimatedMesh::drop().
  See IReferenceCounted::drop() for more information. */
  virtual std::shared_ptr<IMesh> createMesh(void* data, const std::size_t size, const std::string& extension) override;

};

} // namespace scene
} // namespace saga

#endif // __CASSIMP_MESH_LOADER_H_INCLUDED__
