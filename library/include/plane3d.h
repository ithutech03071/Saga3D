// Copyright (C) 2002-2012 Nikolaus Gebhardt
// This file is part of the "Irrlicht Engine".
// For conditions of distribution and use, see copyright notice in irrlicht.h

#ifndef __IRR_PLANE_3D_H_INCLUDED__
#define __IRR_PLANE_3D_H_INCLUDED__

#include "irrMath.h"
#include <glm/vec3.hpp>
#include <glm/gtc/matrix_inverse.hpp>

namespace saga
{
namespace core
{

//! Enumeration for intersection relations of 3d objects
enum EIntersectionRelation3D
{
  ISREL3D_FRONT = 0,
  ISREL3D_BACK,
  ISREL3D_PLANAR,
  ISREL3D_SPANNING,
  ISREL3D_CLIPPED
};

//! Template plane class with some intersection testing methods.
/** It has to be ensured, that the normal is always normalized. The constructors
    and setters of this class will not ensure this automatically. So any normal
    passed in has to be normalized in advance. No change to the normal will be
    made by any of the class methods.
*/
template <class T>
class plane3d
{
  public:

    // Constructors

    plane3d(): Normal(0,1,0) { recalculateD(glm::vec3(0,0,0)); }

    plane3d(const glm::vec3& MPoint, const glm::vec3& Normal) : Normal(Normal) { recalculateD(MPoint); }

    plane3d(T px, T py, T pz, T nx, T ny, T nz) : Normal(nx, ny, nz) { recalculateD(glm::vec3(px, py, pz)); }

    plane3d(const glm::vec3& point1, const glm::vec3& point2, const glm::vec3& point3)
    { setPlane(point1, point2, point3); }

    plane3d(const glm::vec3 & normal, const T d) : Normal(normal), D(d) { }

    // operators

    inline bool operator==(const plane3d<T>& other) const { return (equals(D, other.D) && Normal==other.Normal);}

    inline bool operator!=(const plane3d<T>& other) const { return !(*this == other);}

    // functions

    void setPlane(const glm::vec3& point, const glm::vec3& nvector)
    {
      Normal = nvector;
      recalculateD(point);
    }

    void setPlane(const glm::vec3& nvect, T d)
    {
      Normal = nvect;
      D = d;
    }

    void setPlane(const glm::vec3& point1, const glm::vec3& point2, const glm::vec3& point3)
    {
      // creates the plane from 3 memberpoints
      Normal = glm::cross(point2 - point1, point3 - point1);
      Normal = glm::normalize(Normal);

      recalculateD(point1);
    }


    void transform(const glm::mat4& mat)
    {
      // Transform the plane member point, i.e. rotate, translate and scale it.
      glm::vec4 member = mat * glm::vec4(getMemberPoint(), 1);

      // Transform the normal by the transposed inverse of the matrix
      auto transposedInverse = glm::inverseTranspose(mat);
      auto normal = transposedInverse * glm::vec4(Normal, 1) ;

      setPlane(member, normal);
    }

    //! Get an intersection with a 3d line.
    /** \param lineVect Vector of the line to intersect with.
    \param linePoint Point of the line to intersect with.
    \param outIntersection Place to store the intersection point, if there is one.
    \return True if there was an intersection, false if there was not.
    */
    bool getIntersectionWithLine(const glm::vec3& linePoint,
        const glm::vec3& lineVect,
        glm::vec3& outIntersection) const
    {
      T t2 = glm::dot(Normal, lineVect);

      if (t2 == 0)
        return false;

      T t =- (glm::dot(Normal, linePoint) + D) / t2;
      outIntersection = linePoint + (lineVect * t);
      return true;
    }

    //! Get percentage of line between two points where an intersection with this plane happens.
    /** Only useful if known that there is an intersection.
    \param linePoint1 Point1 of the line to intersect with.
    \param linePoint2 Point2 of the line to intersect with.
    \return Where on a line between two points an intersection with this plane happened.
    For example, 0.5 is returned if the intersection happened exactly in the middle of the two points.
    */
    float getKnownIntersectionWithLine(const glm::vec3& linePoint1,
      const glm::vec3& linePoint2) const
    {
      glm::vec3 vect = linePoint2 - linePoint1;
      T t2 = (float) glm::dot(Normal, vect);
      return (float)-((glm::dot(Normal, linePoint1) + D) / t2);
    }

    //! Get an intersection with a 3d line, limited between two 3d points.
    /** \param linePoint1 Point 1 of the line.
    \param linePoint2 Point 2 of the line.
    \param outIntersection Place to store the intersection point, if there is one.
    \return True if there was an intersection, false if there was not.
    */
    bool getIntersectionWithLimitedLine(
        const glm::vec3& linePoint1,
        const glm::vec3& linePoint2,
        glm::vec3& outIntersection) const
    {
      return (getIntersectionWithLine(linePoint1, linePoint2 - linePoint1, outIntersection) &&
             isBetweenPoints(outIntersection, linePoint1, linePoint2));
    }

    //! Classifies the relation of a point to this plane.
    /** \param point Point to classify its relation.
    \return ISREL3D_FRONT if the point is in front of the plane,
    ISREL3D_BACK if the point is behind of the plane, and
    ISREL3D_PLANAR if the point is within the plane. */
    EIntersectionRelation3D classifyPointRelation(const glm::vec3& point) const
    {
      const T d = glm::dot(Normal, point) + D;

      if (d < -ROUNDING_ERROR_float)
        return ISREL3D_BACK;

      if (d > ROUNDING_ERROR_float)
        return ISREL3D_FRONT;

      return ISREL3D_PLANAR;
    }

    //! Recalculates the distance from origin by applying a new member point to the plane.
    void recalculateD(const glm::vec3& MPoint)
    {
      D = - glm::dot(MPoint, Normal);
    }

    //! Gets a member point of the plane.
    glm::vec3 getMemberPoint() const
    {
      return Normal * -D;
    }

    //! Tests if there is an intersection with the other plane
    /** \return True if there is a intersection. */
    bool existsIntersection(const plane3d<T>& other) const
    {
      glm::vec3 cross = glm::cross(other.Normal, Normal);
      return glm::length(cross) > core::ROUNDING_ERROR_float;
    }

    //! Intersects this plane with another.
    /** \param other Other plane to intersect with.
    \param outLinePoint Base point of intersection line.
    \param outLineVect Vector of intersection.
    \return True if there is a intersection, false if not. */
    bool getIntersectionWithPlane(const plane3d<T>& other,
        glm::vec3& outLinePoint,
        glm::vec3& outLineVect) const
    {
      const T fn00 = glm::length(Normal);
      const T fn01 = glm::dot(Normal, other.Normal);
      const T fn11 = glm::length(other.Normal);
      const double det = fn00*fn11 - fn01*fn01;

      if (fabs(det) < ROUNDING_ERROR_double)
        return false;

      const double invdet = 1.0 / det;
      const double fc0 = (fn11*-D + fn01*other.D) * invdet;
      const double fc1 = (fn00*-other.D + fn01*D) * invdet;

      outLineVect = glm::cross(Normal, other.Normal);
      outLinePoint = Normal*(T)fc0 + other.Normal*(T)fc1;
      return true;
    }

    //! Get the intersection point with two other planes if there is one.
    bool getIntersectionWithPlanes(const plane3d<T>& o1,
        const plane3d<T>& o2, glm::vec3& outPoint) const
    {
      glm::vec3 linePoint, lineVect;
      if (getIntersectionWithPlane(o1, linePoint, lineVect))
        return o2.getIntersectionWithLine(linePoint, lineVect, outPoint);

      return false;
    }

    //! Test if the triangle would be front or backfacing from any point.
    /** Thus, this method assumes a camera position from
    which the triangle is definitely visible when looking into
    the given direction.
    Note that this only works if the normal is Normalized.
    Do not use this method with points as it will give wrong results!
    \param lookDirection: Look direction.
    \return True if the plane is front facing and
    false if it is backfacing. */
    bool isFrontFacing(const glm::vec3& lookDirection) const
    {
      const float d = glm::dot(Normal, lookDirection);
      return F32_LOWER_EQUAL_0 (d);
    }

    //! Get the distance to a point.
    /** Note that this only works if the normal is normalized. */
    T getDistanceTo(const glm::vec3& point) const
    {
      return glm::dot(point, Normal) + D;
    }

    //! Normal vector of the plane.
    glm::vec3 Normal;

    //! Distance from origin.
    T D;
};


//! Typedef for a float 3d plane.
typedef plane3d<float> plane3df;

//! Typedef for an integer 3d plane.
typedef plane3d<std::int32_t> plane3di;

} // namespace core
} // namespace saga

#endif

