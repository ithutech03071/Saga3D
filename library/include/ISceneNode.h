#ifndef __I_SCENE_NODE_H_INCLUDED__
#define __I_SCENE_NODE_H_INCLUDED__

#include "ID.h"
#include "ESceneNodeTypes.h"
#include "ISceneNodeAnimator.h"
#include "GraphicsConstants.h"
#include "SRenderPass.h"
#include "SPipeline.h"
#include "SIndirectCommand.h"
#include "aabbox3d.h"
#include <glm/gtx/euler_angles.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtx/matrix_decompose.hpp>
#include <string>
#include <vector>
#include <array>
#include <algorithm>
#include <memory>
#include <functional>

namespace saga
{
namespace scene
{
  class ISceneNode;

  //! Type for list of scene nodes
  using ISceneNodeList = std::vector<std::shared_ptr<ISceneNode>>;
  //! Type for list of scene node animators
  // typedef std::vector<ISceneNodeAnimator*> ISceneNodeAnimatorList;

  //! Scene node interface.
  /** A scene node is a node in the hierarchical scene graph. Every scene
  node may have children, which are also scene nodes. Children move
  relative to their parent's position. If the parent of a node is not
  visible, its children won't be visible either. In this way, it is for
  example easily possible to attach a light to a moving car, or to place
  a walking character on a moving platform on a moving ship.
  */
  class ISceneNode : public std::enable_shared_from_this<ISceneNode>, public IEventReceiver
  {
  public:

    //! Constructor
    ISceneNode(
      const std::shared_ptr<ISceneNode>& parent,
      const std::shared_ptr<ISceneManager>& mgr,
      const glm::vec3& position = glm::vec3(0,0,0),
      const glm::vec3& rotation = glm::vec3(0,0,0),
      const glm::vec3& scale = glm::vec3(1.0f, 1.0f, 1.0f))
      : RelativeTranslation(position), RelativeRotation(rotation), RelativeScale(scale),
        SceneManager(mgr)
    {
      if (parent)
        parent->addChild(shared_from_this());

      updateAbsolutePosition();
    }

    virtual void onEvent(const SDL_Event& event) override
    {
      for (auto& anim : Animators)
      {
        anim->onEvent(event);
      }
    }

    //! Destructor
    virtual ~ISceneNode()
    {
      // delete all children
      removeAll();

      Animators.clear();

      // if (TriangleSelector)
        // TriangleSelector->drop();
    }

    void setID(const ID id) { NodeID = id; }
    const auto getID() const { return NodeID; }

    //! This method is called just before the rendering process of the whole scene.
    /** Nodes may register themselves in the render pipeline during this call,
    precalculate the geometry which should be renderered, and prevent their
    children from being able to register themselves if they are clipped by simply
    not calling their onRegisterSceneNode method.
    If you are implementing your own scene node, you should overwrite this method
    with an implementation code looking like this:
    \code
    if (IsVisible)
      ISceneNode::onRegisterSceneNode(video::RenderPassHandle pass);
    \endcode
    */
    virtual void onRegisterSceneNode(video::RenderPassHandle pass)
    {
      if (IsVisible)
      {
        auto it = Children.begin();
        for (; it != Children.end(); ++it)
          (*it)->onRegisterSceneNode(pass);
      }
    }

    virtual void onUnregisterSceneNode(video::RenderPassHandle pass)
    {
      auto it = Children.begin();
      for (; it != Children.end(); ++it)
        (*it)->onUnregisterSceneNode(pass);
    }

    void setOnRender(std::function<void()> func) { OnRenderCallback = func; }

    void onRender() { if (OnRenderCallback) OnRenderCallback(); }

    bool hasDrawCommands() const { return !DrawCommands.empty(); }

    void setDrawCommands(video::DrawCommandList&& list) { DrawCommands = std::move(list); }

    const auto& getDrawCommands() const { return DrawCommands; }

    auto getTexture(int slot) const { return Textures.at(slot); }

    auto& getTextures() { return Textures; }

    void setTexture(int slot, video::TextureHandle texture)
    {
      Textures[slot] = texture;
    }

    virtual void setPipeline(video::PipelineHandle pipeline) { Pipeline = pipeline; }

    auto getPipeline() const { return Pipeline; }

    //! onAnimate() is called just before rendering the whole scene.
    /** Nodes may calculate or store animations here, and may do other useful things,
    depending on what they are. Also, onAnimate() should be called for all
    child scene nodes here. This method will be called once per frame, independent
    of whether the scene node is visible or not.
    \param timeMs Current time in milliseconds. */
    virtual void onAnimate(const float time)
    {
      if (IsVisible)
      {
        // animate this node with all animators

        for (auto& anim : Animators)
        {
          anim->animateNode(*this, time);
        }

        // update absolute position
        updateAbsolutePosition();

        // perform the post render process on all children
        auto it = Children.begin();
        for (; it != Children.end(); ++it)
          (*it)->onAnimate(time);
      }
    }

    //! Returns the name of the node.
    /** \return Name as character string. */
    virtual const std::string& getName() const
    {
      return Name;
    }

    //! Sets the name of the node.
    /** \param name New name of the scene node. */
    virtual void setName(const std::string& name)
    {
      Name = name;
    }

    //! Get the axis aligned, not transformed bounding box of this node.
    /** This means that if this node is an animated 3d character,
    moving in a room, the bounding box will always be around the
    origin. To get the box in real world coordinates, just
    transform it with the matrix you receive with
    getAbsoluteTransformation() or simply use
    getTransformedBoundingBox(), which does the same.
    \return The non-transformed bounding box. */
    virtual const core::aabbox3d<float>& getBoundingBox() const = 0;

    //! Get the axis aligned, transformed and animated absolute bounding box of this node.
    /** Note: The result is still an axis-aligned bounding box, so it's size
    changes with rotation.
    \return The transformed bounding box. */
    virtual const core::aabbox3d<float> getTransformedBoundingBox() const
    {
      core::aabbox3d<float> box = getBoundingBox();
      // TODO: get transformed bounding box
      // AbsoluteTransformation.transformBoxEx(box);
      return box;
    }

    //! Get a the 8 corners of the original bounding box transformed and
    //! animated by the absolute transformation.
    /** Note: The result is _not_ identical to getTransformedBoundingBox().getEdges(),
    but getting an aabbox3d of these edges would then be identical.
    \param edges Receives an array with the transformed edges */
    virtual void getTransformedBoundingBoxEdges(std::vector<glm::vec3>& edges) const
    {
      edges.resize(8);
      getBoundingBox().getEdges(edges.data());
      for (std::uint32_t i= 0; i<8; ++i)
        edges[i] = glm::vec3(AbsoluteTransformation * glm::vec4(edges[i], 1));
    }

    //! Get the absolute transformation of the node. Is recalculated every onAnimate()-call.
    /** NOTE: For speed reasons the absolute transformation is not
    automatically recalculated on each change of the relative
    transformation or by a transformation change of an parent. Instead the
    update usually happens once per frame in onAnimate. You can enforce
    an update with updateAbsolutePosition().
    \return The absolute transformation matrix. */
    virtual const glm::mat4& getAbsoluteTransformation() const
    {
      return AbsoluteTransformation;
    }

    //! Returns the relative transformation of the scene node.
    /** The relative transformation is stored internally as 3
    vectors: translation, rotation and scale. To get the relative
    transformation matrix, it is calculated from these values.
    \return The relative transformation matrix. */
    virtual glm::mat4 getRelativeTransformation() const
    {
      auto translation = glm::translate(glm::mat4(1.0f), RelativeTranslation);
      auto rotation = glm::eulerAngleXYZ(RelativeRotation.x, RelativeRotation.y, RelativeRotation.z);
      auto scale = glm::scale(RelativeScale);
      return translation * rotation * scale;
    }

    //! Returns whether the node should be visible (if all of its parents are visible).
    /** This is only an option set by the user, but has nothing to
    do with geometry culling
    \return The requested visibility of the node, true means
    visible (if all parents are also visible). */
    virtual bool isVisible() const
    {
      return IsVisible;
    }

    //! Check whether the node is truly visible, taking into accounts its parents' visibility
    /** \return true if the node and all its parents are visible,
    false if this or any parent node is invisible. */
    virtual bool isTrulyVisible() const
    {
      if(!IsVisible)
        return false;

      if(!Parent)
        return true;

      return Parent->isTrulyVisible();
    }

    //! Sets if the node should be visible or not.
    /** All children of this node won't be visible either, when set
    to false. Invisible nodes are not valid candidates for selection by
    collision manager bounding box methods.
    \param isVisible If the node shall be visible. */
    virtual void setVisible(bool isVisible)
    {
      IsVisible = isVisible;
    }

    //! Adds a child to this scene node.
    /** If the scene node already has a parent it is first removed
    from the other parent.
    \param child A pointer to the new child. */
    virtual void addChild(const std::shared_ptr<ISceneNode>& child)
    {
      if (child && (child.get() != this))
      {
        // Change scene manager?
        // if (SceneManager != child->SceneManager)
          // child->setSceneManager(SceneManager);

        // child->grab();
        child->remove(); // remove from old parent
        Children.push_back(child);
        child->Parent = shared_from_this();
      }
    }

    /** If found in the children list, the child pointer is also
    dropped and might be deleted if no other grab exists.
    \param child A pointer to the child which shall be removed.
    \return True if the child was removed, and false if not,
    e.g. because it couldn't be found in the children list. */
    virtual bool removeChild(const std::shared_ptr<ISceneNode>& child)
    {
      auto it = Children.begin();
      for (; it != Children.end(); ++it)
        if ((*it).get() == child.get())
        {
          (*it)->Parent = 0;
          // (*it)->drop();
          Children.erase(it);
          return true;
        }

      return false;
    }

    //! Removes all children of this scene node
    /** The scene nodes found in the children list are also dropped
    and might be deleted if no other grab exists on them.
    */
    virtual void removeAll()
    {
      auto it = Children.begin();
      for (; it != Children.end(); ++it)
      {
        (*it)->Parent = 0;
        // (*it)->drop();
      }

      Children.clear();
    }

    //! Removes this scene node from the scene
    /** If no other grab exists for this node, it will be deleted.
    */
    virtual void remove()
    {
      if (Parent)
        Parent->removeChild(shared_from_this());
    }

    //! Adds an animator which should animate this node.
    /** \param animator A pointer to the new animator. */
    virtual void addAnimator(const std::shared_ptr<ISceneNodeAnimator>& animator)
    {
      if (animator)
      {
        Animators.push_back(animator);
      }
    }


    //! Get a list of all scene node animators.
    /** \return The list of animators attached to this node. */
    const auto& getAnimators() const
    {
      return Animators;
    }


    //! Removes an animator from this scene node.
    /** If the animator is found, it is also dropped and might be
    deleted if not other grab exists for it.
    \param animator A pointer to the animator to be deleted. */
    virtual void removeAnimator(const std::shared_ptr<ISceneNodeAnimator>& animator)
    {
      Animators.erase(std::remove(Animators.begin(), Animators.end(), animator), Animators.end());
    }

    //! Removes all animators from this scene node.
    /** The animators might also be deleted if no other grab exists
    for them. */
    virtual void removeAnimators()
    {
      Animators.clear();
    }

    //! Gets the scale of the scene node relative to its parent.
    /** This is the scale of this node relative to its parent.
    If you want the absolute scale, use
    getAbsoluteTransformation().getScale()
    \return The scale of the scene node. */
    virtual const glm::vec3& getScale() const
    {
      return RelativeScale;
    }

    //! Sets the relative scale of the scene node.
    /** \param scale New scale of the node, relative to its parent. */
    virtual void setScale(const glm::vec3& scale)
    {
      RelativeScale = scale;
    }

    //! Gets the rotation of the node relative to its parent.
    /** Note that this is the relative rotation of the node.
    If you want the absolute rotation, use
    getAbsoluteTransformation().getRotation()
    \return Current relative rotation of the scene node. */
    virtual const glm::vec3& getRotation() const
    {
      return RelativeRotation;
    }

    //! Sets the rotation of the node relative to its parent.
    /** This only modifies the relative rotation of the node.
    \param rotation New rotation of the node in degrees. */
    virtual void setRotation(const glm::vec3& rotation)
    {
      RelativeRotation = rotation;
    }

    //! Gets the position of the node relative to its parent.
    /** Note that the position is relative to the parent. If you want
    the position in world coordinates, use getAbsolutePosition() instead.
    \return The current position of the node relative to the parent. */
    virtual const glm::vec3& getPosition() const
    {
      return RelativeTranslation;
    }

    //! Sets the position of the node relative to its parent.
    /** Note that the position is relative to the parent.
    \param newpos New relative position of the scene node. */
    virtual void setPosition(const glm::vec3& newpos)
    {
      RelativeTranslation = newpos;
    }

    //! Gets the absolute position of the node in world coordinates.
    /** If you want the position of the node relative to its parent,
    use getPosition() instead.
    NOTE: For speed reasons the absolute position is not
    automatically recalculated on each change of the relative
    position or by a position change of an parent. Instead the
    update usually happens once per frame in onAnimate. You can enforce
    an update with updateAbsolutePosition().
    \return The current absolute position of the scene node (updated on last call of updateAbsolutePosition). */
    virtual glm::vec3 getAbsolutePosition() const
    {
      return AbsoluteTransformation[3];
    }

    //! Returns a const reference to the list of all children.
    /** \return The list of all children of this node. */
    const std::vector<std::shared_ptr<ISceneNode>>& getChildren() const
    {
      return Children;
    }

    //! Changes the parent of the scene node.
    /** \param newParent The new parent to be used. */
    virtual void setParent(const std::shared_ptr<ISceneNode>& newParent)
    {
      remove();

      Parent = newParent;

      if (Parent)
        Parent->addChild(shared_from_this());
    }

    //! Returns the triangle selector attached to this scene node.
    /** The Selector can be used by the engine for doing collision
    detection. You can create a TriangleSelector with
    ISceneManager::createTriangleSelector() or
    ISceneManager::createOctreeTriangleSelector and set it with
    ISceneNode::setTriangleSelector(). If a scene node got no triangle
    selector, but collision tests should be done with it, a triangle
    selector is created using the bounding box of the scene node.
    \return A pointer to the TriangleSelector or 0, if there
    is none. */
    // virtual ITriangleSelector* getTriangleSelector() const
    // {
    //   return TriangleSelector;
    // }


    //! Sets the triangle selector of the scene node.
    /** The Selector can be used by the engine for doing collision
    detection. You can create a TriangleSelector with
    ISceneManager::createTriangleSelector() or
    ISceneManager::createOctreeTriangleSelector(). Some nodes may
    create their own selector by default, so it would be good to
    check if there is already a selector in this node by calling
    ISceneNode::getTriangleSelector().
    \param selector New triangle selector for this scene node. */
    // virtual void setTriangleSelector(ITriangleSelector* selector)
    // {
    //   if (TriangleSelector != selector)
    //   {
    //     // if (TriangleSelector)
    //       // TriangleSelector->drop();

    //     TriangleSelector = selector;
    //     // if (TriangleSelector)
    //       // TriangleSelector->grab();
    //   }
    // }
    //! Updates the absolute position based on the relative and the parents position
    /** Note: This does not recursively update the parents absolute positions, so if you have a deeper
      hierarchy you might want to update the parents first.*/
    virtual void updateAbsolutePosition()
    {
      if (Parent)
      {
        AbsoluteTransformation =
          Parent->getAbsoluteTransformation() * getRelativeTransformation();
      }
      else
        AbsoluteTransformation = getRelativeTransformation();
    }

    //! Returns the parent of this scene node
    /** \return A pointer to the parent. */
    const std::shared_ptr<ISceneNode>& getParent() const
    {
      return Parent;
    }

    //! Returns type of the scene node
    /** \return The type of this node. */
    virtual E_SCENE_NODE_TYPE getType() const
    {
      return E_SCENE_NODE_TYPE::UNKNOWN;
    }

    //! Creates a clone of this scene node and its children.
    /** \param newParent An optional new parent.
    \param newManager An optional new scene manager.
    \return The newly created clone of this node. */
    virtual std::shared_ptr<ISceneNode> clone(const std::shared_ptr<ISceneNode>& newParent = 0)
    {
      return nullptr; // to be implemented by derived classes
    }

    //! Retrieve the scene manager for this node.
    /** \return The node's scene manager. */
    virtual const std::shared_ptr<ISceneManager>& getSceneManager() const { return SceneManager; }

  protected:

    //! A clone function for the ISceneNode members.
    /** This method can be used by clone() implementations of
    derived classes
    \param toCopyFrom The node from which the values are copied
    \param newManager The new scene manager. */
    // void cloneMembers(ISceneNode* toCopyFrom, ISceneManager* newManager)
    // {
    //   Name = toCopyFrom->Name;
    //   AbsoluteTransformation = toCopyFrom->AbsoluteTransformation;
    //   RelativeTranslation = toCopyFrom->RelativeTranslation;
    //   RelativeRotation = toCopyFrom->RelativeRotation;
    //   RelativeScale = toCopyFrom->RelativeScale;
    //   ID = toCopyFrom->ID;
    //   setTriangleSelector(toCopyFrom->TriangleSelector);
    //   AutomaticCullingState = toCopyFrom->AutomaticCullingState;
    //   // DebugDataVisible = toCopyFrom->DebugDataVisible;
    //   IsVisible = toCopyFrom->IsVisible;
    //   IsDebugObject = toCopyFrom->IsDebugObject;

    //   if (newManager)
    //     SceneManager = newManager;
    //   else
    //     SceneManager = toCopyFrom->SceneManager;

    //   // clone children

    //   auto it = toCopyFrom->Children.begin();
    //   for (; it != toCopyFrom->Children.end(); ++it)
    //     (*it)->clone(this, newManager);

      // clone animators

      // ISceneNodeAnimatorList::Iterator ait = toCopyFrom->Animators.begin();
      // for (; ait != toCopyFrom->Animators.end(); ++ait)
      // {
      //   ISceneNodeAnimator* anim = (*ait)->createClone(this, SceneManager);
      //   if (anim)
      //   {
      //     addAnimator(anim);
      //     anim->drop();
      //   }
      // }
    // }

    //! Sets the new scene manager for this node and all children.
    //! Called by addChild when moving nodes between scene managers
    // void setSceneManager(ISceneManager* newManager)
    // {
    //   SceneManager = newManager;

    //   auto it = Children.begin();
    //   for (; it != Children.end(); ++it)
    //     (*it)->setSceneManager(newManager);
    // }

    //! ID of this scene node
    ID NodeID = INVALID_ID;

    //! Name of the scene node.
    std::string Name;

    //! Absolute transformation of the node.
    glm::mat4 AbsoluteTransformation;

    //! Relative translation of the scene node.
    glm::vec3 RelativeTranslation;

    //! Relative rotation of the scene node.
    glm::vec3 RelativeRotation;

    //! Relative scale of the scene node.
    glm::vec3 RelativeScale;

    //! Pointer to the parent
    std::shared_ptr<ISceneNode> Parent = nullptr;

    //! List of all children of this node
    std::vector<std::shared_ptr<ISceneNode>> Children;

    //! Texture list
    std::array<video::STexture::HandleType, video::MAX_NODE_TEXTURES> Textures = { video::NULL_GPU_RESOURCE_HANDLE };

    //! List of all animator nodes
    std::vector<std::shared_ptr<ISceneNodeAnimator>> Animators;

    //! Pointer to the scene manager
    std::shared_ptr<ISceneManager> SceneManager = nullptr;

    //! Custom hook into rendering process of the scene node
    /**
     * This function is called before the node is rendered by GPU.
     * Use it to update node textures, UBO, push constants, SSBO, ...
    */
    std::function<void()> OnRenderCallback = nullptr;

    video::DrawCommandList DrawCommands;

    video::PipelineHandle Pipeline = video::NULL_GPU_RESOURCE_HANDLE;

    //! Is the node visible?
    bool IsVisible = true;
  };


} // namespace scene
} // namespace saga

#endif

