#ifndef __E_MIPMAP_MODES_H_INCLUDED__
#define __E_MIPMAP_MODES_H_INCLUDED__

namespace saga
{
namespace video
{

enum class E_MIPMAP_MODE {
  NEAREST,
  LINEAR,
  CUBIC
};

} // namespace video
} // namespace saga

#endif // __E_MIPMAP_MODES_H_INCLUDED__

