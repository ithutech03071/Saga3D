#ifndef __CMESH_BUFFER_H_INCLUDED__
#define __CMESH_BUFFER_H_INCLUDED__

#include "EPrimitiveTypes.h"
#include "IMeshBuffer.h"
#include "IVideoDriver.h"
#include <vector>
#include <unordered_map>
#include <algorithm>
#include <cstring>

namespace saga
{
namespace scene
{
  //! Implementation of the CPU IMeshBuffer interface
  class CMeshBuffer : public IMeshBuffer
  {
  public:
    //! Default constructor for empty meshbuffer
    CMeshBuffer()
      :  ID(++RootID)
    {
      PrimitiveType = E_PRIMITIVE_TYPE::TRIANGLES;
    }

    virtual bool isGPUBuffer() const override { return false; }

    virtual ~CMeshBuffer() {}

    virtual std::uint64_t getID() const override { return ID; }

    virtual void buildBuffer(video::IVideoDriver& driver, const video::PipelineHandle pipeline) override
    {
      if (GPUBuffers.count(pipeline) == 0)
      {
        GPUBuffers[pipeline] = {};
        auto& buffer = GPUBuffers[pipeline];
        std::size_t bufferSize = 0;
        for (const auto& attr : driver.getPipeline(pipeline).Layout.Attributes.at(Binding))
        {
          if (attr.Type != video::E_ATTRIBUTE_TYPE::INVALID)
            bufferSize += GetAttributeSize(attr.Format);
        }
        bufferSize *= getVertexCount();
        buffer.clear();
        buffer.resize(bufferSize);
        std::size_t position = 0;
        for (std::size_t i = 0 ; i < getVertexCount(); ++i)
        {
          std::size_t customAttributeCount = 0;
          for (const auto& attr : driver.getPipeline(pipeline).Layout.Attributes.at(Binding))
          {
            switch (attr.Type)
            {
              case video::E_ATTRIBUTE_TYPE::INVALID: continue;
              case video::E_ATTRIBUTE_TYPE::POSITION:
              {
                const auto size = GetAttributeComponentSize(attr.Format);
                std::memcpy(buffer.data() + position, &PositionBuffer.at(i).x, size);
                position += size;
                std::memcpy(buffer.data() + position, &PositionBuffer.at(i).y, size);
                position += size;
                std::memcpy(buffer.data() + position, &PositionBuffer.at(i).z, size);
                position += size;
              } break;

              case video::E_ATTRIBUTE_TYPE::NORMAL:
              {
                const auto size = GetAttributeComponentSize(attr.Format);
                std::memcpy(buffer.data() + position, &NormalBuffer.at(i).x, size);
                position += size;
                std::memcpy(buffer.data() + position, &NormalBuffer.at(i).y, size);
                position += size;
                std::memcpy(buffer.data() + position, &NormalBuffer.at(i).z, size);
                position += size;
              } break;

              case video::E_ATTRIBUTE_TYPE::COLOR:
              {
                const auto size = GetAttributeComponentSize(attr.Format);
                std::memcpy(buffer.data() + position, &ColorBuffer.at(i).x, size);
                position += size;
                std::memcpy(buffer.data() + position, &ColorBuffer.at(i).y, size);
                position += size;
                std::memcpy(buffer.data() + position, &ColorBuffer.at(i).z, size);
                position += size;
              } break;

              case video::E_ATTRIBUTE_TYPE::TEXTURE_COORDINATE:
              {
                const auto size = GetAttributeComponentSize(attr.Format);
                std::memcpy(buffer.data() + position, &TCoordBuffer.at(i).x, size);
                position += size;
                std::memcpy(buffer.data() + position, &TCoordBuffer.at(i).y, size);
                position += size;
              } break;

              case video::E_ATTRIBUTE_TYPE::TANGENT:
              {
                const auto size = GetAttributeComponentSize(attr.Format);
                std::memcpy(buffer.data() + position, &TangentBuffer.at(i).x, size);
                position += size;
                std::memcpy(buffer.data() + position, &TangentBuffer.at(i).y, size);
                position += size;
                std::memcpy(buffer.data() + position, &TangentBuffer.at(i).z, size);
                position += size;
              } break;

              case video::E_ATTRIBUTE_TYPE::BITANGENT:
              {
                const auto size = GetAttributeComponentSize(attr.Format);
                std::memcpy(buffer.data() + position, &BiTangentBuffer.at(i).x, size);
                position += size;
                std::memcpy(buffer.data() + position, &BiTangentBuffer.at(i).y, size);
                position += size;
                std::memcpy(buffer.data() + position, &BiTangentBuffer.at(i).z, size);
                position += size;
              } break;

              case video::E_ATTRIBUTE_TYPE::BONE_WEIGHT:
              {
                const auto size = GetAttributeComponentSize(attr.Format);
                std::memcpy(buffer.data() + position, &BoneWeightBuffer.at(i).x, size);
                position += size;
                std::memcpy(buffer.data() + position, &BoneWeightBuffer.at(i).y, size);
                position += size;
                std::memcpy(buffer.data() + position, &BoneWeightBuffer.at(i).z, size);
                position += size;
                std::memcpy(buffer.data() + position, &BoneWeightBuffer.at(i).w, size);
                position += size;
              } break;

              case video::E_ATTRIBUTE_TYPE::BONE_ID:
              {
                const auto size = GetAttributeComponentSize(attr.Format);
                std::memcpy(buffer.data() + position, &BoneIDBuffer.at(i).x, size);
                position += size;
                std::memcpy(buffer.data() + position, &BoneIDBuffer.at(i).y, size);
                position += size;
                std::memcpy(buffer.data() + position, &BoneIDBuffer.at(i).z, size);
                position += size;
                std::memcpy(buffer.data() + position, &BoneIDBuffer.at(i).w, size);
                position += size;
              } break;

              case video::E_ATTRIBUTE_TYPE::CUSTOM:
              {
                const auto size = GetAttributeSize(attr.Format);
                std::memcpy(buffer.data() + position, AttributeBuffer.at(i).data() + customAttributeCount++ * size, size);
                position += size;
              } break;
            }
          }
        }
      }
    }

    //! Get pointer to vertices
    /** \return Pointer to vertices. */
    virtual void* getVertices() override
    {
      return PositionBuffer.data();
    }

    //! Get pointer to GPU staging buffer
    /** \return Pointer to staging buffer. */
    virtual const void* getData(const video::PipelineHandle pipeline) const override
    {
      return GPUBuffers.at(pipeline).data();
    }

    //! Get size of GPU staging buffer
    /** \return Size of staging buffer. */
    virtual std::size_t getSize(const video::PipelineHandle pipeline) const override
    {
      return GPUBuffers.at(pipeline).size();
    }

    //! Get number of vertices
    /** \return Number of vertices. */
    virtual std::size_t getVertexCount() const override
    {
      return PositionBuffer.size();
    }

    //! Set number of vertices
    virtual void setVertexCount(const size_t count) override { }

    //! Get pointer to indices
    /** \return Pointer to indices. */
    virtual const std::uint32_t* getIndices() const override
    {
      return Indices.data();
    }

    //! returns vertex index at this offset
    virtual std::uint32_t getIndex(std::size_t offset) const override
    {
      return Indices.at(offset);
    }

    //! Get pointer to indices
    /** \return Pointer to indices. */
    virtual std::uint32_t* getIndices() override
    {
      return Indices.data();
    }

    //! Get number of indices
    /** \return Number of indices. */
    virtual std::uint32_t getIndexCount() const override
    {
      return Indices.size();
    }

    //! Set number of indices
    virtual void setIndexCount(const std::uint32_t count) override { }

    //! Get the axis aligned bounding box
    /** \return Axis aligned bounding box of this buffer. */
    virtual const core::aabbox3d<float>& getBoundingBox() const override
    {
      return BoundingBox;
    }

    //! Set the axis aligned bounding box
    /** \param box New axis aligned bounding box for this buffer. */
    //! set user axis aligned bounding box
    virtual void setBoundingBox(const core::aabbox3df& box) override
    {
      BoundingBox = box;
    }

    //! Recalculate the bounding box.
    /** should be called if the mesh changed. */
    virtual void recalculateBoundingBox() override
    {
      if (!PositionBuffer.empty())
      {
        BoundingBox.reset(PositionBuffer[0]);
        const std::uint32_t vsize = PositionBuffer.size();
        for (std::uint32_t i=1; i < vsize; ++i)
          BoundingBox.addInternalPoint(PositionBuffer[i]);
      }
      else
        BoundingBox.reset(0,0,0);
    }

    //! returns position of vertex i
    virtual const glm::vec3& getPosition(std::uint32_t i) const override
    {
      return PositionBuffer[i];
    }

    //! returns normal of vertex i
    virtual const glm::vec3& getNormal(std::uint32_t i) const override
    {
      return NormalBuffer[i];
    }

    //! returns texture coord of vertex i
    virtual const glm::vec2& getTCoords(std::uint32_t i) const override
    {
      return TCoordBuffer[i];
    }

    //! returns tangent of vertex i
    virtual const glm::vec3& getTangent(std::uint32_t i) const override
    {
      return TangentBuffer[i];
    }

    //! returns bi-tangent of vertex i
    virtual const glm::vec3& getBiTangent(std::uint32_t i) const override
    {
      return BiTangentBuffer[i];
    }

    //! Append the vertices and indices to the current buffer
    /** Only works for compatible types, i.e. either the same type
    or the main buffer is of standard type. Otherwise, behavior is
    undefined.
    */
    virtual void append(
      std::vector<S3DVertex>&& vertices,
      std::vector<uint32_t>&& indices) override
    {
      const std::uint32_t vertexCount = getVertexCount();
      std::uint32_t i;

      for (i = 0; i < vertices.size(); ++i)
      {
        PositionBuffer.push_back(std::move(vertices[i].Position));
        NormalBuffer.push_back(std::move(vertices[i].Normal));
        ColorBuffer.push_back(std::move(vertices[i].Color));
        TCoordBuffer.push_back(std::move(vertices[i].TextureCoords));
        TangentBuffer.push_back(std::move(vertices[i].Tangent));
        BiTangentBuffer.push_back(std::move(vertices[i].BiTangent));
        BoneWeightBuffer.push_back(std::move(vertices[i].BoneWeights));
        BoneIDBuffer.push_back(std::move(vertices[i].BoneIDs));
        if (vertices[i].Attributes.empty() == false)
          AttributeBuffer.push_back(std::move(vertices[i].Attributes));
        BoundingBox.addInternalPoint(PositionBuffer.back());
      }

      for (i = 0; i < indices.size(); ++i)
      {
        Indices.push_back(indices[i]+vertexCount);
      }
    }

    //! Append the custom attribute buffer to the current mesh buffer
    /**
    \param buffer Pointer to the buffer containing data
    \param size Total size of buffer (bytes)
    \param stride The custom data size of each vertex (bytes) */
    virtual void appendAttribute(const char* buffer, const size_t size, const size_t stride) override
    {
      for (size_t i = 0; i < size / stride; ++i)
      {
        std::vector<unsigned char> buf(buffer + i * stride, buffer + (i+1) * stride);
        AttributeBuffer.push_back(std::move(buf));
      }
    }

    //! Append the meshbuffer to the current buffer
    /** Only works for compatible types, i.e. either the same type
    or the main buffer is of standard type. Otherwise, behavior is
    undefined.
    \param other Meshbuffer to be appended to this one.
    */
    virtual void append(const IMeshBuffer* const other) override
    {
      /*
      if (this==other)
        return;

      const std::uint32_t vertexCount = getVertexCount();
      std::uint32_t i;

      PositionBuffer.reallocate(vertexCount+other->getVertexCount());
      for (i = 0; i < other->getVertexCount(); ++i)
      {
        PositionBuffer.push_back(reinterpret_cast<const T*>(other->getPositionBuffer())[i]);
      }

      Indices.reallocate(getIndexCount()+other->getIndexCount());
      for (i = 0; i < other->getIndexCount(); ++i)
      {
        Indices.push_back(other->getIndices()[i]+vertexCount);
      }
      BoundingBox.addInternalBox(other->getBoundingBox());
      */
    }

    const std::uint64_t ID;

    std::vector<glm::vec3> PositionBuffer;
    std::vector<glm::vec3> NormalBuffer;
    std::vector<glm::vec4> ColorBuffer;
    std::vector<glm::vec2> TCoordBuffer;
    std::vector<glm::vec3> TangentBuffer;
    std::vector<glm::vec3> BiTangentBuffer;
    std::vector<glm::vec4> BoneWeightBuffer;
    std::vector<glm::vec4> BoneIDBuffer;
    std::vector<std::vector<unsigned char>> AttributeBuffer;

    //! GPU staging buffer
    std::unordered_map<video::PipelineHandle, std::vector<unsigned char>> GPUBuffers;
    //! Indices into the vertices of this buffer.
    std::vector<std::uint32_t> Indices;
    //! Bounding box of this meshbuffer.
    core::aabbox3d<float> BoundingBox;
  };

} // namespace scene
} // namespace saga

#endif


