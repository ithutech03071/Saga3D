
#ifndef __I_SCENE_MANAGER_H_INCLUDED__
#define __I_SCENE_MANAGER_H_INCLUDED__

#include "SColor.h"
#include "IEventReceiver.h"
#include "ESceneNodeTypes.h"
#include "ESceneNodeAnimatorTypes.h"
#include "ISceneNode.h"
#include "IMesh.h"
#include "SRenderPass.h"
#include "SPipeline.h"
#include <vector>
#include <unordered_map>

namespace saga
{
namespace video
{
  class IVideoDriver;
  class ITexture;
} // namespace video

namespace scene
{
  class IAnimatedMesh;
  class ISkinnedMesh;
  class IAnimatedMeshSceneNode;
  class ICameraSceneNode;
  class IMeshBuffer;
  class IMeshLoader;
  class IMeshSceneNode;
  // class ISceneNodeAnimator;
  // class ISceneNodeAnimatorCollisionResponse;
  // class ISceneNodeAnimatorFactory;
  // class ISceneNodeFactory;

  class ISceneManager : public IEventReceiver
  {
  public:
    using SceneNodeList = std::vector<std::shared_ptr<ISceneNode>>;
    using PassSceneNodeList = std::unordered_map<video::RenderPassHandle, SceneNodeList>;

    virtual void resetID() = 0;
    virtual ID getMeshID() = 0;
    virtual ID getTextureID() = 0;
    virtual ID getNodeID() = 0;

    virtual void onEvent(const SDL_Event& event) override {}

    //! Loads a mesh from given file
    /*
      \param fileName: path to mesh file
    */
    virtual const std::shared_ptr<IMesh>& getMesh(const std::string& fileName) = 0;

    virtual const std::shared_ptr<IMesh>& getMesh(void* data, const std::size_t size,
      const std::string& extension) = 0;

    virtual const std::shared_ptr<IMesh>& getMesh(const ID id) const = 0;

    virtual void removeMesh(const std::shared_ptr<IMesh>& mesh) = 0;

    virtual void removeMesh(const ID id) = 0;

    //! Adds a scene node for rendering a static mesh.
    /** \param mesh: Pointer to the loaded static mesh to be displayed.
    \param parent: Parent of the scene node. Can be NULL if no parent.
    \param id: Id of the node. This id can be used to identify the scene node.
    \param position: Position of the space relative to its parent where the
    scene node will be placed.
    \param rotation: Initial rotation of the scene node.
    \param scale: Initial scale of the scene node.
    \param alsoAddIfMeshPointerZero: Add the scene node even if a 0 pointer is passed.
    \return Pointer to the created scene node.
    This pointer should not be dropped. See IReferenceCounted::drop() for more information. */
    virtual std::shared_ptr<ISceneNode> createSceneNode(
      const std::shared_ptr<IMesh>& mesh, const std::shared_ptr<ISceneNode>& parent = nullptr,
      const glm::vec3& position = glm::vec3(0,0,0),
      const glm::vec3& rotation = glm::vec3(0,0,0),
      const glm::vec3& scale = glm::vec3(1.0f, 1.0f, 1.0f)) = 0;

    virtual void removeNode(const ID id) = 0;

    virtual void removeNode(const std::shared_ptr<ISceneNode>& node) = 0;

    //! Get interface to the mesh cache which is shared between all existing scene managers.
    /** With this interface, it is possible to manually add new loaded
    meshes (if ISceneManager::getMesh() is not sufficient), to remove them and to iterate
    through already loaded meshes. */

    //! Get the video driver.
    /** \return Pointer to the video Driver.
    This pointer should not be dropped. See IReferenceCounted::drop() for more information. */
    virtual const std::shared_ptr<video::IVideoDriver>& getVideoDriver() const = 0;

    virtual void registerNode(const std::shared_ptr<ISceneNode>& node, video::RenderPassHandle pass) = 0;

    virtual const std::shared_ptr<ISceneNode>& getNode(const ID id) const = 0;

    virtual void unregisterNode(const std::shared_ptr<ISceneNode>& node, video::RenderPassHandle pass) = 0;

    virtual bool isEmptyPass(video::RenderPassHandle pass) const = 0;

    virtual void clearRegisteredNodes() = 0;

    virtual SceneNodeList& getNodeList(const video::RenderPassHandle pass) = 0;

    virtual PassSceneNodeList& getNodeList() = 0;

    //! Adds a cube scene node
    /** \param size: Size of the cube, uniformly in each dimension.
    \param parent: Parent of the scene node. Can be 0 if no parent.
    \param id: Id of the node. This id can be used to identify the scene node.
    \param position: Position of the space relative to its parent
    where the scene node will be placed.
    \param rotation: Initial rotation of the scene node.
    \param scale: Initial scale of the scene node.
    \return Pointer to the created test scene node. This
    pointer should not be dropped. See IReferenceCounted::drop()
    for more information. */
    // virtual IMeshSceneNode* addCubeSceneNode(float size=10.0f, ISceneNode* parent= 0, std::int32_t id=-1,
    //   const glm::vec3& position = glm::vec3(0,0,0),
    //   const glm::vec3& rotation = glm::vec3(0,0,0),
    //   const glm::vec3& scale = glm::vec3(1.0f, 1.0f, 1.0f)) = 0;

    //! Adds a sphere scene node of the given radius and detail
    /** \param radius: Radius of the sphere.
    \param polyCount: The number of vertices in horizontal and
    vertical direction. The total polyCount of the sphere is
    polyCount*polyCount. This parameter must be less than 256 to
    stay within the 16-bit limit of the indices of a meshbuffer.
    \param parent: Parent of the scene node. Can be 0 if no parent.
    \param id: Id of the node. This id can be used to identify the scene node.
    \param position: Position of the space relative to its parent
    where the scene node will be placed.
    \param rotation: Initial rotation of the scene node.
    \param scale: Initial scale of the scene node.
    \return Pointer to the created test scene node. This
    pointer should not be dropped. See IReferenceCounted::drop()
    for more information. */
    // virtual IMeshSceneNode* addSphereSceneNode(float radius=5.0f, std::int32_t polyCount=16,
    //     ISceneNode* parent= 0, std::int32_t id=-1,
    //     const glm::vec3& position = glm::vec3(0,0,0),
    //     const glm::vec3& rotation = glm::vec3(0,0,0),
    //     const glm::vec3& scale = glm::vec3(1.0f, 1.0f, 1.0f)) = 0;

    //! Adds a scene node for rendering an animated mesh model.
    /** \param mesh: Pointer to the loaded animated mesh to be displayed.
    \param parent: Parent of the scene node. Can be NULL if no parent.
    \param id: Id of the node. This id can be used to identify the scene node.
    \param position: Position of the space relative to its parent where the
    scene node will be placed.
    \param rotation: Initial rotation of the scene node.
    \param scale: Initial scale of the scene node.
    \param alsoAddIfMeshPointerZero: Add the scene node even if a 0 pointer is passed.
    \return Pointer to the created scene node.
    This pointer should not be dropped. See IReferenceCounted::drop() for more information. */
    // virtual IAnimatedMeshSceneNode* addAnimatedMeshSceneNode(IAnimatedMesh* mesh,
    //     ISceneNode* parent= 0, std::int32_t id=-1,
    //     const glm::vec3& position = glm::vec3(0,0,0),
    //     const glm::vec3& rotation = glm::vec3(0,0,0),
    //     const glm::vec3& scale = glm::vec3(1.0f, 1.0f, 1.0f),
    //     bool alsoAddIfMeshPointerZero=false) = 0;

    //! Adds a scene node for rendering a animated water surface mesh.
    /** Looks really good when the Material type E_MATERIAL_TYPE::TRANSPARENT_REFLECTION
    is used.
    \param waveHeight: Height of the water waves.
    \param waveSpeed: Speed of the water waves.
    \param waveLength: Length of a water wave.
    \param mesh: Pointer to the loaded static mesh to be displayed with water waves on it.
    \param parent: Parent of the scene node. Can be NULL if no parent.
    \param id: Id of the node. This id can be used to identify the scene node.
    \param position: Position of the space relative to its parent where the
    scene node will be placed.
    \param rotation: Initial rotation of the scene node.
    \param scale: Initial scale of the scene node.
    \return Pointer to the created scene node.
    This pointer should not be dropped. See IReferenceCounted::drop() for more information. */
    // virtual ISceneNode* addWaterSurfaceSceneNode(IMesh* mesh,
    //   float waveHeight=2.0f, float waveSpeed=300.0f, float waveLength=10.0f,
    //   ISceneNode* parent= 0, std::int32_t id=-1,
    //   const glm::vec3& position = glm::vec3(0,0,0),
    //   const glm::vec3& rotation = glm::vec3(0,0,0),
    //   const glm::vec3& scale = glm::vec3(1.0f, 1.0f, 1.0f)) = 0;


    //! Adds a scene node for rendering using a octree to the scene graph.
    /** This a good method for rendering
    scenes with lots of geometry. The octree is built on the fly from the mesh.
    \param mesh: The mesh containing all geometry from which the octree will be build.
    If this animated mesh has more than one frames in it, the first frame is taken.
    \param parent: Parent node of the octree node.
    \param id: id of the node. This id can be used to identify the node.
    \param minimalPolysPerNode: Specifies the minimal polygons contained a octree node.
    If a node gets less polys than this value it will not be split into
    smaller nodes.
    \param alsoAddIfMeshPointerZero: Add the scene node even if a 0 pointer is passed.
    \return Pointer to the octree if successful, otherwise 0.
    This pointer should not be dropped. See IReferenceCounted::drop() for more information. */
    // virtual IOctreeSceneNode* addOctreeSceneNode(IAnimatedMesh* mesh, ISceneNode* parent= 0,
    //   std::int32_t id=-1, std::int32_t minimalPolysPerNode=512, bool alsoAddIfMeshPointerZero=false) = 0;

    //! Adds a scene node for rendering using a octree to the scene graph.
    /** This a good method for rendering scenes with lots of
    geometry. The octree is built on the fly from the mesh, much
    faster then a bsp tree.
    \param mesh: The mesh containing all geometry from which the octree will be build.
    \param parent: Parent node of the octree node.
    \param id: id of the node. This id can be used to identify the node.
    \param minimalPolysPerNode: Specifies the minimal polygons contained a octree node.
    If a node gets less polys than this value it will not be split into
    smaller nodes.
    \param alsoAddIfMeshPointerZero: Add the scene node even if a 0 pointer is passed.
    \return Pointer to the octree if successful, otherwise 0.
    This pointer should not be dropped. See IReferenceCounted::drop() for more information. */
    // virtual IOctreeSceneNode* addOctreeSceneNode(IMesh* mesh, ISceneNode* parent= 0,
    //   std::int32_t id=-1, std::int32_t minimalPolysPerNode=256, bool alsoAddIfMeshPointerZero=false) = 0;

    //! Adds a camera scene node to the scene graph and sets it as active camera.
    /** This camera does not react on user input like for example the one created with
    addCameraSceneNodeFPS(). If you want to move or animate it, use animators or the
    ISceneNode::setPosition(), ICameraSceneNode::setTarget() etc methods.
    By default, a camera's look at position (set with setTarget()) and its scene node
    rotation (set with setRotation()) are independent. If you want to be able to
    control the direction that the camera looks by using setRotation() then call
    ICameraSceneNode::bindTargetAndRotation(true) on it.
    \param position: Position of the space relative to its parent where the camera will be placed.
    \param lookAt: Position where the camera will look at. Also known as target.
    \param parent: Parent scene node of the camera. Can be null. If the parent moves,
    the camera will move too.
    \param id: id of the camera. This id can be used to identify the camera.
    \param makeActive Flag whether this camera should become the active one.
    Make sure you always have one active camera.
    \return Pointer to interface to camera if successful, otherwise 0.
    This pointer should not be dropped. See IReferenceCounted::drop() for more information. */
    virtual const std::shared_ptr<ICameraSceneNode>& addCameraSceneNode(
      const std::shared_ptr<ISceneNode>& parent = nullptr,
      const glm::vec3& position = glm::vec3(0,0,0),
      const glm::vec3& lookAt = glm::vec3(0,0,-1),
      bool makeActive = true) = 0;

    //! Adds a maya style user controlled camera scene node to the scene graph.
    /** This is a standard camera with an animator that provides mouse control similar
    to camera in the 3D Software Maya by Alias Wavefront.
    The camera does not react on setPosition anymore after applying this animator. Instead
    use setTarget, to fix the target the camera the camera hovers around. And setDistance
    to set the current distance from that target, i.e. the radius of the orbit the camera
    hovers on.
    \param parent: Parent scene node of the camera. Can be null.
    \param rotateSpeed: Rotation speed of the camera.
    \param zoomSpeed: Zoom speed of the camera.
    \param translationSpeed: TranslationSpeed of the camera.
    \param id: id of the camera. This id can be used to identify the camera.
    \param distance Initial distance of the camera from the object
    \param makeActive Flag whether this camera should become the active one.
    Make sure you always have one active camera.
    \return Returns a pointer to the interface of the camera if successful, otherwise 0.
    This pointer should not be dropped. See IReferenceCounted::drop() for more information. */
    // virtual ICameraSceneNode* addCameraSceneNodeMaya(ISceneNode* parent= 0,
    //   float rotateSpeed=-1500.f, float zoomSpeed=200.f,
    //   float translationSpeed=1500.f, std::int32_t id=-1, float distance=70.f,
    //   bool makeActive=true) = 0;

    //! Adds a camera scene node with an animator which provides mouse and keyboard control appropriate for first person shooters (FPS).
    /** This FPS camera is intended to provide a demonstration of a
    camera that behaves like a typical First Person Shooter. It is
    useful for simple demos and prototyping but is not intended to
    provide a full solution for a production quality game. It binds
    the camera scene node rotation to the look-at target; @see
    ICameraSceneNode::bindTargetAndRotation(). With this camera,
    you look with the mouse, and move with cursor keys.
    \param parent: Parent scene node of the camera. Can be null.
    \param rotateSpeed: Speed in degrees with which the camera is
    rotated. This can be done only with the mouse.
    \param moveSpeed: Speed in units per millisecond with which
    the camera is moved. Movement is done with the cursor keys.
    \param id: id of the camera. This id can be used to identify
    the camera.
    \param keyMapArray: Optional pointer to an array of a keymap,
    specifying what keys should be used to move the camera. If this
    is null, the default keymap is used. You can define actions
    more then one time in the array, to bind multiple keys to the
    same action.
    \param keyMapSize: Amount of items in the keymap array.
    \param noVerticalMovement: Setting this to true makes the
    camera only move within a horizontal plane, and disables
    vertical movement as known from most ego shooters. Default is
    'false', with which it is possible to fly around in space, if
    no gravity is there.
    \param jumpSpeed: Speed with which the camera is moved when
    jumping.
    \param invertMouse: Setting this to true makes the camera look
    up when the mouse is moved down and down when the mouse is
    moved up, the default is 'false' which means it will follow the
    movement of the mouse cursor.
    \param makeActive Flag whether this camera should become the active one.
    Make sure you always have one active camera.
    \return Pointer to the interface of the camera if successful,
    otherwise nullptr. */
    virtual const std::shared_ptr<ICameraSceneNode>& addCameraSceneNodeFPS(
      const std::shared_ptr<ISceneNode>& parent = nullptr,
      float moveSpeed = 0.5f, float rotateSpeed = 1.f,
      bool makeActive = true) = 0;

    //! Adds a dynamic light scene node to the scene graph.
    /** The light will cast dynamic light on all
    other scene nodes in the scene, which have the material flag video::MTF_LIGHTING
    turned on. (This is the default setting in most scene nodes).
    \param parent: Parent scene node of the light. Can be null. If the parent moves,
    the light will move too.
    \param position: Position of the space relative to its parent where the light will be placed.
    \param color: Diffuse color of the light. Ambient or Specular colors can be set manually with
    the ILightSceneNode::getLightData() method.
    \param radius: Radius of the light.
    \param id: id of the node. This id can be used to identify the node.
    \return Pointer to the interface of the light if successful, otherwise NULL.
    This pointer should not be dropped. See IReferenceCounted::drop() for more information. */
    // virtual ILightSceneNode* addLightSceneNode(ISceneNode* parent = 0,
    //   const glm::vec3& position = glm::vec3(0,0,0),
    //   video::SColorf color = video::SColorf(1.0f, 1.0f, 1.0f),
    //   float radius=100.0f, std::int32_t id=-1) = 0;

    //! Adds a billboard scene node to the scene graph.
    /** A billboard is like a 3d sprite: A 2d element,
    which always looks to the camera. It is usually used for things
    like explosions, fire, lensflares and things like that.
    \param parent Parent scene node of the billboard. Can be null.
    If the parent moves, the billboard will move too.
    \param size Size of the billboard. This size is 2 dimensional
    because a billboard only has width and height.
    \param position Position of the space relative to its parent
    where the billboard will be placed.
    \param id An id of the node. This id can be used to identify
    the node.
    \param colorTop The color of the vertices at the top of the
    billboard (default: white).
    \param colorBottom The color of the vertices at the bottom of
    the billboard (default: white).
    \return Pointer to the billboard if successful, otherwise NULL.
    This pointer should not be dropped. See
    IReferenceCounted::drop() for more information. */
    // virtual IBillboardSceneNode* addBillboardSceneNode(ISceneNode* parent = 0,
    //   const glm::vec2& size = glm::vec2(10.0f, 10.0f),
    //   const glm::vec3& position = glm::vec3(0,0,0), std::int32_t id=-1,
    //   video::SColor colorTop = 0xFFFFFFFF, video::SColor colorBottom = 0xFFFFFFFF) = 0;

    //! Adds a skybox scene node to the scene graph.
    /** A skybox is a big cube with 6 textures on it and
    is drawn around the camera position.
    \param top: Texture for the top plane of the box.
    \param bottom: Texture for the bottom plane of the box.
    \param left: Texture for the left plane of the box.
    \param right: Texture for the right plane of the box.
    \param front: Texture for the front plane of the box.
    \param back: Texture for the back plane of the box.
    \param parent: Parent scene node of the skybox. A skybox usually has no parent,
    so this should be null. Note: If a parent is set to the skybox, the box will not
    change how it is drawn.
    \param id: An id of the node. This id can be used to identify the node.
    \return Pointer to the sky box if successful, otherwise NULL.
    This pointer should not be dropped. See IReferenceCounted::drop() for more information. */
    // virtual ISceneNode* addSkyBoxSceneNode(video::ITexture* top, video::ITexture* bottom,
    //   video::ITexture* left, video::ITexture* right, video::ITexture* front,
    //   video::ITexture* back, ISceneNode* parent = 0, std::int32_t id=-1) = 0;

    //! Adds a skydome scene node to the scene graph.
    /** A skydome is a large (half-) sphere with a panoramic texture
    on the inside and is drawn around the camera position.
    \param texture: Texture for the dome.
    \param horiRes: Number of vertices of a horizontal layer of the sphere.
    \param vertRes: Number of vertices of a vertical layer of the sphere.
    \param texturePercentage: How much of the height of the
    texture is used. Should be between 0 and 1.
    \param spherePercentage: How much of the sphere is drawn.
    Value should be between 0 and 2, where 1 is an exact
    half-sphere and 2 is a full sphere.
    \param radius The Radius of the sphere
    \param parent: Parent scene node of the dome. A dome usually has no parent,
    so this should be null. Note: If a parent is set, the dome will not
    change how it is drawn.
    \param id: An id of the node. This id can be used to identify the node.
    \return Pointer to the sky dome if successful, otherwise NULL.
    This pointer should not be dropped. See IReferenceCounted::drop() for more information. */
    // virtual ISceneNode* addSkyDomeSceneNode(video::ITexture* texture,
    //   std::uint32_t horiRes=16, std::uint32_t vertRes=8,
    //   float texturePercentage= 0.9, float spherePercentage=2.0,float radius = 1000.f,
    //   ISceneNode* parent= 0, std::int32_t id=-1) = 0;

    //! Adds a particle system scene node to the scene graph.
    /** \param withDefaultEmitter: Creates a default working point emitter
    which emits some particles. Set this to true to see a particle system
    in action. If set to false, you'll have to set the emitter you want by
    calling IParticleSystemSceneNode::setEmitter().
    \param parent: Parent of the scene node. Can be NULL if no parent.
    \param id: Id of the node. This id can be used to identify the scene node.
    \param position: Position of the space relative to its parent where the
    scene node will be placed.
    \param rotation: Initial rotation of the scene node.
    \param scale: Initial scale of the scene node.
    \return Pointer to the created scene node.
    This pointer should not be dropped. See IReferenceCounted::drop() for more information. */
    // virtual IParticleSystemSceneNode* addParticleSystemSceneNode(
    //   bool withDefaultEmitter=true, ISceneNode* parent= 0, std::int32_t id=-1,
    //   const glm::vec3& position = glm::vec3(0,0,0),
    //   const glm::vec3& rotation = glm::vec3(0,0,0),
    //   const glm::vec3& scale = glm::vec3(1.0f, 1.0f, 1.0f)) = 0;

    //! Adds a terrain scene node to the scene graph.
    /** This node implements is a simple terrain renderer which uses
    a technique known as geo mip mapping
    for reducing the detail of triangle blocks which are far away.
    The code for the TerrainSceneNode is based on the terrain
    renderer by Soconne and the GeoMipMapSceneNode developed by
    Spintz. They made their code available for Irrlicht and allowed
    it to be distributed under this licence. I only modified some
    parts. A lot of thanks go to them.

    This scene node is capable of loading terrains and updating
    the indices at runtime to enable viewing very large terrains
    very quickly. It uses a CLOD (Continuous Level of Detail)
    algorithm which updates the indices for each patch based on
    a LOD (Level of Detail) which is determined based on a patch's
    distance from the camera.

    The patch size of the terrain must always be a size of 2^N+1,
    i.e. 8+1(9), 16+1(17), etc.
    The MaxLOD available is directly dependent on the patch size
    of the terrain. LOD 0 contains all of the indices to draw all
    the triangles at the max detail for a patch. As each LOD goes
    up by 1 the step taken, in generating indices increases by
    -2^LOD, so for LOD 1, the step taken is 2, for LOD 2, the step
    taken is 4, LOD 3 - 8, etc. The step can be no larger than
    the size of the patch, so having a LOD of 8, with a patch size
    of 17, is asking the algorithm to generate indices every 2^8 (
    256) vertices, which is not possible with a patch size of 17.
    The maximum LOD for a patch size of 17 is 2^4 (16). So,
    with a MaxLOD of 5, you'll have LOD 0 (full detail), LOD 1 (
    every 2 vertices), LOD 2 (every 4 vertices), LOD 3 (every
    8 vertices) and LOD 4 (every 16 vertices).
    \param heightMapFileName: The name of the file on disk, to read vertex data from. This should
    be a gray scale bitmap.
    \param parent: Parent of the scene node. Can be 0 if no parent.
    \param id: Id of the node. This id can be used to identify the scene node.
    \param position: The absolute position of this node.
    \param rotation: The absolute rotation of this node. (NOT YET IMPLEMENTED)
    \param scale: The scale factor for the terrain. If you're
    using a heightmap of size 129x129 and would like your terrain
    to be 12900x12900 in game units, then use a scale factor of (
    core::vector (100.0f, 100.0f, 100.0f). If you use a Y
    scaling factor of 0.0f, then your terrain will be flat.
    \param vertexColor: The default color of all the vertices. If no texture is associated
    with the scene node, then all vertices will be this color. Defaults to white.
    \param maxLOD: The maximum LOD (level of detail) for the node. Only change if you
    know what you are doing, this might lead to strange behavior.
    \param patchSize: patch size of the terrain. Only change if you
    know what you are doing, this might lead to strange behavior.
    \param smoothFactor: The number of times the vertices are smoothed.
    \param addAlsoIfHeightmapEmpty: Add terrain node even with empty heightmap.
    \return Pointer to the created scene node. Can be null
    if the terrain could not be created, for example because the
    heightmap could not be loaded. The returned pointer should
    not be dropped. See IReferenceCounted::drop() for more
    information. */
    // virtual ITerrainSceneNode* addTerrainSceneNode(
    //   const std::string& heightMapFileName,
    //     ISceneNode* parent= 0, std::int32_t id=-1,
    //   const glm::vec3& position = glm::vec3(0.0f,0.0f,0.0f),
    //   const glm::vec3& rotation = glm::vec3(0.0f,0.0f,0.0f),
    //   const glm::vec3& scale = glm::vec3(1.0f,1.0f,1.0f),
    //   video::SColor vertexColor = video::SColor(255,255,255,255),
    //   std::int32_t maxLOD=5, E_TERRAIN_PATCH_SIZE patchSize=E_TERRAIN_PATCH_SIZE::SIZE_17, std::int32_t smoothFactor= 0,
    //   bool addAlsoIfHeightmapEmpty = false) = 0;

    //! Adds a terrain scene node to the scene graph.
    /** Just like the other addTerrainSceneNode() method, but takes an IReadFile
    pointer as parameter for the heightmap. For more information take a look
    at the other function.
    \param heightMapFile: The file handle to read vertex data from. This should
    be a gray scale bitmap.
    \param parent: Parent of the scene node. Can be 0 if no parent.
    \param id: Id of the node. This id can be used to identify the scene node.
    \param position: The absolute position of this node.
    \param rotation: The absolute rotation of this node. (NOT YET IMPLEMENTED)
    \param scale: The scale factor for the terrain. If you're
    using a heightmap of size 129x129 and would like your terrain
    to be 12900x12900 in game units, then use a scale factor of (
    core::vector (100.0f, 100.0f, 100.0f). If you use a Y
    scaling factor of 0.0f, then your terrain will be flat.
    \param vertexColor: The default color of all the vertices. If no texture is associated
    with the scene node, then all vertices will be this color. Defaults to white.
    \param maxLOD: The maximum LOD (level of detail) for the node. Only change if you
    know what you are doing, this might lead to strange behavior.
    \param patchSize: patch size of the terrain. Only change if you
    know what you are doing, this might lead to strange behavior.
    \param smoothFactor: The number of times the vertices are smoothed.
    \param addAlsoIfHeightmapEmpty: Add terrain node even with empty heightmap.
    \return Pointer to the created scene node. Can be null
    if the terrain could not be created, for example because the
    heightmap could not be loaded. The returned pointer should
    not be dropped. See IReferenceCounted::drop() for more
    information. */
    // virtual ITerrainSceneNode* addTerrainSceneNode(
    //   io::IReadFile* heightMapFile,
    //   ISceneNode* parent= 0, std::int32_t id=-1,
    //   const glm::vec3& position = glm::vec3(0.0f,0.0f,0.0f),
    //   const glm::vec3& rotation = glm::vec3(0.0f,0.0f,0.0f),
    //   const glm::vec3& scale = glm::vec3(1.0f,1.0f,1.0f),
    //   video::SColor vertexColor = video::SColor(255,255,255,255),
    //   std::int32_t maxLOD=5, E_TERRAIN_PATCH_SIZE patchSize=E_TERRAIN_PATCH_SIZE::SIZE_17, std::int32_t smoothFactor= 0,
    //   bool addAlsoIfHeightmapEmpty = false) = 0;

    //! Adds an empty scene node to the scene graph.
    /** Can be used for doing advanced transformations
    or structuring the scene graph.
    \return Pointer to the created scene node.
    This pointer should not be dropped. See IReferenceCounted::drop() for more information. */
    // virtual ISceneNode* addEmptySceneNode(ISceneNode* parent= 0, std::int32_t id=-1) = 0;

    //! Adds a dummy transformation scene node to the scene graph.
    /** This scene node does not render itself, and does not respond to set/getPosition,
    set/getRotation and set/getScale. Its just a simple scene node that takes a
    matrix as relative transformation, making it possible to insert any transformation
    anywhere into the scene graph.
    \return Pointer to the created scene node.
    This pointer should not be dropped. See IReferenceCounted::drop() for more information. */
    // virtual IDummyTransformationSceneNode* addDummyTransformationSceneNode(
    //   ISceneNode* parent= 0, std::int32_t id=-1) = 0;

    //! Adds a text scene node, which is able to display 2d text at a position in three dimensional space
    // virtual ITextSceneNode* addTextSceneNode(gui::IGUIFont* font, const char* text,
    //   video::SColor color=video::SColor(100,255,255,255),
    //   ISceneNode* parent = 0, const glm::vec3& position = glm::vec3(0,0,0),
    //   std::int32_t id=-1) = 0;

    //! Adds a text scene node, which uses billboards. The node, and the text on it, will scale with distance.
    /**
    \param font The font to use on the billboard. Pass 0 to use the GUI environment's default font.
    \param text The text to display on the billboard.
    \param parent The billboard's parent. Pass 0 to use the root scene node.
    \param size The billboard's width and height.
    \param position The billboards position relative to its parent.
    \param id: An id of the node. This id can be used to identify the node.
    \param colorTop: The color of the vertices at the top of the billboard (default: white).
    \param colorBottom: The color of the vertices at the bottom of the billboard (default: white).
    \return Pointer to the billboard if successful, otherwise NULL.
    This pointer should not be dropped. See IReferenceCounted::drop() for more information. */
    // virtual IBillboardTextSceneNode* addBillboardTextSceneNode(gui::IGUIFont* font, const char* text,
    //   ISceneNode* parent = 0,
    //   const glm::vec2& size = glm::vec2(10.0f, 10.0f),
    //   const glm::vec3& position = glm::vec3(0,0,0), std::int32_t id=-1,
    //   video::SColor colorTop = 0xFFFFFFFF, video::SColor colorBottom = 0xFFFFFFFF) = 0;

    //! Adds a Hill Plane mesh to the mesh pool.
    /** The mesh is generated on the fly
    and looks like a plane with some hills on it. It is uses mostly for quick
    tests of the engine only. You can specify how many hills there should be
    on the plane and how high they should be. Also you must specify a name for
    the mesh, because the mesh is added to the mesh pool, and can be retrieved
    again using ISceneManager::getMesh() with the name as parameter.
    \param name: The name of this mesh which must be specified in order
    to be able to retrieve the mesh later with ISceneManager::getMesh().
    \param tileSize: Size of a tile of the mesh. (10.0f, 10.0f) would be a
    good value to start, for example.
    \param tileCount: Specifies how much tiles there will be. If you specify
    for example that a tile has the size (10.0f, 10.0f) and the tileCount is
    (10,10), than you get a field of 100 tiles which has the dimension 100.0f x 100.0f.
    \param material: Material of the hill mesh.
    \param hillHeight: Height of the hills. If you specify a negative value
    you will get holes instead of hills. If the height is 0, no hills will be
    created.
    \param countHills: Amount of hills on the plane. There will be countHills.X
    hills along the X axis and countHills.Y along the Y axis. So in total there
    will be countHills.X * countHills.Y hills.
    \param textureRepeatCount: Defines how often the texture will be repeated in
    x and y direction.
    return Null if the creation failed. The reason could be that you
    specified some invalid parameters or that a mesh with that name already
    exists. If successful, a pointer to the mesh is returned.
    This pointer should not be dropped. See IReferenceCounted::drop() for more information. */
    // virtual IAnimatedMesh* addHillPlaneMesh(const std::string& name,
    //   const glm::vec2& tileSize, const glm::uvec2& tileCount,
    //   video::SMaterial* material = 0, float hillHeight = 0.0f,
    //   const glm::vec2& countHills = glm::vec2(0.0f, 0.0f),
    //   const glm::vec2& textureRepeatCount = glm::vec2(1.0f, 1.0f)) = 0;

    //! Adds a static terrain mesh to the mesh pool.
    /** The mesh is generated on the fly
    from a texture file and a height map file. Both files may be huge
    (8000x8000 pixels would be no problem) because the generator splits the
    files into smaller textures if necessary.
    You must specify a name for the mesh, because the mesh is added to the mesh pool,
    and can be retrieved again using ISceneManager::getMesh() with the name as parameter.
    \param meshname: The name of this mesh which must be specified in order
    to be able to retrieve the mesh later with ISceneManager::getMesh().
    \param texture: Texture for the terrain. Please note that this is not a
    hardware texture as usual (ITexture), but an IImage software texture.
    You can load this texture with IVideoDriver::createImageFromFile().
    \param heightmap: A grayscaled heightmap image. Like the texture,
    it can be created with IVideoDriver::createImageFromFile(). The amount
    of triangles created depends on the size of this texture, so use a small
    heightmap to increase rendering speed.
    \param stretchSize: Parameter defining how big a is pixel on the heightmap.
    \param maxHeight: Defines how high a white pixel on the heightmap is.
    \param defaultVertexBlockSize: Defines the initial dimension between vertices.
    \return Null if the creation failed. The reason could be that you
    specified some invalid parameters, that a mesh with that name already
    exists, or that a texture could not be found. If successful, a pointer to the mesh is returned.
    This pointer should not be dropped. See IReferenceCounted::drop() for more information. */
    // virtual IAnimatedMesh* addTerrainMesh(const std::string& meshname,
    //   video::IImage* texture, video::IImage* heightmap,
    //   const glm::vec2& stretchSize = glm::vec2(10.0f,10.0f),
    //   float maxHeight=200.0f,
    //   const glm::uvec2& defaultVertexBlockSize = glm::uvec2(64,64)) = 0;

    //! add a static arrow mesh to the meshpool
    /** \param name Name of the mesh
    \param vtxColorCylinder color of the cylinder
    \param vtxColorCone color of the cone
    \param tesselationCylinder Number of quads the cylinder side consists of
    \param tesselationCone Number of triangles the cone's roof consists of
    \param height Total height of the arrow
    \param cylinderHeight Total height of the cylinder, should be lesser than total height
    \param widthCylinder Diameter of the cylinder
    \param widthCone Diameter of the cone's base, should be not smaller than the cylinder's diameter
    \return Pointer to the arrow mesh if successful, otherwise 0.
    This pointer should not be dropped. See IReferenceCounted::drop() for more information. */
    // virtual IAnimatedMesh* addArrowMesh(const std::string& name,
    //     video::SColor vtxColorCylinder= 0xFFFFFFFF,
    //     video::SColor vtxColorCone= 0xFFFFFFFF,
    //     std::uint32_t tesselationCylinder=4, std::uint32_t tesselationCone=8,
    //     float height=1.f, float cylinderHeight= 0.6f,
    //     float widthCylinder= 0.05f, float widthCone= 0.3f) = 0;

    //! add a static sphere mesh to the meshpool
    /** \param name Name of the mesh
    \param radius Radius of the sphere
    \param polyCountX Number of quads used for the horizontal tiling
    \param polyCountY Number of quads used for the vertical tiling
    \return Pointer to the sphere mesh if successful, otherwise 0.
    This pointer should not be dropped. See IReferenceCounted::drop() for more information. */
    // virtual IAnimatedMesh* addSphereMesh(const std::string& name,
    //     float radius=5.f, std::uint32_t polyCountX = 16,
    //     std::uint32_t polyCountY = 16) = 0;

    //! Gets the root scene node.
    /** This is the scene node which is parent
    of all scene nodes. The root scene node is a special scene node which
    only exists to manage all scene nodes. It will not be rendered and cannot
    be removed from the scene.
    \return Pointer to the root scene node.
    This pointer should not be dropped. See IReferenceCounted::drop() for more information. */
    virtual const std::shared_ptr<ISceneNode>& getRootSceneNode() const = 0;

    //! Get the first scene node with the specified id.
    /** \param id: The id to search for
    \param start: Scene node to start from. All children of this scene
    node are searched. If null is specified, the root scene node is
    taken.
    \return Pointer to the first scene node with this id,
    and null if no scene node could be found.
    This pointer should not be dropped. See IReferenceCounted::drop() for more information. */
    // virtual ISceneNode* getSceneNodeFromId(std::int32_t id, ISceneNode* start= 0) = 0;

    //! Get the first scene node with the specified name.
    /** \param name: The name to search for
    \param start: Scene node to start from. All children of this scene
    node are searched. If null is specified, the root scene node is
    taken.
    \return Pointer to the first scene node with this id,
    and null if no scene node could be found.
    This pointer should not be dropped. See IReferenceCounted::drop() for more information. */
    // virtual ISceneNode* getSceneNodeFromName(const char* name, ISceneNode* start= 0) = 0;

    //! Get the first scene node with the specified type.
    /** \param type: The type to search for
    \param start: Scene node to start from. All children of this scene
    node are searched. If null is specified, the root scene node is
    taken.
    \return Pointer to the first scene node with this type,
    and null if no scene node could be found.
    This pointer should not be dropped. See IReferenceCounted::drop() for more information. */
    // virtual ISceneNode* getSceneNodeFromType(scene::E_SCENE_NODE_TYPE type, ISceneNode* start= 0) = 0;

    //! Get scene nodes by type.
    /** \param type: Type of scene node to find (E_SCENE_NODE_TYPE::ANY will return all child nodes).
    \param outNodes: results will be added to this array (outNodes is not cleared).
    \param start: Scene node to start from. This node and all children of this scene
    node are checked (recursively, so also children of children, etc). If null is specified,
    the root scene node is taken as start-node. */
    // virtual void getSceneNodesFromType(E_SCENE_NODE_TYPE type,
    //     std::vector<scene::ISceneNode*>& outNodes,
    //     ISceneNode* start= 0) = 0;

    //! Get the current active camera.
    /** \return The active camera is returned. Note that this can
    be NULL, if there was no camera created yet.*/
    virtual const std::shared_ptr<ICameraSceneNode>& getActiveCamera() const = 0;

    //! Sets the currently active camera.
    /** The previous active camera will be deactivated.
    \param camera: The new camera which should be active. */
    virtual void setActiveCamera(const std::shared_ptr<ICameraSceneNode>& camera) = 0;

    //! Registers a node for rendering it at a specific time.
    /** This method should only be used by SceneNodes when they get a
    ISceneNode::onRegisterSceneNode(video::RenderPassHandle pass) call.
    \param node: Node to register for drawing. Usually scene nodes would set 'this'
    as parameter here because they want to be drawn.
    \param pass: Specifies when the node wants to be drawn in relation to the other nodes.
    For example, if the node is a shadow, it usually wants to be drawn after all other nodes
    and will use E_SCENE_NODE_RENDER_PASS::SHADOW for this. See scene::E_SCENE_NODE_RENDER_PASS for details.
    \return scene will be rendered (passed culling) */
    // virtual std::uint32_t registerNodeForRendering(ISceneNode* node,
      // E_SCENE_NODE_RENDER_PASS pass = E_SCENE_NODE_RENDER_PASS::AUTOMATIC) = 0;

    //! Clear all nodes which are currently registered for rendering
    /** Usually you don't have to care about this as drawAll will clear nodes
    after rendering them. But sometimes you might have to manully reset this.
    For example when you deleted nodes between registering and rendering. */
    // virtual void clearAllRegisteredNodesForRendering() = 0;

    //! Animate all the scene nodes.
    /** This should be called before rendering */
    virtual void animate(const video::RenderPassHandle pass = video::NULL_GPU_RESOURCE_HANDLE) = 0;

    //! Creates a rotation animator, which rotates the attached scene node around itself.
    /** \param rotationSpeed Specifies the speed of the animation in degree per 10 milliseconds.
    \return The animator. Attach it to a scene node with ISceneNode::addAnimator()
    and the animator will animate it.
    If you no longer need the animator, you should call ISceneNodeAnimator::drop().
    See IReferenceCounted::drop() for more information. */
    // virtual ISceneNodeAnimator* createRotationAnimator(const glm::vec3& rotationSpeed) = 0;

    //! Creates a fly circle animator, which lets the attached scene node fly around a center.
    /** \param center: Center of the circle.
    \param radius: Radius of the circle.
    \param speed: The orbital speed, in radians per millisecond.
    \param direction: Specifies the upvector used for alignment of the mesh.
    \param startPosition: The position on the circle where the animator will
    begin. Value is in multiples of a circle, i.e. 0.5 is half way around. (phase)
    \param radiusEllipsoid: if radiusEllipsoid != 0 then radius2 from a ellipsoid
    begin. Value is in multiples of a circle, i.e. 0.5 is half way around. (phase)
    \return The animator. Attach it to a scene node with ISceneNode::addAnimator()
    and the animator will animate it.
    If you no longer need the animator, you should call ISceneNodeAnimator::drop().
    See IReferenceCounted::drop() for more information. */
    // virtual ISceneNodeAnimator* createFlyCircleAnimator(
    //     const glm::vec3& center=glm::vec3(0.f,0.f,0.f),
    //     float radius=100.f, float speed= 0.001f,
    //     const glm::vec3& direction=glm::vec3(0.f, 1.f, 0.f),
    //     float startPosition = 0.f,
    //     float radiusEllipsoid = 0.f) = 0;

    //! Creates a fly straight animator, which lets the attached scene node fly or move along a line between two points.
    /** \param startPoint: Start point of the line.
    \param endPoint: End point of the line.
    \param timeForWay: Time in milliseconds how long the node should need to
    move from the start point to the end point.
    \param loop: If set to false, the node stops when the end point is reached.
    If loop is true, the node begins again at the start.
    \param pingpong Flag to set whether the animator should fly
    back from end to start again.
    \return The animator. Attach it to a scene node with ISceneNode::addAnimator()
    and the animator will animate it.
    If you no longer need the animator, you should call ISceneNodeAnimator::drop().
    See IReferenceCounted::drop() for more information. */
    // virtual ISceneNodeAnimator* createFlyStraightAnimator(const glm::vec3& startPoint,
    //   const glm::vec3& endPoint, std::uint32_t timeForWay, bool loop=false, bool pingpong = false) = 0;

    //! Creates a texture animator, which switches the textures of the target scene node based on a list of textures.
    /** \param textures: List of textures to use.
    \param timePerFrame: Time in milliseconds, how long any texture in the list
    should be visible.
    \param loop: If set to to false, the last texture remains set, and the animation
    stops. If set to true, the animation restarts with the first texture.
    \return The animator. Attach it to a scene node with ISceneNode::addAnimator()
    and the animator will animate it.
    If you no longer need the animator, you should call ISceneNodeAnimator::drop().
    See IReferenceCounted::drop() for more information. */
    // virtual ISceneNodeAnimator* createTextureAnimator(const std::vector<video::ITexture*>& textures,
    //   std::int32_t timePerFrame, bool loop=true) = 0;

    //! Creates a scene node animator, which deletes the scene node after some time automatically.
    /** \param timeMs: Time in milliseconds, after when the node will be deleted.
    \return The animator. Attach it to a scene node with ISceneNode::addAnimator()
    and the animator will animate it.
    If you no longer need the animator, you should call ISceneNodeAnimator::drop().
    See IReferenceCounted::drop() for more information. */
    // virtual ISceneNodeAnimator* createDeleteAnimator(std::uint32_t timeMs) = 0;

    //! Creates a special scene node animator for doing automatic collision detection and response.
    /** See ISceneNodeAnimatorCollisionResponse for details.
    \param world: Triangle selector holding all triangles of the world with which
    the scene node may collide. You can create a triangle selector with
    ISceneManager::createTriangleSelector();
    \param sceneNode: SceneNode which should be manipulated. After you added this animator
    to the scene node, the scene node will not be able to move through walls and is
    affected by gravity. If you need to teleport the scene node to a new position without
    it being effected by the collision geometry, then call sceneNode->setPosition(); then
    animator->setTargetNode(sceneNode);
    \param ellipsoidRadius: Radius of the ellipsoid with which collision detection and
    response is done. If you have got a scene node, and you are unsure about
    how big the radius should be, you could use the following code to determine
    it:
    \code
    const core::aabbox3d<float>& box = yourSceneNode->getBoundingBox();
    glm::vec3 radius = box.MaxEdge - box.getCenter();
    \endcode
    \param gravityPerSecond: Sets the gravity of the environment, as an acceleration in
    units per second per second. If your units are equivalent to meters, then
    glm::vec3(0,-10.0f,0) would give an approximately realistic gravity.
    You can disable gravity by setting it to glm::vec3(0,0,0).
    \param ellipsoidTranslation: By default, the ellipsoid for collision detection is created around
    the center of the scene node, which means that the ellipsoid surrounds
    it completely. If this is not what you want, you may specify a translation
    for the ellipsoid.
    \param slidingValue: DOCUMENTATION NEEDED.
    \return The animator. Attach it to a scene node with ISceneNode::addAnimator()
    and the animator will cause it to do collision detection and response.
    If you no longer need the animator, you should call ISceneNodeAnimator::drop().
    See IReferenceCounted::drop() for more information. */
    // virtual ISceneNodeAnimatorCollisionResponse* createCollisionResponseAnimator(
    //   ITriangleSelector* world, ISceneNode* sceneNode,
    //   const glm::vec3& ellipsoidRadius = glm::vec3(30,60,30),
    //   const glm::vec3& gravityPerSecond = glm::vec3(0,-10.0f,0),
    //   const glm::vec3& ellipsoidTranslation = glm::vec3(0,0,0),
    //   float slidingValue = 0.0005f) = 0;

    //! Creates a follow spline animator.
    /** The animator modifies the position of
    the attached scene node to make it follow a Hermite spline.
    It uses a subset of Hermite splines: either cardinal splines
    (tightness != 0.5) or Catmull-Rom-splines (tightness == 0.5).
    The animator moves from one control point to the next in
    1/speed seconds. This code was sent in by Matthias Gall.
    If you no longer need the animator, you should call ISceneNodeAnimator::drop().
    See IReferenceCounted::drop() for more information. */
    // virtual ISceneNodeAnimator* createFollowSplineAnimator(std::int32_t startTime,
    //   const std::vector< glm::vec3 >& points,
    //   float speed = 1.0f, float tightness = 0.5f, bool loop=true, bool pingpong=false) = 0;

    //! Creates a simple ITriangleSelector, based on a mesh.
    /** Triangle selectors
    can be used for doing collision detection. Don't use this selector
    for a huge amount of triangles like in Quake3 maps.
    Instead, use for example ISceneManager::createOctreeTriangleSelector().
    Please note that the created triangle selector is not automatically attached
    to the scene node. You will have to call ISceneNode::setTriangleSelector()
    for this. To create and attach a triangle selector is done like this:
    \code
    ITriangleSelector* s = sceneManager->createTriangleSelector(yourMesh,
        yourSceneNode);
    yourSceneNode->setTriangleSelector(s);
    s->drop();
    \endcode
    \param mesh: Mesh of which the triangles are taken.
    \param node: Scene node of which transformation is used.
    \param separateMeshbuffers: When true it's possible to get information which meshbuffer
    got hit in collision tests. But has a slight speed cost.
    \return The selector, or null if not successful.
    If you no longer need the selector, you should call ITriangleSelector::drop().
    See IReferenceCounted::drop() for more information. */
    // virtual ITriangleSelector* createTriangleSelector(IMesh* mesh, ISceneNode* node, bool separateMeshbuffers=false) = 0;

    //! Creates a simple ITriangleSelector, based on a meshbuffer.
    /**
    This is a static selector which won't update when the mesh changes.
    \param meshBuffer Triangles of that meshbuffer are used
    \param materialIndex If you pass a material index that index can be returned by the triangle selector.
    \para node: Scene node of which transformation is used.
    */
    // virtual ITriangleSelector* createTriangleSelector(const IMeshBuffer* meshBuffer, std::uint32_t materialIndex, ISceneNode* node) = 0;

    //! Creates a simple ITriangleSelector, based on an animated mesh scene node.
    /** Details of the mesh associated with the node will be extracted internally.
    \param node The animated mesh scene node from which to build the selector
    \param separateMeshbuffers: When true it's possible to get information which meshbuffer
    got hit in collision tests. But has a slight speed cost.
    */
    // virtual ITriangleSelector* createTriangleSelector(IAnimatedMeshSceneNode* node, bool separateMeshbuffers=false) = 0;


    //! Creates a simple dynamic ITriangleSelector, based on a axis aligned bounding box.
    /** Triangle selectors
    can be used for doing collision detection. Every time when triangles are
    queried, the triangle selector gets the bounding box of the scene node,
    an creates new triangles. In this way, it works good with animated scene nodes.
    \param node: Scene node of which the bounding box, visibility and transformation is used.
    \return The selector, or null if not successful.
    If you no longer need the selector, you should call ITriangleSelector::drop().
    See IReferenceCounted::drop() for more information. */
    // virtual ITriangleSelector* createTriangleSelectorFromBoundingBox(ISceneNode* node) = 0;

    //! Creates a Triangle Selector, optimized by an octree.
    /** Triangle selectors
    can be used for doing collision detection. This triangle selector is
    optimized for huge amounts of triangle, it organizes them in an octree.
    Please note that the created triangle selector is not automatically attached
    to the scene node. You will have to call ISceneNode::setTriangleSelector()
    for this. To create and attach a triangle selector is done like this:
    \code
    ITriangleSelector* s = sceneManager->createOctreeTriangleSelector(yourMesh,
        yourSceneNode);
    yourSceneNode->setTriangleSelector(s);
    s->drop();
    \endcode
    For more information and examples on this, take a look at the collision
    tutorial in the SDK.
    \param mesh: Mesh of which the triangles are taken.
    \param node: Scene node of which visibility and transformation is used.
    \param minimalPolysPerNode: Specifies the minimal polygons contained a octree node.
    If a node gets less polys than this value, it will not be split into
    smaller nodes.
    \return The selector, or null if not successful.
    If you no longer need the selector, you should call ITriangleSelector::drop().
    See IReferenceCounted::drop() for more information. */
    // virtual ITriangleSelector* createOctreeTriangleSelector(IMesh* mesh,
      // ISceneNode* node, std::int32_t minimalPolysPerNode=32) = 0;

    //! Creates a Triangle Selector for a single meshbuffer, optimized by an octree.
    /** Triangle selectors
    can be used for doing collision detection. This triangle selector is
    optimized for huge amounts of triangle, it organizes them in an octree.
    Please note that the created triangle selector is not automatically attached
    to the scene node. You will have to call ISceneNode::setTriangleSelector()
    for this. To create and attach a triangle selector is done like this:
    \code
    ITriangleSelector* s = sceneManager->createOctreeTriangleSelector(yourMesh,
        yourSceneNode);
    yourSceneNode->setTriangleSelector(s);
    s->drop();
    \endcode
    For more information and examples on this, take a look at the collision
    tutorial in the SDK.
    \param meshBuffer: Meshbuffer of which the triangles are taken.
    \param materialIndex: Setting this value allows the triangle selector to return the material index
    \param node: Scene node of which visibility and transformation is used.
    \param minimalPolysPerNode: Specifies the minimal polygons contained a octree node.
    If a node gets less polys than this value, it will not be split into
    smaller nodes.
    \return The selector, or null if not successful.
    If you no longer need the selector, you should call ITriangleSelector::drop().
    See IReferenceCounted::drop() for more information. */
    // virtual ITriangleSelector* createOctreeTriangleSelector(IMeshBuffer* meshBuffer, std::uint32_t materialIndex,
    //   ISceneNode* node, std::int32_t minimalPolysPerNode=32) = 0;

    // //! //! Creates a Triangle Selector, optimized by an octree.
    // /**
    //  ITriangleSelector* createOctTreeTriangleSelector(IMesh* mesh,
    //   ISceneNode* node, std::int32_t minimalPolysPerNode=32)
    // {
    //   return createOctreeTriangleSelector(mesh, node, minimalPolysPerNode);
    // }

    //! Creates a meta triangle selector.
    /** A meta triangle selector is nothing more than a
    collection of one or more triangle selectors providing together
    the interface of one triangle selector. In this way,
    collision tests can be done with different triangle soups in one pass.
    \return The selector, or null if not successful.
    If you no longer need the selector, you should call ITriangleSelector::drop().
    See IReferenceCounted::drop() for more information. */
    // virtual IMetaTriangleSelector* createMetaTriangleSelector() = 0;

    //! Creates a triangle selector which can select triangles from a terrain scene node.
    /** \param node: Pointer to the created terrain scene node
    \param LOD: Level of detail, 0 for highest detail.
    \return The selector, or null if not successful.
    If you no longer need the selector, you should call ITriangleSelector::drop().
    See IReferenceCounted::drop() for more information. */
    // virtual ITriangleSelector* createTerrainTriangleSelector(
      // ITerrainSceneNode* node, std::int32_t LOD= 0) = 0;

    //! Adds an external mesh loader for extending the engine with new file formats.
    /** If you want the engine to be extended with
    file formats it currently is not able to load (e.g. .cob), just implement
    the IMeshLoader interface in your loading class and add it with this method.
    Using this method it is also possible to override built-in mesh loaders with
    newer or updated versions without the need to recompile the engine.
    \param externalLoader: Implementation of a new mesh loader. */
    // virtual void addExternalMeshLoader(IMeshLoader* externalLoader) = 0;

    //! Returns the number of mesh loaders supported by Irrlicht at this time
    // virtual std::uint32_t getMeshLoaderCount() const = 0;

    //! Retrieve the given mesh loader
    /** \param index The index of the loader to retrieve. This parameter is an 0-based
    array index.
    \return A pointer to the specified loader, 0 if the index is incorrect. */
    // virtual IMeshLoader* getMeshLoader(std::uint32_t index) const = 0;

    //! Adds an external scene loader for extending the engine with new file formats.
    /** If you want the engine to be extended with
    file formats it currently is not able to load (e.g. .vrml), just implement
    the ISceneLoader interface in your loading class and add it with this method.
    Using this method it is also possible to override the built-in scene loaders
    with newer or updated versions without the need to recompile the engine.
    \param externalLoader: Implementation of a new mesh loader. */
    // virtual void addExternalSceneLoader(ISceneLoader* externalLoader) = 0;

    //! Returns the number of scene loaders supported by Irrlicht at this time
    // virtual std::uint32_t getSceneLoaderCount() const = 0;

    //! Retrieve the given scene loader
    /** \param index The index of the loader to retrieve. This parameter is an 0-based
    array index.
    \return A pointer to the specified loader, 0 if the index is incorrect. */
    // virtual ISceneLoader* getSceneLoader(std::uint32_t index) const = 0;

    //! Get pointer to the scene collision manager.
    /** \return Pointer to the collision manager
    This pointer should not be dropped. See IReferenceCounted::drop() for more information. */
    // virtual ISceneCollisionManager* getSceneCollisionManager() = 0;

    //! Get pointer to the mesh manipulator.
    /** \return Pointer to the mesh manipulator
    This pointer should not be dropped. See IReferenceCounted::drop() for more information. */
    // virtual IMeshManipulator* getMeshManipulator() = 0;

    //! Adds a scene node to the deletion queue.
    /** The scene node is immediately
    deleted when it's secure. Which means when the scene node does not
    execute animators and things like that. This method is for example
    used for deleting scene nodes by their scene node animators. In
    most other cases, a ISceneNode::remove() call is enough, using this
    deletion queue is not necessary.
    See ISceneManager::createDeleteAnimator() for details.
    \param node: Node to delete. */
    // virtual void addToDeletionQueue(ISceneNode* node) = 0;

    //! Clears the whole scene.
    /** All scene nodes are removed. */
    virtual void clear() = 0;

    //! Get the default scene node factory which can create all built in scene nodes
    /** \return Pointer to the default scene node factory
    This pointer should not be dropped. See IReferenceCounted::drop() for more information. */
    // virtual ISceneNodeFactory* getDefaultSceneNodeFactory() = 0;

    //! Adds a scene node factory to the scene manager.
    /** Use this to extend the scene manager with new scene node types which it should be
    able to create automatically, for example when loading data from xml files. */
    // virtual void registerNodeFactory(ISceneNodeFactory* factoryToAdd) = 0;

    //! Get amount of registered scene node factories.
    // virtual std::uint32_t getRegisteredSceneNodeFactoryCount() const = 0;

    //! Get a scene node factory by index
    /** \return Pointer to the requested scene node factory, or 0 if it does not exist.
    This pointer should not be dropped. See IReferenceCounted::drop() for more information. */
    // virtual ISceneNodeFactory* getSceneNodeFactory(std::uint32_t index) = 0;

    //! Get the default scene node animator factory which can create all built-in scene node animators
    /** \return Pointer to the default scene node animator factory
    This pointer should not be dropped. See IReferenceCounted::drop() for more information. */
    // virtual ISceneNodeAnimatorFactory* getDefaultSceneNodeAnimatorFactory() = 0;

    //! Adds a scene node animator factory to the scene manager.
    /** Use this to extend the scene manager with new scene node animator types which it should be
    able to create automatically, for example when loading data from xml files. */
    // virtual void registerNodeAnimatorFactory(ISceneNodeAnimatorFactory* factoryToAdd) = 0;

    //! Get amount of registered scene node animator factories.
    // virtual std::uint32_t getRegisteredSceneNodeAnimatorFactoryCount() const = 0;

    //! Get scene node animator factory by index
    /** \return Pointer to the requested scene node animator factory, or 0 if it does not exist.
    This pointer should not be dropped. See IReferenceCounted::drop() for more information. */
    // virtual ISceneNodeAnimatorFactory* getSceneNodeAnimatorFactory(std::uint32_t index) = 0;

    //! Get typename from a scene node type or null if not found
    // virtual const char* getSceneNodeTypeName(E_SCENE_NODE_TYPE type) = 0;

    //! Returns a typename from a scene node animator type or null if not found
    // virtual const char* getAnimatorTypeName(ESCENE_NODE_ANIMATOR_TYPE type) = 0;

    //! Adds a scene node to the scene by name
    /** \return Pointer to the scene node added by a factory
    This pointer should not be dropped. See IReferenceCounted::drop() for more information. */
    // virtual ISceneNode* addSceneNode(const char* sceneNodeTypeName, ISceneNode* parent= 0) = 0;

    //! creates a scene node animator based on its type name
    /** \param typeName: Type of the scene node animator to add.
    \param target: Target scene node of the new animator.
    \return Returns pointer to the new scene node animator or null if not successful. You need to
    drop this pointer after calling this, see IReferenceCounted::drop() for details. */
    // virtual ISceneNodeAnimator* createSceneNodeAnimator(const char* typeName, ISceneNode* target= 0) = 0;

    //! Get a mesh writer implementation if available
    /** Note: You need to drop() the pointer after use again, see IReferenceCounted::drop()
    for details. */
    // virtual IMeshWriter* createMeshWriter(EMESH_WRITER_TYPE type) = 0;

    //! Get a skinned mesh, which is not available as header-only code
    /** Note: You need to drop() the pointer after use again, see IReferenceCounted::drop()
    for details. */
    // virtual ISkinnedMesh* createSkinnedMesh() = 0;

    //! Get an instance of a geometry creator.
    /** The geometry creator provides some helper methods to create various types of
    basic geometry. This can be useful for custom scene nodes. */
    // virtual const IGeometryCreator* getGeometryCreator(void) const = 0;

    //! Check if node is culled in current view frustum
    /** Please note that depending on the used culling method this
    check can be rather coarse, or slow. A positive result is
    correct, though, i.e. if this method returns true the node is
    positively not visible. The node might still be invisible even
    if this method returns false.
    \param node The scene node which is checked for culling.
    \return True if node is not visible in the current scene, else
    false. */
    // virtual bool isCulled(const ISceneNode* node) const = 0;
  };

} // namespace scene
} // namespace saga

#endif

