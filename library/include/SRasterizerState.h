#ifndef __SRASTERIZER_STATE_H_INCLUDED__
#define __SRASTERIZER_STATE_H_INCLUDED__

#include "ECullMode.h"
#include "EFrontFaceMode.h"

namespace saga
{
namespace video
{
  struct SRasterizerState
  {
    E_CULL_MODE CullMode = E_CULL_MODE::BACK_FACE;
    E_FRONT_FACE_MODE FrontFaceMode = E_FRONT_FACE_MODE::COUNTER_CLOCKWISE;
    bool DepthClamp = false;
    float DepthBias = 0.f;
    float DepthBiasClamp = 0.f;
    float DepthBiasSlope = 0.f;
    int SampleCount;
  };

} // namespace scene
} // namespace saga

#endif // __SRASTERIZER_STATE_H_INCLUDED__

