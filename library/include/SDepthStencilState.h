#ifndef __SDEPTH_STENCIL_STATE_H_INCLUDED__
#define __SDEPTH_STENCIL_STATE_H_INCLUDED__

#include "ECompareFunc.h"

namespace saga
{
namespace video
{
  struct SDepthStencilState
  {
    bool Enabled = true;
    E_COMPARE_FUNC DepthCompareFunc = E_COMPARE_FUNC::LESS_EQUAL;
    bool DepthWriteEnabled = true;
  };

} // namespace scene
} // namespace saga

#endif // __SDEPTH_STENCIL_STATE_H_INCLUDED__

