#ifndef __I_MESH_H_INCLUDED__
#define __I_MESH_H_INCLUDED__

#include "ID.h"
#include "aabbox3d.h"
#include <memory>
#include <SShaderBuffer.h>

namespace saga
{
namespace scene
{
  class IMeshBuffer;

  enum E_MESH_TYPE
  {
    STATIC,
    ANIMATED,
    SKINNED
  };

  //! Class which holds the geometry of an object.
  /** An IMesh is nothing more than a collection of some mesh buffers
  (IMeshBuffer). SMesh is a simple implementation of an IMesh.
  A mesh is usually added to an IMeshSceneNode in order to be rendered.
  */
  class IMesh
  {
  public:

    void setID(const ID id) { MeshID = id; }
    ID getID() const { return MeshID; }

    //! adds a MeshBuffer
    /** The bounding box is not updated automatically. */
    virtual void addMeshBuffer(std::unique_ptr<IMeshBuffer> buf) = 0;

    //! Get the amount of mesh buffers.
    /** \return Amount of mesh buffers (IMeshBuffer) in this mesh. */
    virtual std::uint32_t getMeshBufferCount() const = 0;

    //! Get pointer to a mesh buffer.
    /** \param nr: Zero based index of the mesh buffer. The maximum value is
    getMeshBufferCount() - 1;
    \return Pointer to the mesh buffer or 0 if there is no such
    mesh buffer. */
    virtual IMeshBuffer& getMeshBuffer(std::uint32_t nr = 0) = 0;

    //! Get an axis aligned bounding box of the mesh.
    /** \return Bounding box of this mesh. */
    virtual const core::aabbox3d<float>& getBoundingBox() const = 0;

    //! Set user-defined axis aligned bounding box
    /** \param box New bounding box to use for the mesh. */
    virtual void setBoundingBox(const core::aabbox3df& box) = 0;

    virtual E_MESH_TYPE getMeshType() const = 0;

  protected:
    //! ID of this mesh
    ID MeshID = INVALID_ID;
  };

} // namespace scene
} // namespace saga

#endif

