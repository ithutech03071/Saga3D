#ifndef __ID_H_INCLUDED__
#define __ID_H_INCLUDED__

#include <cstdint>

namespace saga
{

  constexpr auto INVALID_ID = 0;
  //! Type for node ID
  using ID = std::uint32_t;

} // namespace saga

#endif // __ID_H_INCLUDED__

