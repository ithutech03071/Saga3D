#ifndef __I_MESH_SCENE_NODE_H_INCLUDED__
#define __I_MESH_SCENE_NODE_H_INCLUDED__

#include "ISceneNode.h"
#include "IMesh.h"
#include "ISceneManager.h"
#include "IMeshBuffer.h"
#include "IVideoDriver.h"

namespace saga
{
namespace scene
{

class IMesh;

//! A scene node displaying a static mesh
class IMeshSceneNode : public ISceneNode
{
public:

  //! Constructor
  /** Use setMesh() to set the mesh to display.
  */
  IMeshSceneNode(
    const std::shared_ptr<IMesh>& mesh,
    const std::shared_ptr<ISceneNode>& parent,
    const std::shared_ptr<ISceneManager>& mgr,
    const glm::vec3& position = glm::vec3(0,0,0),
    const glm::vec3& rotation = glm::vec3(0,0,0),
    const glm::vec3& scale = glm::vec3(1,1,1))
    : ISceneNode(parent, mgr, position, rotation, scale),
      Mesh(mesh) {}

  ~IMeshSceneNode()
  {
    SceneManager->getVideoDriver()->destroyPipelineBuffer(*this, true);
  }

  virtual void setPipeline(video::PipelineHandle pipeline) override
  {
    ISceneNode::setPipeline(pipeline);
    auto& mesh = getMesh();
    auto& meshBuffer = mesh->getMeshBuffer();
    auto& driver = SceneManager->getVideoDriver();
    meshBuffer.buildBuffer(*driver, getPipeline());
    driver->createPipelineBuffer(*this);
  }

  virtual void onRegisterSceneNode(video::RenderPassHandle pass) override
  {
    if (IsVisible)
    {
      SceneManager->getNodeList()[pass].push_back(shared_from_this());
      ISceneNode::onRegisterSceneNode(pass);
    }
  }

  virtual void onUnregisterSceneNode(video::RenderPassHandle pass) override
  {
    auto& nodeList = SceneManager->getNodeList(pass);
    for (auto nodeIt = nodeList.begin(); nodeIt != nodeList.end(); ++nodeIt) {
      if ((*nodeIt)->getID() == getID()) {
        nodeList.erase(nodeIt);
        break;
      }
    }
    ISceneNode::onUnregisterSceneNode(pass);
  }

  //! returns the axis aligned bounding box of this node
  virtual const core::aabbox3d<float>& getBoundingBox() const override
  { return Mesh ? Mesh->getBoundingBox() : Box; }

  //! returns the material based on the zero based index i. To get the amount
  //! of materials used by this scene node, use getMaterialCount().
  //! This function is needed for inserting the node into the scene hirachy on a
  //! optimal position for minimizing renderstate changes, but can also be used
  //! to directly modify the material of a scene node.
  // virtual video::SMaterial& getMaterial(std::uint32_t i) override;

  //! Returns type of the scene node
  virtual E_SCENE_NODE_TYPE getType() const override { return E_SCENE_NODE_TYPE::MESH; }

  //! Sets a new mesh
  virtual void setMesh(const std::shared_ptr<IMesh>& mesh) { Mesh = mesh; }

  //! Returns the current mesh
  virtual const std::shared_ptr<IMesh>& getMesh() const { return Mesh; }

protected:
  std::shared_ptr<IMesh> Mesh = nullptr;
  core::aabbox3d<float> Box;
};

} // namespace scene
} // namespace saga


#endif

