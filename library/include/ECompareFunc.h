#ifndef __E_COMPARE_FUNC_H_INCLUDED__
#define __E_COMPARE_FUNC_H_INCLUDED__

namespace saga
{
namespace video
{

//! Comparison function, e.g. for depth buffer test
enum E_COMPARE_FUNC
{
  //! Test never succeeds, this equals disable
  NEVER,
  //! <= test, default for e.g. depth test
  LESS_EQUAL,
  //! Exact equality
  EQUAL,
  //! exclusive less comparison, i.e. <
  LESS,
  //! Succeeds almost always, except for exact equality
  NOT_EQUAL,
  //! >= test
  GREATER_EQUAL,
  //! inverse of <=
  GREATER,
  //! test succeeds always
  ALWAYS
};

} // namespace scene
} // namespace saga

#endif

