// Copyright (C) 2002-2012 Nikolaus Gebhardt
// This file is part of the "Irrlicht Engine".
// For conditions of distribution and use, see copyright notice in irrlicht.h

#ifndef __IRR_LINE_3D_H_INCLUDED__
#define __IRR_LINE_3D_H_INCLUDED__

#include <glm/vec3.hpp>

namespace saga
{
namespace core
{

//! 3D line between two points with intersection methods.
template <class T>
class line3d
{
  public:

    //! Default constructor
    /** line from (0,0,0) to (1,1,1) */
    line3d() : start(0,0,0), end(1,1,1) {}
    //! Constructor with two points
    line3d(T xa, T ya, T za, T xb, T yb, T zb) : start(xa, ya, za), end(xb, yb, zb) {}
    //! Constructor with two points as vectors
    line3d(const glm::vec3& start, const glm::vec3& end) : start(start), end(end) {}

    // operators

    line3d<T> operator+(const glm::vec3& point) const { return line3d<T>(start + point, end + point); }
    line3d<T>& operator+=(const glm::vec3& point) { start += point; end += point; return *this; }

    line3d<T> operator-(const glm::vec3& point) const { return line3d<T>(start - point, end - point); }
    line3d<T>& operator-=(const glm::vec3& point) { start -= point; end -= point; return *this; }

    bool operator==(const line3d<T>& other) const
    { return (start==other.start && end==other.end) || (end==other.start && start==other.end);}
    bool operator!=(const line3d<T>& other) const
    { return !(start==other.start && end==other.end) || (end==other.start && start==other.end);}

    // functions
    //! Set this line to a new line going through the two points.
    void setLine(const T& xa, const T& ya, const T& za, const T& xb, const T& yb, const T& zb)
    { start = {xa, ya, za}; end = {xb, yb, zb}; }
    //! Set this line to a new line going through the two points.
    void setLine(const glm::vec3& nstart, const glm::vec3& nend)
    { start = nstart; end = nend; }
    //! Set this line to new line given as parameter.
    void setLine(const line3d<T>& line)
    { start = line.start; end = line.end; }

    //! Get length of line
    /** \return Length of line. */
    T getLength() const { return glm::distance(start, end); }

    //! Get squared length of line
    /** \return Squared length of line. */
    T getLengthSQ() const { auto d = glm::distance(start, end);  return d * d; }

    //! Get middle of line
    /** \return Center of line. */
    glm::vec3 getMiddle() const
    {
      return (start + end)/(T)2;
    }

    //! Get vector of line
    /** \return vector of line. */
    glm::vec3 getVector() const
    {
      return end - start;
    }

    //! Check if the given point is between start and end of the line.
    /** Assumes that the point is already somewhere on the line.
    \param point The point to test.
    \return True if point is on the line between start and end, else false.
    */
    bool isPointBetweenStartAndEnd(const glm::vec3& point) const
    {
      return isBetweenPoints(point, start, end);
    }

    //! Get the closest point on this line to a point
    /** \param point The point to compare to.
    \return The nearest point which is part of the line. */
    glm::vec3 getClosestPoint(const glm::vec3& point) const
    {
      glm::vec3 c = point - start;
      glm::vec3 v = end - start;
      T d = (T) glm::length(v);
      v /= d;
      T t = glm::dot(v, c);

      if (t < (T)0.0)
        return start;
      if (t > d)
        return end;

      v *= t;
      return start + v;
    }

    //! Check if the line intersects with a sphere
    /** \param sorigin: Origin of the sphere.
    \param sradius: Radius of the sphere.
    \param outdistance: The distance to the first intersection point.
    \return True if there is an intersection.
    If there is one, the distance to the first intersection point
    is stored in outdistance. */
    bool getIntersectionWithSphere(const glm::vec3& sorigin, T sradius, double& outdistance) const
    {
      const glm::vec3 q = sorigin - start;
      T c = glm::length(q);
      T v = glm::dot(q, glm::normalize(getVector()));
      T d = sradius * sradius - (c*c - v*v);

      if (d < 0.0)
        return false;

      outdistance = v - core::squareroot (d);
      return true;
    }

    // member variables

    //! Start point of line
    glm::vec3 start;
    //! End point of line
    glm::vec3 end;
};

  //! Typedef for an float line.
  typedef line3d<float> line3df;
  //! Typedef for an integer line.
  typedef line3d<std::int32_t> line3di;

} // namespace core
} // namespace saga

#endif

