// Copyright (C) 2002-2012 Nikolaus Gebhardt
// This file is part of the "Irrlicht Engine".
// For conditions of distribution and use, see copyright notice in irrlicht.h

#ifndef __I_CAMERA_SCENE_NODE_H_INCLUDED__
#define __I_CAMERA_SCENE_NODE_H_INCLUDED__

#include "ISceneNode.h"
#include "IEventReceiver.h"

namespace saga
{
namespace scene
{
  struct SViewFrustum;

  //! Scene Node which is a (controllable) camera.
  /** The whole scene will be rendered from the cameras point of view.
  Because the ICameraSceneNode is a SceneNode, it can be attached to any
  other scene node, and will follow its parents movement, rotation and so
  on.
  */
  class ICameraSceneNode : public ISceneNode
  {
  public:

    //! Constructor
    ICameraSceneNode(const std::shared_ptr<ISceneNode>& parent, const std::shared_ptr<ISceneManager>& mgr,
      const glm::vec3& position = glm::vec3(0,0,0),
      const glm::vec3& rotation = glm::vec3(0,0,0),
      const glm::vec3& scale = glm::vec3(1.0f,1.0f,1.0f))
      : ISceneNode(parent, mgr, position, rotation, scale), IsOrthogonal(false) {}

    //! Sets the projection matrix of the camera.
    /** The glm::mat4 class has some methods to build a
    projection matrix. e.g:
    glm::mat4::buildProjectionMatrixPerspectiveFovLH.
    Note that the matrix will only stay as set by this method until
    one of the following Methods are called: setNearValue,
    setFarValue, setAspectRatio, setFOV.
    \param projection The new projection matrix of the camera.
    \param isOrthogonal Set this to true if the matrix is an
    orthogonal one (e.g. from matrix4::buildProjectionMatrixOrtho).
    */
    virtual void setProjectionMatrix(const glm::mat4& projection, bool isOrthogonal = false) = 0;

    //! Gets the current projection matrix of the camera.
    /** \return The current projection matrix of the camera. */
    virtual const glm::mat4& getProjectionMatrix() const = 0;

    //! Gets the current view matrix of the camera.
    /** \return The current view matrix of the camera. */
    virtual const glm::mat4& getViewMatrix() const = 0;

    //! Sets the look at target of the camera
    /** If the camera's target and rotation are bound (@see
    bindTargetAndRotation()) then calling this will also change
    the camera's scene node rotation to match the target.
    Note that setTarget uses the current absolute position
    internally, so if you changed setPosition since last rendering you must
    call updateAbsolutePosition before using this function.
    \param pos Look at target of the camera, in world co-ordinates. */
    virtual void setTarget(const glm::vec3& newTarget) = 0;

    //! Sets the rotation of the node.
    /** This only modifies the relative rotation of the node.
    If the camera's target and rotation are bound (@see
    bindTargetAndRotation()) then calling this will also change
    the camera's target to match the rotation.
    \param rotation New rotation of the node in degrees. */
    virtual void setRotation(const glm::vec3& rotation) = 0;

    //! Gets the current look at target of the camera
    /** \return The current look at target of the camera, in world co-ordinates */
    virtual const glm::vec3& getTarget() const = 0;

    //! Sets the up vector of the camera.
    /** \param pos: New upvector of the camera. */
    virtual void setUpVector(const glm::vec3& pos) = 0;

    //! Gets the up vector of the camera.
    /** \return The up vector of the camera, in world space. */
    virtual const glm::vec3& getUpVector() const = 0;

    //! Gets the value of the near plane of the camera.
    /** \return The value of the near plane of the camera. */
    virtual float getNearValue() const = 0;

    //! Gets the value of the far plane of the camera.
    /** \return The value of the far plane of the camera. */
    virtual float getFarValue() const = 0;

    //! Gets the aspect ratio of the camera.
    /** \return The aspect ratio of the camera. */
    virtual float getAspectRatio() const = 0;

    //! Gets the field of view of the camera.
    /** \return The field of view of the camera in radians. */
    virtual float getFOV() const = 0;

    //! Sets the value of the near clipping plane. (default: 1.0f)
    /** \param zn: New z near value. */
    virtual void setNearValue(float zn) = 0;

    //! Sets the value of the far clipping plane (default: 2000.0f)
    /** \param zf: New z far value. */
    virtual void setFarValue(float zf) = 0;

    //! Sets the aspect ratio (default: 4.0f / 3.0f)
    /** \param aspect: New aspect ratio. */
    virtual void setAspectRatio(float aspect) = 0;

    //! Sets the field of view (Default: PI / 2.5f)
    /** \param fovy: New field of view in radians. */
    virtual void setFOV(float fovy) = 0;

    //! Checks if a camera is orthogonal.
    virtual bool isOrthogonal() const
    {
      return IsOrthogonal;
    }

    //! Updates view matrix
    virtual void updateViewMatrix() = 0;

    //! Updates projection matrix
    virtual void updateProjectionMatrix() = 0;

  protected:
    bool IsOrthogonal;
  };

} // namespace scene
} // namespace saga

#endif

