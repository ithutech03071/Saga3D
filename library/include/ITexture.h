// Copyright (C) 2002-2012 Nikolaus Gebhardt
// This file is part of the "Irrlicht Engine".
// For conditions of distribution and use, see copyright notice in irrlicht.h

#ifndef __I_TEXTURE_H_INCLUDED__
#define __I_TEXTURE_H_INCLUDED__

#include "EDriverTypes.h"
#include <string>

namespace saga
{
namespace video
{

//! Interface of a Video Driver dependent Texture.
/** An ITexture is created by an IVideoDriver by using IVideoDriver::addTexture
or IVideoDriver::getTexture. After that, the texture may only be used by this
VideoDriver. As you can imagine, textures of the DirectX and the OpenGL device
will, e.g., not be compatible. An exception is the Software device and the
NULL device, their textures are compatible. If you try to use a texture
created by one device with an other device, the device will refuse to do that
and write a warning or an error message to the output buffer.
*/
class ITexture
{
public:

  //! constructor
    ITexture(const std::string& name, E_TEXTURE_TYPE type)
    :    NamedPath(name), DriverType(E_DRIVER_TYPE::NULL_DRIVER),
         OriginalColorFormat(E_PIXEL_FORMAT::UNKNOWN),
         ColorFormat(E_PIXEL_FORMAT::UNKNOWN), Pitch(0),
         HasMipMaps(false), IsRenderTarget(false),
         Source(E_TEXTURE_SOURCE::UNKNOWN), Type(type)
  {
  }

  //! Lock function.
  /** Locks the Texture and returns a pointer to access the
  pixels. After lock() has been called and all operations on the pixels
  are done, you must call unlock().
  Locks are not accumulating, hence one unlock will do for an arbitrary
  number of previous locks. You should avoid locking different levels without
  unlocking in between, though, because only the last level locked will be
  unlocked.
  The size of the i-th mipmap level is defined as max(getSize().Width>>i,1)
  and max(getSize().Height>>i,1)
  \param mode Specifies what kind of changes to the locked texture are
  allowed. Unspecified behavior will arise if texture is written in read
  only mode or read from in write only mode.
  Support for this feature depends on the driver, so don't rely on the
  texture being write-protected when locking with read-only, etc.
  \param layer It determines which cubemap face or texture array layer should be locked.
  \return Returns a pointer to the pixel data. The format of the pixel can
  be determined by using getColorFormat(). 0 is returned, if
  the texture cannot be locked. */
    virtual void* lock(E_TEXTURE_LOCK_MODE mode = E_TEXTURE_LOCK_MODE::READ_WRITE, std::uint32_t layer = 0) = 0;

  //! Unlock function. Must be called after a lock() to the texture.
  /** One should avoid to call unlock more than once before another lock.
  The last locked mip level will be unlocked. */
  virtual void unlock() = 0;

  //! Regenerates the mip map levels of the texture.
  /** Required after modifying the texture, usually after calling unlock().
  \param data Optional parameter to pass in image data which will be
  used instead of the previously stored or automatically generated mipmap
  data. The data has to be a continuous pixel data for all mipmaps until
  1x1 pixel. Each mipmap has to be half the width and height of the previous
  level. At least one pixel will be always kept.
  \param layer It informs a texture about layer which needs
  mipmaps regeneration. */
  virtual void regenerateMipMapLevels(void* data = 0, std::uint32_t layer = 0) = 0;

  //! Get original size of the texture.
  /** The texture is usually scaled, if it was created with an unoptimal
  size. For example if the size was not a power of two. This method
  returns the size of the texture it had before it was scaled. Can be
  useful when drawing 2d images on the screen, which should have the
  exact size of the original texture. Use ITexture::getSize() if you want
  to know the real size it has now stored in the system.
  \return The original size of the texture. */
  const glm::uvec2& getOriginalSize() const { return OriginalSize; };

  //! Get dimension (=size) of the texture.
  /** \return The size of the texture. */
  const glm::uvec2& getSize() const { return Size; };

  //! Get driver type of texture.
  /** This is the driver, which created the texture. This method is used
  internally by the video devices, to check, if they may use a texture
  because textures may be incompatible between different devices.
  \return Driver type of texture. */
  E_DRIVER_TYPE getDriverType() const { return DriverType; };

  //! Get the color format of texture.
  /** \return The color format of texture. */
  E_PIXEL_FORMAT getColorFormat() const { return ColorFormat; };

  //! Get pitch of the main texture (in bytes).
  /** The pitch is the amount of bytes used for a row of pixels in a
  texture.
  \return Pitch of texture in bytes. */
  std::uint32_t getPitch() const { return Pitch; };

  //! Check whether the texture has MipMaps
  /** \return True if texture has MipMaps, else false. */
  bool hasMipMaps() const { return HasMipMaps; }

  //! Check whether the texture is a render target
  /** Render targets can be set as such in the video driver, in order to
  render a scene into the texture. Once unbound as render target, they can
  be used just as usual textures again.
  \return True if this is a render target, otherwise false. */
  bool isRenderTarget() const { return IsRenderTarget; }

  //! Get name of texture (in most cases this is the filename)
  const std::string& getName() const { return NamedPath; }

  //! Check where the last IVideoDriver::getTexture found this texture
  E_TEXTURE_SOURCE getSource() const { return Source; }

  //! Used internally by the engine to update Source status on IVideoDriver::getTexture calls.
  void updateSource(E_TEXTURE_SOURCE source) { Source = source; }

  //! Returns if the texture has an alpha channel
  bool hasAlpha() const
  {
    bool status = false;

    switch (ColorFormat)
    {
        case E_PIXEL_FORMAT::RGBA8:
        case E_PIXEL_FORMAT::DXT1:
        case E_PIXEL_FORMAT::DXT2:
        case E_PIXEL_FORMAT::DXT3:
        case E_PIXEL_FORMAT::DXT4:
        case E_PIXEL_FORMAT::DXT5:
        case E_PIXEL_FORMAT::A16B16G16R16F:
        case E_PIXEL_FORMAT::A32B32G32R32F:
      status = true;
      break;
    default:
      break;
    }

    return status;
  }

  //! Returns the type of texture
  E_TEXTURE_TYPE getType() const { return Type; }

protected:
  std::string NamedPath;
  glm::uvec2 OriginalSize;
  glm::uvec2 Size;
  E_DRIVER_TYPE DriverType;
  E_PIXEL_FORMAT OriginalColorFormat;
  E_PIXEL_FORMAT ColorFormat;
  std::uint32_t Pitch;
  bool HasMipMaps;
  bool IsRenderTarget;
  E_TEXTURE_SOURCE Source;
  E_TEXTURE_TYPE Type;
};


} // namespace video
} // namespace saga

#endif

