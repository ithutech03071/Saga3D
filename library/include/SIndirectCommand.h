#ifndef __SINDIRECT_COMMAND_H_INCLUDED__
#define __SINDIRECT_COMMAND_H_INCLUDED__

#include <vector>

namespace saga
{
namespace video
{
  struct SIndirectCommand
  {
    std::uint32_t VertexCount = 0;
    std::uint32_t InstanceCount = 1;
    std::uint32_t VertexOffset = 0;
    std::uint32_t FirstInstance = 0;
  };

  struct SIndexedIndirectCommand
  {
    std::uint32_t IndexCount = 0;
    std::uint32_t InstanceCount = 1;
    std::uint32_t IndexOffset = 0;
    std::uint32_t VertexOffset = 0;
    std::uint32_t FirstInstance = 0;
  };

  struct SDrawCommand
  {
    glm::ivec4 Scissor;
    bool UseScissor = false;
    bool IndexedDraw = false;
    std::uint32_t VertexCount = 0;
    std::uint32_t IndexCount = 0;
    std::uint32_t IndexOffset = 0;
    std::uint32_t VertexOffset = 0;
    std::uint32_t InstanceCount = 1;
    std::uint32_t FirstInstance = 0;
  };

  using DrawCommandList = std::vector<SDrawCommand>;

} // namespace video
} // namespace saga

#endif // __SINDIRECT_COMMAND_H_INCLUDED__

