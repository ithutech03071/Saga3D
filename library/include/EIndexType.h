#ifndef __E_INDEX_TYPE_H_INCLUDED__
#define __E_INDEX_TYPE_H_INCLUDED__

namespace saga
{
namespace scene
{

enum class E_INDEX_TYPE
{
  UINT16,
  UINT32 
};

} // namespace scene
} // namespace saga

#endif

