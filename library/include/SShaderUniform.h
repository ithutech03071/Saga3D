#ifndef __SSHADER_UNIFORM_H_INCLUDED__
#define __SSHADER_UNIFORM_H_INCLUDED__

#include "SGPUResource.h"
#include <memory>

namespace saga
{
namespace video
{
  struct SShaderUniform : public SGPUResource
  {
    std::unique_ptr<void*> Data;
    std::size_t Size;
  };

  using ShaderUniformHandle = SGPUResource::HandleType;

} // namespace scene
} // namespace saga

#endif // __SSHADER_UNIFORM_H_INCLUDED__

