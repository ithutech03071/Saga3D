// Copyright (C) 2002-2012 Nikolaus Gebhardt
// This file is part of the "Irrlicht Engine".
// For conditions of distribution and use, see copyright notice in irrlicht.h

#ifndef __E_HARDWARE_BUFFER_FLAGS_INCLUDED__
#define __E_HARDWARE_BUFFER_FLAGS_INCLUDED__

namespace saga
{
namespace scene
{

  enum class E_HARDWARE_MAPPING
  {
    //! Don't store on the hardware
    NEVER,

    //! Rarely changed, usually stored completely on the hardware
    STATIC,

    //! Sometimes changed, driver optimized placement
    DYNAMIC,

    //! Always changed, cache optimizing on the GPU
    STREAM
  };

  enum class E_BUFFER_TYPE
  {
    //! Does not change anything
    NONE,
    //! Change the vertex mapping
    VERTEX,
    //! Change the index mapping
    INDEX,
    //! Change both vertex and index mapping to the same value
    VERTEX_AND_INDEX
  };

} // namespace scene
} // namespace saga

#endif

