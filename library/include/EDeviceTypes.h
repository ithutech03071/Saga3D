#ifndef __E_DEVICE_TYPES_H_INCLUDED__
#define __E_DEVICE_TYPES_H_INCLUDED__

namespace saga
{

  //! An enum class for the different device types supported by the engine.
  enum class E_DEVICE_TYPE
  {
    SDL,
  };

} // namespace saga

#endif // __E_DEVICE_TYPES_H_INCLUDED__

