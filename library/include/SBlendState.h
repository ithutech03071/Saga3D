#ifndef __SBLEND_STATE_H_INCLUDED__
#define __SBLEND_STATE_H_INCLUDED__

#include "EBlendFactor.h"
#include "EBlendOp.h"
#include "EPixelFormat.h"
#include "GraphicsConstants.h"
#include <array>

namespace saga
{
namespace video
{
  enum E_COLOR_COMPONENT
  {
    R = 1 << 0,
    G = 1 << 1,
    B = 1 << 2,
    A = 1 << 3,
    RGB = E_COLOR_COMPONENT::R | E_COLOR_COMPONENT::G | E_COLOR_COMPONENT::B,
    RGBA = E_COLOR_COMPONENT::RGB | E_COLOR_COMPONENT::A
  };

  struct SBlendAttachmentState
  {
    bool Enabled = false;
    E_BLEND_FACTOR SrcColor;
    E_BLEND_FACTOR DstColor;
    E_BLEND_OP ColorBlendOp;
    E_BLEND_FACTOR SrcAlpha;
    E_BLEND_FACTOR DstAlpha;
    E_BLEND_OP AlphaBlendOp;
    std::uint32_t ColorWriteMask;
  };

  struct SBlendState
  {
    bool Enabled = false;
    int ColorAttachmentCount;
    float Factors[4] = { 0.f };
    std::array<SBlendAttachmentState, MAX_COLOR_ATTACHMENTS> AttachmentStates;
  };

} // namespace scene
} // namespace saga

#endif // __SBLEND_STATE_H_INCLUDED__

