// Copyright (C) 2002-2012 Nikolaus Gebhardt
// This file is part of the "Irrlicht Engine".
// For conditions of distribution and use, see copyright notice in irrlicht.h

#ifndef __E_PRIMITIVE_TYPES_H_INCLUDED__
#define __E_PRIMITIVE_TYPES_H_INCLUDED__

namespace saga
{
namespace scene 
{

  //! Enumeration for all primitive types there are.
  enum class E_PRIMITIVE_TYPE
  {
    //! All vertices are non-connected points.
    POINTS,

    //! All vertices form a single connected line.
    LINE_STRIP,

    //! Just as LINE_STRIP, but the last and the first vertex is also connected.
    LINE_LOOP,

    //! Every two vertices are connected creating n/2 lines.
    LINES,

    //! After the first two vertices each vertex defines a new triangle.
    //! Always the two last and the new one form a new triangle.
    TRIANGLE_STRIP,

    //! After the first two vertices each vertex defines a new triangle.
    //! All around the common first vertex.
    TRIANGLE_FAN,

    //! Explicitly set all vertices for each triangle.
    TRIANGLES,

    //! After the first two vertices each further two vertices create a quad with the preceding two.
    //! Not supported by Direct3D
    QUAD_STRIP,

    //! Every four vertices create a quad.
    //! Not supported by Direct3D
    //! Deprecated with newer OpenGL drivers
    QUADS,

    //! Just as LINE_LOOP, but filled.
    //! Not supported by Direct3D
    //! Deprecated with newer OpenGL drivers
    POLYGON,

    //! The single vertices are expanded to quad billboards on the GPU.
    POINT_SPRITES
  };

} // namespace video
} // namespace saga

#endif

