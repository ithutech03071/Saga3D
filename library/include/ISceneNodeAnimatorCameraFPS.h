// Copyright (C) 2002-2012 Nikolaus Gebhardt
// This file is part of the "Irrlicht Engine".
// For conditions of distribution and use, see copyright notice in irrlicht.h

#ifndef __I_SCENE_NODE_ANIMATOR_CAMERA_FPS_H_INCLUDED__
#define __I_SCENE_NODE_ANIMATOR_CAMERA_FPS_H_INCLUDED__

#include "ISceneNodeAnimator.h"
#include "IEventReceiver.h"

namespace saga
{
namespace scene
{

  //! Special scene node animator for FPS cameras
  /** This scene node animator can be attached to a camera to make it act
  like a first person shooter
  */
  class ISceneNodeAnimatorCameraFPS : public ISceneNodeAnimator
  {
  public:

    //! Returns the speed of movement in units per millisecond
    virtual float getMoveSpeed() const = 0;

    //! Sets the speed of movement in units per millisecond
    virtual void setMoveSpeed(float moveSpeed) = 0;

    //! Returns the rotation speed in degrees when using mouse
    /** The degrees are equivalent to a half screen movement of the mouse,
    i.e. if the mouse cursor had been moved to the border of the screen since
    the last animation. */
    virtual float getRotateSpeed() const = 0;

    //! Set the rotation speed in degrees when using mouse
    virtual void setRotateSpeed(float rotateSpeed) = 0;

    //! Sets whether the Y axis of the mouse should be inverted.
    /** If enabled then moving the mouse down will cause
    the camera to look up. It is disabled by default. */
    virtual void setInvertMouse(bool invert) = 0;
  };
} // namespace scene
} // namespace saga

#endif // __I_SCENE_NODE_ANIMATOR_CAMERA_FPS_H_INCLUDED__
