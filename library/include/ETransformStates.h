#ifndef __E_TRANSFORM_STATES_H_INCLUDED__
#define __E_TRANSFORM_STATES_H_INCLUDED__

namespace saga
{
namespace video
{

  //! enumeration for geometry transformation states
  enum class E_TRANSFORM_STATE
  {
    //! View transformation
    VIEW = 0,
    //! Projection transformation
    PROJECTION,
    //! World transformation
    WORLD,
    //! Texture transformation
    TEXTURE_0,
    //! Texture transformation
    TEXTURE_1,
    //! Texture transformation
    TEXTURE_2,
    //! Texture transformation
    TEXTURE_3,
  #if _IRR_MATERIAL_MAX_TEXTURES_ > 4
      //! Texture transformation
      TEXTURE_4,
  #if _IRR_MATERIAL_MAX_TEXTURES_ > 5
      //! Texture transformation
      TEXTURE_5,
  #if _IRR_MATERIAL_MAX_TEXTURES_ > 6
      //! Texture transformation
      TEXTURE_6,
  #if _IRR_MATERIAL_MAX_TEXTURES_ > 7
      //! Texture transformation
      TEXTURE_7,
  #endif
  #endif
  #endif
  #endif
  };

} // namespace video
} // namespace saga

#endif  // __E_TRANSFORM_STATES_H_INCLUDED__
