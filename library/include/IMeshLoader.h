#ifndef __I_MESH_LOADER_H_INCLUDED__
#define __I_MESH_LOADER_H_INCLUDED__

#include <string>
#include <memory>

namespace saga
{
namespace scene
{
class IMesh;

//! Class which is able to load a static/animated mesh from a file.
/** If you want Irrlicht be able to load meshes of
currently unsupported file formats (e.g. .cob), then implement
this and add your new Meshloader with
ISceneManager::addExternalMeshLoader() to the engine. */
class IMeshLoader
{
public:
  //! Constructor
  IMeshLoader() { }

  //! Destructor
  virtual ~IMeshLoader() { }

  //! Returns true if the file might be loaded by this class.
  /** This decision should be based on the file extension (e.g. ".cob")
  only.
  \param filename Name of the file to test.
  \return True if the file might be loaded by this class. */
  virtual bool isSupportedExtension(const std::string& filename) const = 0;

  //! Loads a mesh from the file.
  /**
  \param data Pointer to mesh file in memory to read.
  \param size Size of mesh file.
  \return Pointer to the created mesh. Returns nullptr if loading failed. */
  virtual std::shared_ptr<IMesh> createMesh(void* data, const std::size_t size, const std::string& extension) = 0;
};

} // namespace scene
} // namespace saga

#endif // __I_MESH_LOADER_H_INCLUDED__
