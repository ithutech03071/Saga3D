// Copyright (C) 2002-2012 Nikolaus Gebhardt
// This file is part of the "Irrlicht Engine".
// For conditions of distribution and use, see copyright notice in irrlicht.h

#ifndef __SVIEW_FRUSTUM_H_INCLUDED__
#define __SVIEW_FRUSTUM_H_INCLUDED__

#include "plane3d.h"
#include "line3d.h"
#include "aabbox3d.h"

#include "ETransformStates.h"
#include "GraphicsConstants.h"
#include "enumType.hpp"
#include <glm/vec3.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <array>

namespace saga
{
namespace scene
{

  //! Defines the view frustum. That's the space visible by the camera.
  /** The view frustum is enclosed by 6 planes. These six planes share
  eight points. A bounding box around these eight points is also stored in
  this structure.
  */
  struct SViewFrustum
  {
    enum class E_PLANES
    {
      //! Far plane of the frustum. That is the plane furthest away from the eye.
      FAR,
      //! Near plane of the frustum. That is the plane nearest to the eye.
      NEAR,
      //! Left plane of the frustum.
      LEFT,
      //! Right plane of the frustum.
      RIGHT,
      //! Bottom plane of the frustum.
      BOTTOM,
      //! Top plane of the frustum.
      TOP,
    };

    //! Default Constructor
    SViewFrustum() : BoundingRadius(0.f), FarNearDistance(0.f) {}

    //! Copy Constructor
    SViewFrustum(const SViewFrustum& other);

    //! This constructor creates a view frustum based on a projection and/or view matrix.
    SViewFrustum(const glm::mat4& mat);

    //! This constructor creates a view frustum based on a projection and/or view matrix.
    inline void setFrom(const glm::mat4& mat);

    //! transforms the frustum by the matrix
    /** \param mat: Matrix by which the view frustum is transformed.*/
    void transform(const glm::mat4& mat);

    //! returns the point which is on the far left upper corner inside the the view frustum.
    glm::vec3 getFarLeftUp() const;

    //! returns the point which is on the far left bottom corner inside the the view frustum.
    glm::vec3 getFarLeftDown() const;

    //! returns the point which is on the far right top corner inside the the view frustum.
    glm::vec3 getFarRightUp() const;

    //! returns the point which is on the far right bottom corner inside the the view frustum.
    glm::vec3 getFarRightDown() const;

    //! returns the point which is on the near left upper corner inside the the view frustum.
    glm::vec3 getNearLeftUp() const;

    //! returns the point which is on the near left bottom corner inside the the view frustum.
    glm::vec3 getNearLeftDown() const;

    //! returns the point which is on the near right top corner inside the the view frustum.
    glm::vec3 getNearRightUp() const;

    //! returns the point which is on the near right bottom corner inside the the view frustum.
    glm::vec3 getNearRightDown() const;

    //! returns a bounding box enclosing the whole view frustum
    const core::aabbox3d<float> &getBoundingBox() const;

    //! recalculates the bounding box and sphere based on the planes
    inline void recalculateBoundingBox();

    //! get the bounding sphere's radius (of an optimized sphere, not the AABB's)
    float getBoundingRadius() const;

    //! get the bounding sphere's radius (of an optimized sphere, not the AABB's)
    glm::vec3 getBoundingCenter() const;

    //! the cam should tell the frustum the distance between far and near
    void setFarNearDistance(float distance);

    //! get the given state's matrix based on frustum E_TRANSFORM_STATE
    glm::mat4& getTransform(video::E_TRANSFORM_STATE state);

    //! get the given state's matrix based on frustum E_TRANSFORM_STATE
    const glm::mat4& getTransform(video::E_TRANSFORM_STATE state) const;

    //! clips a line to the view frustum.
    /** \return True if the line was clipped, false if not */
    bool clipLine(core::line3d<float>& line) const;

    //! the position of the camera
    glm::vec3 cameraPosition;

    core::plane3d<float>& plane(E_PLANES type) { return planes[core::enumToPOD(type)]; }
    
    core::plane3d<float> plane(E_PLANES type) const { return planes.at(core::enumToPOD(type)); }

    //! all planes enclosing the view frustum.
    std::array<core::plane3d<float>, VIEW_FRUSTUM_PLANE_COUNT> planes;

    //! bounding box around the view frustum
    core::aabbox3d<float> boundingBox;

  private:
    //! Hold a copy of important transform matrices
    // TODO: port to enum class
    enum class E_TRANSFORM_STATE_FRUSTUM
    {
      VIEW = 0,
      PROJECTION = 1,
    };

    //! recalculates the bounding sphere based on the planes
    inline void recalculateBoundingSphere();

    //! Hold a copy of important transform matrices
    std::array<glm::mat4, TRANSFORM_FRUSTUM_COUNT> Matrices;

    float BoundingRadius;
    float FarNearDistance;
    glm::vec3 BoundingCenter;
  };

  /*!
    Copy constructor ViewFrustum
  */
  inline SViewFrustum::SViewFrustum(const SViewFrustum& other)
  {
    cameraPosition=other.cameraPosition;
    boundingBox=other.boundingBox;

    std::uint32_t i;
    for (i = 0; i < VIEW_FRUSTUM_PLANE_COUNT; ++i)
      planes[i]=other.planes[i];

    for (i = 0; i < TRANSFORM_FRUSTUM_COUNT; ++i)
      Matrices[i]=other.Matrices[i];

    BoundingRadius = other.BoundingRadius;
    FarNearDistance = other.FarNearDistance;
    BoundingCenter = other.BoundingCenter;
  }

  inline SViewFrustum::SViewFrustum(const glm::mat4& mat)
  {
    setFrom(mat);
  }

  inline void SViewFrustum::transform(const glm::mat4& mat)
  {
    for (std::uint32_t i = 0; i < VIEW_FRUSTUM_PLANE_COUNT; ++i)
      planes[i].transform(mat);

    cameraPosition = glm::vec4(cameraPosition, 1) * mat;
    recalculateBoundingBox();
  }

  inline glm::vec3 SViewFrustum::getFarLeftUp() const
  {
    glm::vec3 p;
    plane(scene::SViewFrustum::E_PLANES::FAR).getIntersectionWithPlanes(
      plane(scene::SViewFrustum::E_PLANES::TOP),
      plane(scene::SViewFrustum::E_PLANES::LEFT), p);

    return p;
  }

  inline glm::vec3 SViewFrustum::getFarLeftDown() const
  {
    glm::vec3 p;
    plane(scene::SViewFrustum::E_PLANES::FAR).getIntersectionWithPlanes(
      plane(scene::SViewFrustum::E_PLANES::BOTTOM),
      plane(scene::SViewFrustum::E_PLANES::LEFT), p);

    return p;
  }

  inline glm::vec3 SViewFrustum::getFarRightUp() const
  {
    glm::vec3 p;
    plane(scene::SViewFrustum::E_PLANES::FAR).getIntersectionWithPlanes(
      plane(scene::SViewFrustum::E_PLANES::TOP),
      plane(scene::SViewFrustum::E_PLANES::RIGHT), p);

    return p;
  }

  inline glm::vec3 SViewFrustum::getFarRightDown() const
  {
    glm::vec3 p;
    plane(scene::SViewFrustum::E_PLANES::FAR).getIntersectionWithPlanes(
      plane(scene::SViewFrustum::E_PLANES::BOTTOM),
      plane(scene::SViewFrustum::E_PLANES::RIGHT), p);

    return p;
  }

  inline glm::vec3 SViewFrustum::getNearLeftUp() const
  {
    glm::vec3 p;
    plane(scene::SViewFrustum::E_PLANES::NEAR).getIntersectionWithPlanes(
      plane(scene::SViewFrustum::E_PLANES::TOP),
      plane(scene::SViewFrustum::E_PLANES::LEFT), p);

    return p;
  }

  inline glm::vec3 SViewFrustum::getNearLeftDown() const
  {
    glm::vec3 p;
    plane(scene::SViewFrustum::E_PLANES::NEAR).getIntersectionWithPlanes(
      plane(scene::SViewFrustum::E_PLANES::BOTTOM),
      plane(scene::SViewFrustum::E_PLANES::LEFT), p);

    return p;
  }

  inline glm::vec3 SViewFrustum::getNearRightUp() const
  {
    glm::vec3 p;
    plane(scene::SViewFrustum::E_PLANES::NEAR).getIntersectionWithPlanes(
      plane(scene::SViewFrustum::E_PLANES::TOP),
      plane(scene::SViewFrustum::E_PLANES::RIGHT), p);

    return p;
  }

  inline glm::vec3 SViewFrustum::getNearRightDown() const
  {
    glm::vec3 p;
    plane(scene::SViewFrustum::E_PLANES::NEAR).getIntersectionWithPlanes(
      plane(scene::SViewFrustum::E_PLANES::BOTTOM),
      plane(scene::SViewFrustum::E_PLANES::RIGHT), p);

    return p;
  }

  inline const core::aabbox3d<float> &SViewFrustum::getBoundingBox() const
  {
    return boundingBox;
  }

  inline void SViewFrustum::recalculateBoundingBox()
  {
    boundingBox.reset (cameraPosition);

    boundingBox.addInternalPoint(getFarLeftUp());
    boundingBox.addInternalPoint(getFarRightUp());
    boundingBox.addInternalPoint(getFarLeftDown());
    boundingBox.addInternalPoint(getFarRightDown());

    // Also recalculate the bounding sphere when the bbox changes
    recalculateBoundingSphere();
  }

  inline float SViewFrustum::getBoundingRadius() const
  {
    return BoundingRadius;
  }

  inline glm::vec3 SViewFrustum::getBoundingCenter() const
  {
    return BoundingCenter;
  }

  inline void SViewFrustum::setFarNearDistance(float distance)
  {
    FarNearDistance = distance;
  }

  //! This constructor creates a view frustum based on a projection
  //! and/or view matrix.
  inline void SViewFrustum::setFrom(const glm::mat4& matrix)
  {
    // left clipping plane
    auto mat = glm::value_ptr(matrix);
    plane(E_PLANES::LEFT).Normal.x = mat[3 ] + mat[0];
    plane(E_PLANES::LEFT).Normal.y = mat[7 ] + mat[4];
    plane(E_PLANES::LEFT).Normal.z = mat[11] + mat[8];
    plane(E_PLANES::LEFT).D =        mat[15] + mat[12];

    // right clipping plane
    plane(E_PLANES::RIGHT).Normal.x = mat[3 ] - mat[0];
    plane(E_PLANES::RIGHT).Normal.y = mat[7 ] - mat[4];
    plane(E_PLANES::RIGHT).Normal.z = mat[11] - mat[8];
    plane(E_PLANES::RIGHT).D =        mat[15] - mat[12];

    // top clipping plane
    plane(E_PLANES::TOP).Normal.x = mat[3 ] - mat[1];
    plane(E_PLANES::TOP).Normal.y = mat[7 ] - mat[5];
    plane(E_PLANES::TOP).Normal.z = mat[11] - mat[9];
    plane(E_PLANES::TOP).D =        mat[15] - mat[13];

    // bottom clipping plane
    plane(E_PLANES::BOTTOM).Normal.x = mat[3 ] + mat[1];
    plane(E_PLANES::BOTTOM).Normal.y = mat[7 ] + mat[5];
    plane(E_PLANES::BOTTOM).Normal.z = mat[11] + mat[9];
    plane(E_PLANES::BOTTOM).D =        mat[15] + mat[13];

    // far clipping plane
    plane(E_PLANES::FAR).Normal.x = mat[3 ] - mat[2];
    plane(E_PLANES::FAR).Normal.y = mat[7 ] - mat[6];
    plane(E_PLANES::FAR).Normal.z = mat[11] - mat[10];
    plane(E_PLANES::FAR).D =        mat[15] - mat[14];

    // near clipping plane
    plane(E_PLANES::NEAR).Normal.x = mat[2];
    plane(E_PLANES::NEAR).Normal.y = mat[6];
    plane(E_PLANES::NEAR).Normal.z = mat[10];
    plane(E_PLANES::NEAR).D =        mat[14];

    // normalize normals
    std::uint32_t i;
    for (i = 0; i != VIEW_FRUSTUM_PLANE_COUNT; ++i)
    {
      const float len = -core::reciprocal_squareroot(glm::length2(planes[i].Normal));
      planes[i].Normal *= len;
      planes[i].D *= len;
    }

    // make bounding box
    recalculateBoundingBox();
  }

  /*!
    View Frustum depends on Projection & View Matrix
  */
  inline glm::mat4& SViewFrustum::getTransform(video::E_TRANSFORM_STATE state)
  {
    return Matrices[core::enumToPOD(state)];
  }

  /*!
    View Frustum depends on Projection & View Matrix
  */
  inline const glm::mat4& SViewFrustum::getTransform(video::E_TRANSFORM_STATE state) const
  {
    return Matrices.at(core::enumToPOD(state));
  }

  //! Clips a line to the frustum
  inline bool SViewFrustum::clipLine(core::line3d<float>& line) const
  {
    bool wasClipped = false;
    for (std::uint32_t i = 0; i < VIEW_FRUSTUM_PLANE_COUNT; ++i)
    {
      if (planes[i].classifyPointRelation(line.start) == core::ISREL3D_FRONT)
      {
        line.start = glm::mix(line.start, line.end,
          planes[i].getKnownIntersectionWithLine(line.start, line.end)
        );
        wasClipped = true;
      }
      if (planes[i].classifyPointRelation(line.end) == core::ISREL3D_FRONT)
      {
        line.end = glm::mix(line.start, line.end,
          planes[i].getKnownIntersectionWithLine(line.start, line.end)
        );
        wasClipped = true;
      }
    }
    return wasClipped;
  }

  inline void SViewFrustum::recalculateBoundingSphere()
  {
    // Find the center
    const float shortlen = glm::length(getNearLeftUp() - getNearRightUp());
    const float longlen = glm::length(getFarLeftUp() - getFarRightUp());

    const float farlen = FarNearDistance;
    const float fartocenter = (farlen + (shortlen - longlen) * (shortlen + longlen)/(4*farlen)) / 2;
    const float neartocenter = farlen - fartocenter;

    BoundingCenter = cameraPosition + -plane(E_PLANES::NEAR).Normal * neartocenter;

    // Find the radius
    glm::vec3 dir[8];
    dir[0] = getFarLeftUp() - BoundingCenter;
    dir[1] = getFarRightUp() - BoundingCenter;
    dir[2] = getFarLeftDown() - BoundingCenter;
    dir[3] = getFarRightDown() - BoundingCenter;
    dir[4] = getNearRightDown() - BoundingCenter;
    dir[5] = getNearLeftDown() - BoundingCenter;
    dir[6] = getNearRightUp() - BoundingCenter;
    dir[7] = getNearLeftUp() - BoundingCenter;

    std::uint32_t i = 0;
    float diam[8] = { 0.f };

    for (i = 0; i < 8; ++i)
      diam[i] = glm::length2(dir[i]);

    float longest = 0;

    for (i = 0; i < 8; ++i)
    {
      if (diam[i] > longest)
        longest = diam[i];
    }

    BoundingRadius = sqrtf(longest);
  }

} // namespace scene
} // namespace saga

#endif

