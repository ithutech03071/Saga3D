// Copyright (C) 2002-2012 Nikolaus Gebhardt
// This file is part of the "Irrlicht Engine".
// For conditions of distribution and use, see copyright notice in irrlicht.h

#ifndef __IRR_RECT_H_INCLUDED__
#define __IRR_RECT_H_INCLUDED__

#include <glm/vec2.hpp>

namespace saga
{
namespace core
{

  //! Rectangle template.
  /** Mostly used by 2D GUI elements and for 2D drawing methods.
  It has 2 positions instead of position and dimension and a fast
  method for collision detection with other rectangles and points.

  Coordinates are (0,0) for top-left corner, and increasing to the right
  and to the bottom.
  */
  template <class T>
  class rect
  {
  public:

    //! Default constructor creating empty rectangle at (0,0)
    rect() : UpperLeftCorner(0,0), LowerRightCorner(0,0) {}

    //! Constructor with two corners
    rect(T x, T y, T x2, T y2)
      : UpperLeftCorner(x,y), LowerRightCorner(x2,y2) {}

    //! Constructor with two corners
    rect(const glm::vec2& upperLeft, const glm::vec2& lowerRight)
      : UpperLeftCorner(upperLeft), LowerRightCorner(lowerRight) {}

    //! Constructor with upper left corner and dimension
    template <class U>
    rect(const glm::vec2& pos, const glm::vec2& size)
      : UpperLeftCorner(pos), LowerRightCorner(pos.x + size.x, pos.y + size.y) {}

    //! Constructor with upper left at 0,0 and lower right using dimension
    template <class U>
    explicit rect(const glm::vec2& size)
      : UpperLeftCorner(0,0), LowerRightCorner(size.x, size.y) {}

    //! move right by given numbers
    rect<T> operator+(const glm::vec2& pos) const
    {
      rect<T> ret(*this);
      return ret+=pos;
    }

    //! move right by given numbers
    rect<T>& operator+=(const glm::vec2& pos)
    {
      UpperLeftCorner += pos;
      LowerRightCorner += pos;
      return *this;
    }

    //! move left by given numbers
    rect<T> operator-(const glm::vec2& pos) const
    {
      rect<T> ret(*this);
      return ret-=pos;
    }

    //! move left by given numbers
    rect<T>& operator-=(const glm::vec2& pos)
    {
      UpperLeftCorner -= pos;
      LowerRightCorner -= pos;
      return *this;
    }

    //! equality operator
    bool operator==(const rect<T>& other) const
    {
      return (UpperLeftCorner == other.UpperLeftCorner &&
        LowerRightCorner == other.LowerRightCorner);
    }

    //! inequality operator
    bool operator!=(const rect<T>& other) const
    {
      return (UpperLeftCorner != other.UpperLeftCorner ||
        LowerRightCorner != other.LowerRightCorner);
    }

    //! compares size of rectangles
    bool operator<(const rect<T>& other) const
    {
      return getArea() < other.getArea();
    }

    //! Returns size of rectangle
    T getArea() const
    {
      return getWidth() * getHeight();
    }

    //! Returns if a 2d point is within this rectangle.
    /** \param pos Position to test if it lies within this rectangle.
    \return True if the position is within the rectangle, false if not. */
    bool isPointInside(const glm::vec2& pos) const
    {
      return (UpperLeftCorner.x <= pos.x &&
        UpperLeftCorner.y <= pos.y &&
        LowerRightCorner.x >= pos.x &&
        LowerRightCorner.y >= pos.y);
    }

    //! Check if the rectangle collides with another rectangle.
    /** \param other Rectangle to test collision with
    \return True if the rectangles collide. */
    bool isRectCollided(const rect<T>& other) const
    {
      return (LowerRightCorner.y > other.UpperLeftCorner.y &&
        UpperLeftCorner.y < other.LowerRightCorner.y &&
        LowerRightCorner.x > other.UpperLeftCorner.x &&
        UpperLeftCorner.x < other.LowerRightCorner.x);
    }

    //! Clips this rectangle with another one.
    /** \param other Rectangle to clip with */
    void clipAgainst(const rect<T>& other)
    {
      if (other.LowerRightCorner.x < LowerRightCorner.x)
        LowerRightCorner.x = other.LowerRightCorner.x;
      if (other.LowerRightCorner.y < LowerRightCorner.y)
        LowerRightCorner.y = other.LowerRightCorner.y;

      if (other.UpperLeftCorner.x > UpperLeftCorner.x)
        UpperLeftCorner.x = other.UpperLeftCorner.x;
      if (other.UpperLeftCorner.y > UpperLeftCorner.y)
        UpperLeftCorner.y = other.UpperLeftCorner.y;

      // correct possible invalid rect resulting from clipping
      if (UpperLeftCorner.y > LowerRightCorner.y)
        UpperLeftCorner.y = LowerRightCorner.y;
      if (UpperLeftCorner.x > LowerRightCorner.x)
        UpperLeftCorner.x = LowerRightCorner.x;
    }

    //! Moves this rectangle to fit inside another one.
    /** \return True on success, false if not possible */
    bool constrainTo(const rect<T>& other)
    {
      if (other.getWidth() < getWidth() || other.getHeight() < getHeight())
        return false;

      T diff = other.LowerRightCorner.x - LowerRightCorner.x;
      if (diff < 0)
      {
        LowerRightCorner.x += diff;
        UpperLeftCorner.x  += diff;
      }

      diff = other.LowerRightCorner.y - LowerRightCorner.y;
      if (diff < 0)
      {
        LowerRightCorner.y += diff;
        UpperLeftCorner.y  += diff;
      }

      diff = UpperLeftCorner.x - other.UpperLeftCorner.x;
      if (diff < 0)
      {
        UpperLeftCorner.x  -= diff;
        LowerRightCorner.x -= diff;
      }

      diff = UpperLeftCorner.y - other.UpperLeftCorner.y;
      if (diff < 0)
      {
        UpperLeftCorner.y  -= diff;
        LowerRightCorner.y -= diff;
      }

      return true;
    }

    //! Get width of rectangle.
    T getWidth() const
    {
      return LowerRightCorner.x - UpperLeftCorner.x;
    }

    //! Get height of rectangle.
    T getHeight() const
    {
      return LowerRightCorner.y - UpperLeftCorner.y;
    }

    //! If the lower right corner of the rect is smaller then the upper left, the points are swapped.
    void repair()
    {
      if (LowerRightCorner.x < UpperLeftCorner.x)
      {
        T t = LowerRightCorner.x;
        LowerRightCorner.x = UpperLeftCorner.x;
        UpperLeftCorner.x = t;
      }

      if (LowerRightCorner.y < UpperLeftCorner.y)
      {
        T t = LowerRightCorner.y;
        LowerRightCorner.y = UpperLeftCorner.y;
        UpperLeftCorner.y = t;
      }
    }

    //! Returns if the rect is valid to draw.
    /** It would be invalid if the UpperLeftCorner is lower or more
    right than the LowerRightCorner. */
    bool isValid() const
    {
      return ((LowerRightCorner.x >= UpperLeftCorner.x) &&
        (LowerRightCorner.y >= UpperLeftCorner.y));
    }

    //! Get the center of the rectangle
    glm::vec2 getCenter() const
    {
      return glm::vec2(
          (UpperLeftCorner.x + LowerRightCorner.x) / 2,
          (UpperLeftCorner.y + LowerRightCorner.y) / 2);
    }

    //! Get the dimensions of the rectangle
    glm::vec2 getSize() const
    {
      return glm::vec2(getWidth(), getHeight());
    }


    //! Adds a point to the rectangle
    /** Causes the rectangle to grow bigger if point is outside of
    the box
    \param p Point to add to the box. */
    void addInternalPoint(const glm::vec2& p)
    {
      addInternalPoint(p.x, p.y);
    }

    //! Adds a point to the bounding rectangle
    /** Causes the rectangle to grow bigger if point is outside of
    the box
    \param x X-Coordinate of the point to add to this box.
    \param y Y-Coordinate of the point to add to this box. */
    void addInternalPoint(T x, T y)
    {
      if (x>LowerRightCorner.x)
        LowerRightCorner.x = x;
      if (y>LowerRightCorner.y)
        LowerRightCorner.y = y;

      if (x<UpperLeftCorner.x)
        UpperLeftCorner.x = x;
      if (y<UpperLeftCorner.y)
        UpperLeftCorner.y = y;
    }

    //! Upper left corner
    glm::vec2 UpperLeftCorner;
    //! Lower right corner
    glm::vec2 LowerRightCorner;
  };

  //! Rectangle with float values
  typedef rect<float> rectf;
  //! Rectangle with int values
  typedef rect<std::int32_t> recti;

} // namespace core
} // namespace saga

#endif

